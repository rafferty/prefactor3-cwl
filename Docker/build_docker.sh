#! /bin/bash
set -e

git_clone_or_pull () {
    REPO=$1
    BRANCH=$2
    DIR=$3
    if [ -d $DIR ] 
    then
     git -C "${DIR}" pull
    else
     git clone --depth 1 -b "${BRANCH}" "${REPO}" "${DIR}"
    fi
}

Prefactor_TAG=production

# FETCHES Prefactor
git_clone_or_pull https://github.com/lofar-astron/prefactor.git ${Prefactor_TAG} prefactor
SCRIPT_PATH=$(realpath ${BASH_SOURCE[0]})

DOCKER_PATH=$(dirname ${SCRIPT_PATH})

docker build ${DOCKER_PATH} -t lofareosc/prefactor
docker build ${DOCKER_PATH} -f ${DOCKER_PATH}/Dockerfile_ci -t lofareosc/prefactor-ci
