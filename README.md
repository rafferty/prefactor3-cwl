# Prefactor3-CWL

This is a CWL implementation of the Prefactor 3 pipeline, which lives at https://github.com/lofar-astron/prefactor.

<img src="https://www.egi.eu/wp-content/uploads/2020/01/eu-logo.jpeg" alt="EU Flag" width="80">
<img src="https://www.egi.eu/wp-content/uploads/2020/01/eosc-hub-v-web.png" alt="EOSC-hub logo" height="60">
This work is co-funded by the EOSC-hub project (Horizon 2020) under Grant number 777536. 
