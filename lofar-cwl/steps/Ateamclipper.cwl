class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: check_ateam_separation
baseCommand:
  - python3
  - /usr/local/bin/Ateamclipper.py
inputs:
  - id: msin
    type:
      - Directory
      - type: array
        items: Directory
    inputBinding:
      position: 0
    doc: Input measurement set
outputs:
  - id: msout
    doc: Output MS
    type: Directory
    outputBinding:
      glob: $(inputs.msin.basename)
  - id: logfile
    type: 'File[]'
    outputBinding:
      glob: Ateamclipper.log
  - id: output
    type: File
    outputBinding:
      glob: Ateamclipper.txt
label: Ateamclipper
hints:
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
  - class: DockerRequirement
    dockerPull: lofareosc/prefactor:HBAcalibrator
  - class: InlineJavascriptRequirement
stdout: Ateamclipper.log
