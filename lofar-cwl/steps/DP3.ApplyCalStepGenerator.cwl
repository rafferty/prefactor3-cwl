#!/usr/bin/env cwl-runner

class: Workflow
cwlVersion: v1.0
id: applycal_step_generator

inputs:
  - id: steps
    type: Any[]?
    default: []
  - id: step_name
    type: string
    doc: unique name for the step
    default: applycal

  - id: parmdb
    type: File
    doc: Path of parmdb in which the parameters are stored. This can also be an H5Parm file, in that case the filename has to end in '.h5'
  - id: solset
    type: string?
    doc: |
      In case of applying an H5Parm file: the name of the solset to be used.
      If empty, defaults to the name of one solset present in the H5Parm
      (if more solsets are present in an H5Parm and solset is left empty,
       an error will be thrown)
  - id: correction
    type:
      type: enum
      symbols:
       - gain
       - tec
       - clock
       - rotationangle
       - rotation
       - scalarphase
       - scalaramplitude
       - rotationmeasure
       - fulljones
       - polalign
       - bandpass
       - RMextract
       - phaseOrig
    doc: |
      Type of correction to perform, can be one of 'gain', 'tec', 'clock',
      '(common)rotationangle' / 'rotation', '(common)scalarphase',
      '(common)scalaramplitude' or 'rotationmeasure'
      (create multiple ApplyCal steps for multiple corrections).
      When using H5Parm,
      this is for now the name of the soltab;
      the type will be deduced from the metadata in that soltab,
      except for full Jones, in which case correction should be 'fulljones'.
  - id: soltab
    type: string[]?
    doc: |
     The name or names of the H5 soltab.
     Currently only used when correction=fulljones,
      in which case soltab should list two names (amplitude and phase soltab).
  - id: direction
    type: string?
    doc: If using H5Parm, the direction of the solution to use
  - id: updateweights
    type: boolean?
    doc: |
      Update the weights column, in a way consistent with
      the weights being inverse proportional to the autocorrelations
      (e.g. if 'autoweights' was used before).
  - id: interpolation
    default: nearest
    type:
      type: enum
      symbols:
       - nearest
       - linear
    doc: If using H5Parm, the type of interpolation (in time and frequency) to use, can be one of 'nearest' or 'linear'.
  - id: invert
    type: boolean?
    doc: |
     Invert the corrections, to correct the data. Default is true.
     If you want to corrupt the data, set it to 'false'
  - id: timeslotsperparmupdate
    type: int?
    doc: |
      Number of time slots to handle after one read of the parameter file.
      Optimization to prevent spurious reading from the parmdb.
outputs:
  - id: augmented_steps
    outputSource:
      - DP3_GenericStep/augmented_steps
    type: Any[]
steps:
  - id: DP3_GenericStep
    in:
      - id: step_type
        default: 'applycal'
      - id: step_id
        source: step_name
      - id: steps
        source: steps
      - id: parameters
        valueFrom: $(inputs)

      - id: parmdb
        source: parmdb
      - id: solset
        source: solset
      - id: correction
        source: correction
      - id: soltab
        source: soltab
      - id: direction
        source: direction
      - id: updateweights
        source: updateweights
      - id: interpolation
        source: interpolation
      - id: invert
        source: invert
      - id: timeslotsperparmupdate
        source: timeslotsperparmupdate


#-------------------------------

    out:
      - augmented_steps
    run: ../steps/DP3.GenericStep.cwl
requirements:
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement
  - class: MultipleInputFeatureRequirement
