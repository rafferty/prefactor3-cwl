#!/usr/bin/env cwl-runner

class: Workflow
cwlVersion: v1.0
id: average_step_generator

inputs:
  - id: steps
    type: Any[]?
    default: []
  - id: step_name
    type: string
    doc: unique name for the step
    default: average
  - id: timestep
    type: int
    doc: >-
      Number of time slots to average. It is truncated if exceeding the actual
      number of times.
    default: 1
  - id: freqstep
    type: int
    doc: >-
      Number of channels to average. It is truncated if exceeding the actual
      number of channels.
    default: 1
  - id: minpoints
    type: int
    doc: >-
      If number of averaged unflagged input points < minpoints, the averaged
      point is flagged.
    default: 0
  - id: minperc
    type: float
    doc: 'Like minpoints, but expressed as a percentage of timestep*freqstep.'
    default: 0
  - id: timeresolution
    type: float
    doc: >-
      Target time resolution, in seconds. If this is given, and bigger than
      zero, it overrides <step>.timestep
    default: 0
  - id: freqresolution
    type: string
    doc: >-
      Target frequency resolution, in Hz (or append "MHz" or "kHz" to specify it
      in those units). If this is given, and bigger than zero, it overrides
      <step>.freqstep
    default: 0
outputs:
  - id: augmented_steps
    outputSource:
      - DP3_GenericStep/augmented_steps
    type: Any[]

steps:
  - id: DP3_GenericStep
    in:
      - id: step_type
        default: 'averager'
      - id: step_id
        source: step_name
      - id: steps
        source: steps
      - id: parameters
        valueFrom: $(inputs)
      - id: timestep
        source: timestep
      - id: freqstep
        source: freqstep
      - id: minpoints
        source: minpoints
      - id: minperc
        source: minperc
      - id: timeresolution
        source: timeresolution
      - id: freqresolution
        source: freqresolution
    out:
      - augmented_steps
    run: ../steps/DP3.GenericStep.cwl
requirements:
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement
  - class: MultipleInputFeatureRequirement
