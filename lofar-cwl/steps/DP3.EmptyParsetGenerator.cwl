#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
id: empty_parset_generator
baseCommand: [echo, 'steps=[]']

requirements:
  InlineJavascriptRequirement: {}

inputs: []

stdout: output_parset
outputs:
  - id: output_parset
    doc: Empty parset file
    streamable: True
    type: File
    outputBinding:
        glob: output_parset
  - id: output_secondary_files
    doc: files needed to execute the step
    type: File[]
    outputBinding:
      outputEval: $([])
