class: Workflow
cwlVersion: v1.0
id: dp3_execute
label: DP3.Execute
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: msout_name
    type: string
    'sbg:x': -425.39886474609375
    'sbg:y': 127
  - id: msin
    type: Directory
    'sbg:x': -254.39886474609375
    'sbg:y': 242
  - id: steps
    type: 'Any[]?'
    'sbg:x': -518.3988647460938
    'sbg:y': -88
  - id: autoweight
    type: boolean
    default: true
    'sbg:x': -113
    'sbg:y': 290
outputs:
  - id: secondary_output_files
    outputSource:
      - dppp/secondary_output_files
    type: Any
    'sbg:x': 213.60113525390625
    'sbg:y': -184
  - id: secondary_output_directories
    outputSource:
      - dppp/secondary_output_directories
    type: Any
    'sbg:x': 248.60113525390625
    'sbg:y': -47
  - id: msout
    outputSource:
      - dppp/msout
    type: Directory
    'sbg:x': 224.60113525390625
    'sbg:y': 130
  - id: logfile
    outputSource:
      - dppp/logfile
    type: 'File[]'
    'sbg:x': 185.1875
    'sbg:y': 334.5
  - id: parset
    outputSource:
      - generic_step/parset
    type: File
    'sbg:x': 11.1875
    'sbg:y': -299.5
steps:
  - id: generic_step
    in:
      - id: steps
        source:
          - steps
    out:
      - id: parset
      - id: input_files
      - id: input_directories
      - id: output_file_names
      - id: output_directory_names
    run: ./DP3.ParsetGenerator.cwl
    'sbg:x': -294
    'sbg:y': -84
  - id: dppp
    in:
      - id: parset
        source: generic_step/parset
      - id: msin
        source: msin
      - id: msout_name
        source: msout_name
      - id: secondary_files
        source:
          - generic_step/input_files
      - id: secondary_directories
        source:
          - generic_step/input_directories
      - id: output_file_names
        source: generic_step/output_file_names
      - id: output_directory_names
        source: generic_step/output_directory_names
      - id: autoweight
        source: autoweight
    out:
      - id: msout
      - id: secondary_output_files
      - id: secondary_output_directories
      - id: logfile
    run: ./DPPP.cwl
    'sbg:x': 26
    'sbg:y': -46
requirements: []
