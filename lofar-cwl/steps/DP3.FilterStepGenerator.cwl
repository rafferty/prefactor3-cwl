#!/usr/bin/env cwl-runner

class: Workflow
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: dp3-filter-step-generator

inputs:
  - id: steps
    type: Any[]?
    default: []
  - id: step_name
    type: string
    doc: unique name for the step
    default: filter
  - id: baseline
    type: string
    doc: baseline to keeps
  - id: remove
    type: boolean
    doc: Remove or flag
outputs:
- id: augmented_steps
  outputSource:
    - DP3_GenericStep/augmented_steps
  type: Any[]

steps:
  - id: DP3_GenericStep
    in:
    - id: steps
      source: steps
    - id: parameters
      valueFrom: $(inputs)
    - id: step_id
      source: step_name
    - id: step_type
      default: 'filter'
    - id: baseline
      source: baseline
    - id: remove
      source: remove
    out:
      - augmented_steps
    run: ../steps/DP3.GenericStep.cwl
requirements:
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement
  - class: MultipleInputFeatureRequirement