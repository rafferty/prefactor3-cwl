#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
id: generic_step

requirements:
  - class: InlineJavascriptRequirement

baseCommand:
  - python3

arguments:
 - prefix: -c
   valueFrom: |
        import sys

        true = True
        false = False
        null = None
        steps_list = $(inputs.steps)
        steps_id_list = list(map(lambda item: item['step_id'], steps_list))

        for step in steps_list:
            try:
                step_id = step.get('step_id')
                step_type = step.get('step_type')
                parameters = step.get('parameters')


                line = '.'.join([step_id, 'type'])
                line += '=%s' % step_type
                print(line)

                for parameter_name, parameter in parameters.items():
                    if isinstance(parameter, dict):
                        if parameter['class'] in ['File', 'Directory']:
                            parameter = parameter['basename']
                        else:
                            raise NotImplemented()
                    if parameter is None:
                        continue
                    line = '.'.join([step_id, parameter_name])
                    line = '%s=%s' % (line, parameter)

                    print(line)
            except KeyError as e:
                print('invalid input for step ', step, ':', e)
                sys.exit(1)
        last_step_line = 'steps=[%s,count]' % ', '.join(steps_id_list)
        print(last_step_line)
inputs:
  - id: steps
    type: Any[]?

outputs:
  - id: parset
    doc: Parset output file
    type: File
    outputBinding:
      glob: parset
  - id: input_files
    doc: additional input files
    type: File[]
    outputBinding:
      outputEval: |-
        ${
        var outfiles = []
        for(var step in inputs.steps){
          step = inputs.steps[step]
          for(var parameter in step['parameters']){
            parameter = step['parameters'][parameter]

            if (parameter != null && parameter.class =='File'){
              outfiles.push(parameter)
            }
          }
        }
        return outfiles
        }
  - id: input_directories
    doc: additional input directory
    type: Directory[]
    outputBinding:
      outputEval: |
        ${
        var outdirs = []
        for(var step in inputs.steps){
          step = inputs.steps[step]
          for(var parameter in step['parameters']){
            parameter = step['parameters'][parameter]
            if (parameter != null && parameter.class=='Directory'){
              outdirs.push(parameter)
            }
          }
        }
        return outdirs
        }

  - id: output_file_names
    doc: expected output files
    type: Any
    outputBinding:
      outputEval: |
        ${
        var out_names = {}
        for(var step_idx in inputs.steps){
          var step = inputs.steps[step_idx]
          var step_id = step['step_id']
          var step_dirs = {}
          if(!step.hasOwnProperty("output_files")) continue;

          var step_files = {}

          for(var index in step['output_files']){
            var file_id = index
            var file_name = step['output_files'][file_id]
            step_files[file_id] = file_name
          }

          out_names[step_id] = step_files
        }
        return out_names
        }
  - id: output_directory_names
    doc: expected output directories
    type: Any
    outputBinding:
      outputEval: |-
        ${
        var out_names = {}
        for(var step_idx in inputs.steps){
          var step = inputs.steps[step_idx]
          var step_id = step['step_id']
          var step_dirs = {}
          if(!step.hasOwnProperty("output_directories")) continue;

          for(var index in step['output_directories']){
            var file_id = index
            var file_name = step['output_directories'][file_id]
            step_dirs[file_id] = file_name
          }
          out_names[step_id] = step_dirs
        }
        return out_names
        }
stdout:
  parset
