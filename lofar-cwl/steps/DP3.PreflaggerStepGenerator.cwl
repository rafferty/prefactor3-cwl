#!/usr/bin/env cwl-runner

class: Workflow
cwlVersion: v1.0
id: preflag_step_generator

inputs:
  - id: steps
    type: Any[]?
    default: []
  - id: step_name
    type: string
    doc: unique name for the step
    default: preflag
  - id: count.save
    default: false
    type: boolean
    doc: >-
      If true, the flag percentages per frequency are saved to a table with
      extension .flagfreq and percentages per station to a table with extension
      .flagstat. The basename of the table is the MS name (without extension)
      followed by the stepname and extension.
  - id: count.path
    type: string?
    doc: >-
      The directory where to create the flag percentages table. If empty, the
      path of the input MS is used.
  - id: mode
    default: set
    type: string
    doc: >-
      Case-insensitive string telling what to do with the flags of the data
      matching (or not matching) the selection criteria given in the other
      parameters.
  - id: expr
    default: ''
    type: string?
    doc: >-
      Expression of preflagger keyword sets (see above). Operators AND, OR, and
      NOT are possible (or their equivalents &&,&, ||, |, and !). Parentheses
      can be used to change precedence order. For example:: c1 and (c2 or c3)
  - default:
    id: timeofday
    type: string?
    doc: >-
      Ranges of UTC time-of-day given as st..end or val+-delta. Each value must
      be given as 12:34:56.789, 12h34m56.789, or as a value followed by a unit
      like h, min, or s.
  - id: abstime
    default: []
    type: string[]
    doc: >-
      Ranges of absolute UTC date/time given as st..end or val+-delta. Each
      value (except delta) must be given as a date/time in casacore MVTime
      format, for instance 12-Mar-2010/11:31:00.000. A delta value must be given
      as a time (for instance 1:30:0 or 20s).
  - id: reltime
    default: []
    type: string[]
    doc: >-
      Ranges of times (using .. or +-) since the start of the observation. A
      time can be given like 1:30:0 or 20s.
  - id: timeslot
    default: []
    type: string[]
    doc: >-
      Time slot sequence numbers. First time slot is 0. st..end means end
      inclusive.
  - id: lst
    default: []
    type: string[]
    doc: >-
      Ranges of Local Apparent Sidereal Times like 1:30:0 +- 20min. The LST of a
      time slot is calculated for the array position, thus not per antenna.
  - id: azimuth
    default: []
    type: float[]
    doc: >-
      Ranges of azimuth angles given as st..end or val+-delta. Each value has to
      be given as a casacore direction like 12:34:56.789 or 12h34m56.789,
      12.34.56.789 or 12d34m56.789, or a value followed by a unit like rad or
      deg.
  - id: elevation
    type: string?
    doc: >-
      Ranges of elevation angles (similar to azimuth). For example::
      0deg..10deg
  - id: baseline
    default: []
    type: string[]
    doc: See Description of baseline selection parameters.
  - id: corrtype
    default: ''
    type: string
    doc: Correlation type to match? Must be auto, cross, or an empty string.
  - id: blmin
    default: -1
    type: float
    doc: If blmin > 0, baselines with length < blmin meter will match.
  - id: blmax
    default: -1
    type: float
    doc: If blmax > 0, baselines with length > blmax meter will match.
  - id: uvmmin
    default: -1
    type: float
    doc: >-
      If uvmmin > 0, baselines with UV-distance < uvmmin meter will match. Note
      that the UV-distance is the projected baseline length.
  - id: uvmmax
    default: -1
    type: float
    doc: If uvmmax > 0, baselines with UV-distance > uvmmax meter will match.
  - id: freqrange
    default: ''
    type: string
    doc: >-
      Channels in the given frequency ranges will match. Each value in the
      vector is a range which can be given as start..end or start+-delta. A
      value can be followed by a unit like KHz. If only one value in a range has
      a unit, the unit is also applied to the other value. If a range has no
      unit, it defaults to MHz. For example: freqrange=[1.2 .. 1.4 MHz,
      1.8MHz+-50KHz] flags channels between 1.2MHz and 1.4MHz and between
      1.75MHz and 1.85MHz. The example shows that blanks can be used at will.
  - id: chan
    default: ''
    type: string
    doc: >-
      The given channels will match (start counting at 0). Channels exceeding
      the number of channels are ignored. Similar to msin, it is possible to
      specify the channels as an expression of nchan. Furthermore, .. can be
      used to specify ranges. For example: chan=[0..nchan/32-1,
      31*nchan/32..nchan-1] to flag the first and last 2 or 8 channels
      (depending on 64 or 256 channels in the observation).
  - id: amplmin
    type: float?
    doc: >-
      Correlation data with amplitude < amplmin will match. It can be given per
      correlation. For example, amplmin=[100,,,100] matches data points with XX
      or YY amplitude < 100. The non-specified amplitudes get the default value.
  - id: amplmax
    type: float?
    doc: Correlation data with amplitude > amplmax will match.
  - id: phasemin
    type: float?
    doc: Correlation data with phase < phasemin (in radians) will match.
  - id: phasemax
    type: float?
    doc: Correlation data with phase > phasemax (in radians) will match.
  - id: realmin
    type: float?
    doc: Correlation data with real complex part < realmin will match.
  - id: realmax
    type: float?
    doc: Correlation data with real complex part > realmax will match.
  - id: imagmin
    type: float?
    doc: Correlation data with imaginary complex part < imagmin will match.
  - id: imagmax
    type: float?
    doc: Correlation data with imaginary complex part > imagmax will match.
outputs:
- id: augmented_steps
  outputSource:
    - DP3_GenericStep/augmented_steps
  type: Any[]

steps:
  - id: DP3_GenericStep
    in:
      - id: step_type
        default: 'preflagger'
      - id: step_id
        source: step_name
      - id: steps
        source: steps
      - id: parameters
        valueFrom: $(inputs)
      - id: count.save
        source: count.save
      - id: count.path
        source: count.path
      - id: mode
        source: mode
      - id: expr
        source: expr
      - id: abstime
        source: abstime
      - id: reltime
        source: reltime
      - id: timeslot
        source: timeslot
      - id: lst
        source: lst
      - id: azimuth
        source: azimuth
      - id: elevation
        source: elevation
      - id: baseline
        source: baseline
      - id: corrtype
        source: corrtype
      - id: blmin
        source: blmin
      - id: blmax
        source: blmax
      - id: uvmmin
        source: uvmmin
      - id: uvmmax
        source: uvmmax
      - id: freqrange
        source: freqrange
      - id: chan
        source: chan
      - id: amplmin
        source: amplmin
      - id: amplmax
        source: amplmax
      - id: phasemin
        source: phasemin
      - id: phasemax
        source: phasemax
      - id: realmin
        source: realmin
      - id: realmax
        source: realmax
      - id: imagmin
        source: imagmin
      - id: imagmax
        source: imagmax
    out:
      - augmented_steps
    run: ../steps/DP3.GenericStep.cwl

requirements:
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement
  - class: MultipleInputFeatureRequirement
