class: ExpressionTool
cwlVersion: v1.0
id: selector
inputs:
  - id: select_a
    type: boolean
    doc: 'if true selects a_steps if false selects b_steps'
  - id: a_steps
    type: Any[]
    default: []
  - id: b_steps
    type: Any[]
    default: []
outputs:
  - id: selected_steps
    type: Any
expression: |
  $(
    {'selected_steps': inputs.select_a ? inputs.a_steps : inputs.b_steps}
  )
label: Selector

requirements:
  - class: InlineJavascriptRequirement
