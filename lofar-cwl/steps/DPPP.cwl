class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: dppp
baseCommand:
  - DPPP
inputs:
  - id: parset
    type: File?
    inputBinding:
      position: -1
  - id: msin
    type: Directory?
    inputBinding:
      position: 0
      prefix: msin=
      separate: false
    doc: Input Measurement Set
  - default: "."
    id: msout_name
    type: string
    inputBinding:
      position: 0
      prefix: msout=
      separate: false
    doc: Output Measurement Set
  - id: secondary_files
    type: 'File[]?'
    doc: Secondary files needed for the step
  - id: secondary_directories
    type: 'Directory[]?'
    doc: Secondary directories needed for the step
  - id: output_file_names
    type: Any
    doc: Expected output file names
  - id: output_directory_names
    type: Any
    doc: Expected output file names
  - default: false
    id: autoweight
    type: boolean
    inputBinding:
      position: 0
      prefix: 'msin.autoweight=True'
  - default: ''
    id: baseline
    type: string
    inputBinding:
      position: 0
      prefix: 'msin.baseline='
      separate: false
  - default: DATA
    id: output_column
    type: string?
    inputBinding:
      position: 0
      prefix: 'msout.datacolumn='
      separate: false
  - default: DATA
    id: input_column
    type: string?
    inputBinding:
      position: 0
      prefix: 'msin.datacolumn='
      separate: false
  - id: writefullresflag
    type: boolean
    default: false
    inputBinding:
       prefix: msout.writefullresflag=True
  - id: overwrite
    type: boolean
    default: false
    inputBinding:
       prefix: msout.overwrite=True
  - id: storagemanager
    type: string
    default: ""
    inputBinding:
       prefix: msout.storagemanager=
       separate: false
  - id: databitrate
    type: int
    default: 0
    inputBinding:
       prefix: msout.storagemanager.databitrate=
       separate: false
outputs:
  - id: msout
    doc: Output Measurement Set
    type: Directory
    outputBinding:
      glob: '$(inputs.msout_name=="." ? inputs.msin.basename : inputs.msout_name)'
  - id: secondary_output_files
    doc: Secondary output files
    type: Any
    outputBinding:
      outputEval: |-
        ${
          var output={}
          for(var step_name in inputs.output_file_names){
            var file_per_idx = inputs.output_file_names[step_name];
            for(var file_idx in file_per_idx){
                var file_name = file_per_idx[file_idx];

                output[file_idx] = {'class':'File', 'path': file_name};
            }
          }
          return output
        }
  - id: secondary_output_directories
    doc: Secondary output directories
    type: Any
    outputBinding:
      outputEval: |-
        ${
          var output={}
          for(var step_name in inputs.output_directory_names){
            var file_per_idx = inputs.output_directory_names[step_name];
            for(var file_idx in file_per_idx){
                var file_name = file_per_idx[file_idx];

                output[file_idx] = {'class':'Directory', 'path': file_name};
            }
          }
          return output
        }
  - id: logfile
    type: File[]
    outputBinding:
      glob: 'DPPP*.log'
hints:
  - class: DockerRequirement
    dockerPull: lofareosc/prefactor:HBAcalibrator
stdout: DPPP.log
stderr: DPPP_err.log
requirements:
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
      - entry: $(inputs.secondary_files)
      - entry: $(inputs.secondary_directories)
      
