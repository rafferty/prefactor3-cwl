#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.0
id: losoto_abs

$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
doc: |
  Take absolute value. Needed before smooth if amplitudes are negative!
  WEIGHT: no need to be weight compliant

requirements:
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: |
          [abs]
          soltab = $(inputs.soltab)
          operation = ABS
      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "losoto"

arguments:
  - '--verbose'
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: lofareosc/prefactor:HBAcalibrator

inputs:
  - id: input_h5parm
    type: File
    format: lofar:#H5Parm
  - id: soltab
    type: string
    doc: "Solution table"

outputs:
  - id: output_h5parm
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.input_h5parm.basename)


$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
