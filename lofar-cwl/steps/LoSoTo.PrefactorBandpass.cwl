class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  lofar: 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
  sbg: 'https://www.sevenbridges.com/'
id: losoto_prefactor_bandpass
baseCommand:
  - losoto
inputs:
  - format: 'lofar:#H5Parm'
    id: input_h5parm
    type: File
  - id: soltab
    type: string
    doc: Solution table
  - id: chanWidth
    type: string?
    doc: >
      the width of each channel in the data from which solutions were obtained.
      Can be

      either a string like "48kHz" or a float in Hz. If interpolate = True,
      chanWidth

      must be specified
  - id: BadSBList
    type: string?
    doc: a list of bad subbands that will be flagged
  - id: outSoltabName
    type: string?
    doc: >
      Name of the output bandpass soltab. An existing soltab with this name will
      be

      overwritten
  - default: false
    id: interpolate
    type: boolean?
    doc: >
      If True, interpolate to a regular frequency grid and then smooth, ignoring
      bad

      subbands. If False, neither interpolation nor smoothing is done and the
      output

      frequency grid is the same as the input one. If interpolate = True,
      chanWidth

      must be specified
  - id: removeTimeAxis
    type: boolean?
    doc: >
      If True, the time axis of the output bandpass soltab is removed by doing a
      median

      over time. If False, the output time grid is the same as the input one
  - id: autoFlag
    type: boolean?
    doc: 'If True, automatically flag bad frequencies and stations'
  - id: nSigma
    type: float?
    doc: >-
      Number of sigma for autoFlagging. Amplitudes outside of nSigma*stddev are
      flagged
  - id: maxFlaggedFraction
    type: float?
    doc: >
      Maximum allowable fraction of flagged frequencies for autoFlagging.
      Stations with

      higher fractions will be completely flagged
  - id: maxStddev
    type: float?
    doc: Maximum allowable standard deviation for autoFlagging
  - id: ncpu
    type: int?
    doc: Number of CPUs to use during autoFlagging (0 = all)
outputs:
  - id: output_h5parm
    type: File
    outputBinding:
      glob: $(inputs.input_h5parm.basename)
    format: 'lofar:#H5Parm'
doc: |
  This operation for LoSoTo implements the Prefactor bandpass operation
  WEIGHT: flag-only compliant, no need for weight
arguments:
  - $(inputs.input_h5parm.basename)
  - parset.config
requirements:
  - class: InitialWorkDirRequirement
    listing:
      - entryname: parset.config
        entry: $(get_losoto_config('PREFACTOR_BANDPASS').join('\n'))
        writable: false
      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true
  - class: InlineJavascriptRequirement
    expressionLib:
      - |
        function get_losoto_config(step_name) {
            var par = ['soltab = ' + inputs.soltab]
            if (inputs.ncpu !== null && inputs.ncpu !== undefined) par.push('ncpu='+inputs.ncpu);
            console.log(inputs, par)
            par.push("[" + step_name + "]")
            par.push('operation=' + step_name)
            for(var field_name in inputs){
                if(field_name === 'input_h5parm' ||
                   field_name === 'soltab' ||
                   field_name === 'ncpu' || 
                   field_name === 'execute') continue;

                if(inputs[field_name] === null ||
                   inputs[field_name] === 'null') continue;
                
                if(inputs[field_name]["class"] !== undefined &&
                   (inputs[field_name]["class"] ==="Directory" ||
                    inputs[field_name]["class"] ==="File")){
                    par.push(field_name+'='+inputs[field_name].path)
                } else {
                    par.push(field_name+'='+inputs[field_name])
                }
            }
            return par
        }
hints:
  - class: DockerRequirement
    dockerPull: 'lofareosc/prefactor:HBAcalibrator'
$schema:
  - 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
