#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.0
id: losoto_replicate_on_axis

doc: |
  Replace the values along a certain axis taking them from one specific axis
  cell

requirements:
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - entryname: "replicate_axis.py"
        entry: |
          import sys
          from losoto.h5parm import openSoltab
          from losoto.operations import replicateonaxis

          soltab_address = "$(inputs.soltab)"
          soltab = openSoltab(sys.argv[1], address=soltab_address)

          axisReplicate = "$(inputs.axisReplicate)"
          fromCell = "$(inputs.fromCell !== null ? inputs.fromCell : 0)"
          updateWeights = "$(inputs.updateWeights? inputs.updateWeights : 'True')"

          replicateonaxis.run(soltab, axisReplicate=axisReplicate,
                                      fromCell=fromCell,
                                      updateWeights=updateWeights)

      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "python3"

arguments:
  - replicate_axis.py
  - $(inputs.input_h5parm.basename)


hints:
  DockerRequirement:
    dockerPull: lofareosc/prefactor:HBAcalibrator

inputs:
  - id: input_h5parm
    type: File
    format: lofar:#H5Parm
  - id: soltab
    type: string
    doc: "Solution table"
  - id: axisReplicate
    type: string
    doc: Axis along which replicate the values.
  - id: fromCell
    type: string?
    doc: A cell value in axisReplicate from which to copy the data values.
  - id: updateWeights
    type: boolean?
    doc: |
      If False then weights are untouched, if True they are replicated
      like data. Default: True.

outputs:
  - id: output_h5parm
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.input_h5parm.basename)


$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
