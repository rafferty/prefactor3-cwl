class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  lofar: 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
  sbg: 'https://www.sevenbridges.com/'
id: calib_rot_diag
baseCommand:
  - DPPP
inputs:
  - id: msin
    type: Directory?
    inputBinding:
      position: 0
      prefix: msin=
      separate: false
    doc: Input Measurement Set
  - default: DATA
    id: msin_datacolumn
    type: string
    inputBinding:
      position: 0
      prefix: msin.datacolumn=
      separate: false
    doc: Input data Column
  - default: MODEL_DATA
    id: msin_modelcolum
    type: string
    inputBinding:
      position: 0
      prefix: msin.modelcolumn=
      separate: false
    doc: Model data Column
  - default: instrument.h5
    id: output_name_h5parm
    type: string
    inputBinding:
      position: 0
      prefix: ddecal.h5parm=
      separate: false
  - default: out.MS
    id: msout_name
    type: string
    inputBinding:
      position: 0
      prefix: msout=
      separate: false
    doc: Output Measurement Set
  - default: true
    id: propagate_solutions
    type: boolean
    inputBinding:
      position: 0
      prefix: propagatesolutions=True
  - default: false
    id: flagunconverged
    type: boolean
    inputBinding:
      position: 0
      prefix: flagdivergedonly=True
    doc: |
      Flag unconverged solutions (i.e., those from solves that did not converge
      within maxiter iterations).
  - default: false
    id: flagdivergedonly
    type: boolean
    inputBinding:
      position: 0
      prefix: flagdivergedonly=True
    doc: |
      Flag only the unconverged solutions for which divergence was detected.
      At the moment, this option is effective only for rotation+diagonal
      solves, where divergence is detected when the amplitudes of any station
      are found to be more than a factor of 5 from the mean amplitude over all
      stations.
      If divergence for any one station is detected, all stations are flagged
      for that solution interval. Only effective when flagunconverged=true
      and mode=rotation+diagonal.
outputs:
  - id: msout
    doc: Output Measurement Set
    type: Directory
    outputBinding:
      glob: '$(inputs.msout_name=="."?inputs.msin:inputs.msout_name)'
  - id: h5parm
    doc: Filename of output H5Parm (to be read by e.g. losoto)
    type: File
    outputBinding:
      glob: $(inputs.output_name_h5parm)
    format: 'lofar:#H5Parm'
arguments:
  - 'steps=[ddecal,count]'
  - ddecal.mode=rotation+diagonal
  - ddecal.uvlambdamin=300
  - ddecal.maxiter=50
  - ddecal.nchan=1
  - ddecal.solint=1
  - ddecal.propagateconvergedonly=True
  - ddecal.flagdivergedonly=True
  - ddecal.tolerance=1.e-3
  - ddecal.usemodelcolumn=True
hints:
  - class: DockerRequirement
    dockerPull: 'lofareosc/prefactor:HBAcalibrator'
requirements:
  - class: InlineJavascriptRequirement
$schema:
  - 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
