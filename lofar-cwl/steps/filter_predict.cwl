class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: predict
baseCommand:
  - DPPP
inputs:
  - id: msin
    type: Directory
    inputBinding:
      position: 0
      prefix: msin=
      separate: false
    doc: Input Measurement Set
  - default: DATA
    id: msin_datacolumn
    type: string
    inputBinding:
      position: 0
      prefix: msin.datacolumn=
      separate: false
    doc: Input data Column
  - default: MODEL_DATA
    id: msout_datacolumn
    type: string
    inputBinding:
      position: 0
      prefix: msout.datacolumn=
      separate: false
  - id: sources_db
    type: File
    inputBinding:
      position: 0
      prefix: predict.sourcedb=
      separate: false
  - default: null
    id: sources
    type: 'string[]'
    inputBinding:
      position: 0
      prefix: predict.sources=
      separate: false
      itemSeparator: ','
      valueFrom: "[$(self.join(','))]"
  - default: false
    id: usebeammodel
    type: boolean
    inputBinding:
      position: 0
      prefix: predict.usebeammodel=True
  - default: false
    id: onebeamperpatch
    type: boolean
    inputBinding:
      position: 0
      prefix: predict.onebeamperpatch=True
  - default: null
    id: filter_baselines
    type: string
    inputBinding:
      position: 0
      prefix: filter.baseline=
      separate: false
      valueFrom: '$(self)'
  - default: false
    id: filter_remove
    type: boolean
    inputBinding:
      position: 0
      prefix: filter.remove=True
  - id: writefullresflag
    type: boolean
    default: false
    inputBinding:
       prefix: msout.writefullresflag=True
  - id: overwrite
    type: boolean
    default: false
    inputBinding:
       prefix: msout.overwrite=True
  - id: storagemanager
    type: string
    default: ""
    inputBinding:
       prefix: msout.storagemanager=
       separate: false
  - id: databitrate
    type: int?
    inputBinding:
       prefix: msout.storagemanager.databitrate=
       separate: false
outputs:
  - id: msout
    doc: Output Measurement Set
    type: Directory
    outputBinding:
      glob: $(inputs.msin.basename)
  - id: logfile
    type: File[]
    outputBinding:
      glob: 'filter_predict*.log'
arguments:
  - 'steps=[filter,predict,count]'
  - predict.beammode=array_factor
  - predict.usechannelfreq=False
  - msout=.
requirements:
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
  - class: InlineJavascriptRequirement
hints:
  - class: DockerRequirement
    dockerPull: lofareosc/prefactor:HBAcalibrator
stdout: filter_predict.log
stderr: filter_predict_err.log