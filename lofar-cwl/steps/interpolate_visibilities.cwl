class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: interpolate_visibilities
baseCommand:
  - DPPP
inputs:
  - id: msin
    type: Directory
    inputBinding:
      position: 0
      prefix: msin=
      separate: false
  - default: Dysco
    id: storage_manager
    type: string
    inputBinding:
      position: 0
      prefix: msout.storagemanager=
      separate: false
  - default: 0
    id: storage_manager_databitrate
    type: int
    inputBinding:
      position: 0
      prefix: msout.storagemanager.databitrate=
      separate: false
  - id: window_size
    type: int
    inputBinding:
      position: 0
      prefix: interpolate.windowsize=
      separate: false
outputs:
  - id: msout
    type: Directory
    outputBinding:
      glob: $(inputs.msin.basename)
label: interpolate_visibilities
arguments:
  - msout.datacolumn=INTERP_DATA
  - msout=.
  - msin.datacolumn=DATA
  - 'steps=[interpolate]'
requirements:
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
  - class: InlineJavascriptRequirement
hints:
  - class: DockerRequirement
    dockerPull: 'lofareosc/prefactor:HBAcalibrator'
