class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: make_sourcedb
baseCommand:
  - makesourcedb
inputs:
  - id: sky_model
    type:
      - File
      - string
    default: '/data/skymodels/Ateam_LBA_CC.skymodel'
    inputBinding:
      position: 0
      prefix: in=
      separate: false
  - default: Ateam.sourcedb
    id: output_file_name
    type: string?
    inputBinding:
      position: 1
      prefix: out=
      separate: false
      valueFrom: $(inputs.output_file_name)
  - default: blob
    id: outtype
    type: string?
    inputBinding:
      position: 2
      prefix: outtype=
      separate: false
  - default: <
    id: format
    type: string?
    inputBinding:
      position: 3
      prefix: format=
      separate: false
  - default: make_sourcedb.log
    id: logname
    type: string?
outputs:
  - id: sourcedb
    type:
      - Directory
      - File
    outputBinding:
      glob: $(inputs.output_file_name)
  - id: log
    type: File?
    outputBinding:
      glob: $(inputs.logname)
label: make_sourcedb_ateam
hints:
  - class: DockerRequirement
    dockerPull: 'lofareosc/prefactor:HBAcalibrator'
stdout: $(inputs.logname)
requirements:
  - class: InlineJavascriptRequirement
