class: CommandLineTool
cwlVersion: v1.1
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: ms_concat
baseCommand:
  - /bin/bash
  - script.sh
inputs:
  - default: 50
    id: min_length
    type: int?
    inputBinding:
      position: 0
      prefix: '--min_length'
    doc: Minimum amount of subbands to concatenate in frequency.
  - id: overhead
    type: float?
    default: 0.8
    inputBinding:
      position: 0
      prefix: '--overhead'
    doc: |
      Only use this fraction of the available memory for deriving the amount
      of data to be concatenated.
  - id: msin
    type: 'Directory[]'
#    inputBinding:
#      position: 1
#      shellQuote: false
  - default: out.MS
    id: msout
    type: string
    inputBinding:
      position: 2
outputs:
  - id: concat_meta_ms
    type: 'Directory'
    outputBinding:
      glob: |
        workdir/$(inputs.msout)_[0-9*]
  - id: concat_additional_ms
    type: 'Directory'
    outputBinding:
      glob: |
        workdir/$(inputs.msout)_*_CONCAT
  - id: ms_outs
    type: 'Directory[]'
    outputBinding:
       glob: 'workdir/out_*'
#      outputEval: $(inputs.msin)
  - id: logfile
    type: File?
    outputBinding:
      glob: ms_concat.log
label: ms_concat
hints:
  - class: DockerRequirement
    dockerPull: 'lofareosc/prefactor:HBAcalibrator'
requirements:
  - class: ShellCommandRequirement
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
      - entryname: script.sh
        entry: | 
          #!/bin/bash
          directories=\$(ls -d out_*)
          #echo \$directories
          mkdir -pv workdir
          cp -r \$directories workdir/. && cd workdir
          python3 /usr/local/bin/concat_MS_CWL.py \$1 \$2 \$3 \$4 \$directories \$5
  - class: InplaceUpdateRequirement
    inplaceUpdate: true
  - class: InlineJavascriptRequirement
stdout: ms_concat.log



#          
#          mkdir -pv dirs && cp -rv \$directories dirs/. && cd dirs
#          directories=\$(ls -d out_*)
#          

      #- entryname: script.sh
      #  entry: |
      #    #!/bin/bash
      #    
      #     ls

  #- python3
  #- /usr/local/bin/concat_MS_CWL.py
#            directories=$(ls out_*)
#          python3 /usr/local/bin/concat_MS_CWL.py $@  $directories

