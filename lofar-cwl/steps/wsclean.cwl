class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: wsclean
baseCommand:
  - wsclean
inputs:
  - id: msin
    type: 'Directory[]'
    inputBinding:
      position: 2
      shellQuote: false
      itemSeparator: ','
      valueFrom: $(concatenate_path_wsclean(self))
  - id: image_size
    default:
      - 1000
      - 1000
    type: 'int[]'
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-size'
  - id: image_scale
    default: '36asec'
    type: string?
    inputBinding:
      position: 1
      prefix: '-scale'
      shellQuote: false
  - id: niter
    default: 10000
    type: int?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-niter'
  - id: nmiter
    default: false
    type: 
      - boolean?
      - int?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-nmiter'
  - id: auto_threshold
    default: false
    type: 
      - boolean?
      - int?
    inputBinding:
      shellQuote: false
      position: 1
      prefix: '-auto-threshold'
  - id: multiscale
    default: false
    type: 
      - boolean?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-multiscale'
  - id: mgain
    default: false
    type: 
      - boolean?
      - float?
    inputBinding:
      shellQuote: false
      position: 1
      prefix: '-mgain'
  - id: ncpu
    default: false
    type: 
      - boolean?
      - int?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-j'
  - id: parallel-gridding
    default: false
    type: 
      - boolean?
      - int?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-parallel-gridding'
  - id: parallel-deconvolution
    default: false
    type: 
      - boolean?
      - int?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-parallel-deconvolution'
  - id: parallel-reordering
    default: false
    type: 
      - boolean?
      - int?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-parallel-reordering'
  - id: channels-out
    default: false
    type: 
      - boolean?
      - int?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-channels-out'
  - id: join-channels
    default: false
    type: boolean?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-join-channels'
  - id: taper-gaussian
    default: false
    type: 
      - boolean?
      - string?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-taper-gaussian'
  - id: weighting
    default: false
    type: 
      - boolean?
      - string?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-weight'
  - id: maxuvw-m
    default: false
    type: 
      - boolean?
      - int?
    inputBinding:
      position: 1
      prefix: '-maxuvw-m'
      shellQuote: false
  - id: tempdir
    type: string?
    default: '/tmp/'
    inputBinding:
      position: 1
      prefix: '-temp-dir'
      shellQuote: false
  - id: model_update
    default: true
    type: boolean
    inputBinding:
      position: 1
      prefix: '-no-update-model-required'
      shellQuote: false
  - id: image_name
    default: 'image'
    type: string
    inputBinding:
      position: 1
      prefix: '-name'
      shellQuote: false
outputs:
  - id: dirty_image
    type: File
    outputBinding:
      glob: $(inputs.image_name)-MFS-dirty.fits
  - id: image
    type: File
    outputBinding:
      glob: $(inputs.image_name)-MFS-image.fits
  - id: logfile
    type: 'File[]'
    outputBinding:
      glob: 'wsclean*.log'
label: WSClean
hints:
  - class: DockerRequirement
    dockerPull: 'lofareosc/prefactor:HBAcalibrator'
  - class: InitialWorkDirRequirement
    listing:
      - $(inputs.msin)
requirements:
  - class: ShellCommandRequirement
  - class: InlineJavascriptRequirement
    expressionLib:
      - { $include: 'utils.js' }

stdout: wsclean.log
stderr: wsclean_err.log