class: Workflow
cwlVersion: v1.0
id: myworkflow
label: MyWorkflow
$namespaces:
  sbg: 'https://www.sevenbridges.com'
inputs:
  - id: msin
    type: Directory
    'sbg:x': -379
    'sbg:y': -172.47381591796875
  - id: strategy
    type: File?
    'sbg:x': -555.515625
    'sbg:y': -628.5
outputs:
  - id: msout
    outputSource:
      - dppp/msout
    type: Directory
    'sbg:x': 108.00761413574219
    'sbg:y': -379
steps:
  - id: dppp
    in:
      - id: parset
        source: aoflagger_step_generator/output_parset
      - id: msin
        source: msin
      - id: secondary_files
        source:
          - aoflagger_step_generator/output_secondary_files
    out:
      - id: msout
    run: steps/DPPPStep.cwl
    'sbg:x': -77.87055969238281
    'sbg:y': -458.0888366699219
  - id: average_step_generator
    in:
      - id: input_parset
        source: empty_parset_generator/output_parset
      - id: input_secondary_files
        source:
          - empty_parset_generator/output_secondary_files
    out:
      - id: output_parset
      - id: output_secondary_files
    run: steps/AveragingStepGenerator.cwl
    'sbg:x': -659.6497192382812
    'sbg:y': -479.365478515625
  - id: aoflagger_step_generator
    in:
      - id: input_parset
        source: average_step_generator/output_parset
      - id: input_secondary_files
        source:
          - average_step_generator/output_secondary_files
      - id: strategy
        source: strategy
    out:
      - id: output_parset
      - id: output_secondary_files
    run: steps/AOFlaggerStepGenerator.cwl
    'sbg:x': -393.4390869140625
    'sbg:y': -485.9670104980469
  - id: empty_parset_generator
    in: []
    out:
      - id: output_parset
      - id: output_secondary_files
    run: steps/EmptyParsetGenerator.cwl
    'sbg:x': -903.7893676757812
    'sbg:y': -480.0863037109375
requirements:
  - class: MultipleInputFeatureRequirement
