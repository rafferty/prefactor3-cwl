class: Workflow
cwlVersion: 'sbg:draft-2'
id: applybeam
label: applybeam
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - type:
      - 'null'
      - boolean
    id: '#usechannelfreq'
    'sbg:includeInPorts': true
    'sbg:x': -491.39886474609375
    'sbg:y': -275.5
  - type:
      - 'null'
      - boolean
    id: '#updateweights'
    'sbg:includeInPorts': true
    'sbg:x': -595.3988647460938
    'sbg:y': -140.5
  - type:
      - 'null'
      - string
    id: '#onebeamperpatch'
    'sbg:includeInPorts': true
    'sbg:x': -609.3988647460938
    'sbg:y': -22.5
  - type:
      - 'null'
      - boolean
    id: '#invert'
    'sbg:includeInPorts': true
    'sbg:x': -568.3988647460938
    'sbg:y': 102.5
  - type:
      - 'null'
      - string
    id: '#direction'
    'sbg:includeInPorts': true
    'sbg:x': -472.39886474609375
    'sbg:y': 211.5
  - type:
      - type: enum
        symbols:
          - array_factor
          - element
          - default
        name: ''
    id: '#beammode'
    'sbg:includeInPorts': true
    'sbg:x': -455
    'sbg:y': 378
  - type:
      - Directory
    id: '#msin'
    'sbg:includeInPorts': true
    'sbg:x': -203
    'sbg:y': 424
outputs:
  - id: '#msout'
    source:
      - '#dp3_execute.msout'
    type:
      - Directory
    'sbg:x': 429.60113525390625
    'sbg:y': 267.5
steps:
  - id: '#applybeam_step_generator'
    inputs:
      - id: '#applybeam_step_generator.direction'
        source: '#direction'
      - id: '#applybeam_step_generator.onebeamperpatch'
        source: '#onebeamperpatch'
      - id: '#applybeam_step_generator.usechannelfreq'
        source: '#usechannelfreq'
      - id: '#applybeam_step_generator.updateweights'
        source: '#updateweights'
      - id: '#applybeam_step_generator.invert'
        source: '#invert'
      - id: '#applybeam_step_generator.beammode'
        source: '#beammode'
    outputs:
      - id: '#applybeam_step_generator.augmented_steps'
    run: ../lofar-cwl/steps/DP3.ApplyBeamStepGenerator.cwl
    'sbg:x': -30
    'sbg:y': 115
  - id: '#dp3_execute'
    inputs:
      - id: '#dp3_execute.msin'
        source: '#msin'
      - id: '#dp3_execute.steps'
        source:
          - '#applybeam_step_generator.augmented_steps'
    outputs:
      - id: '#dp3_execute.secondary_output_files'
      - id: '#dp3_execute.secondary_output_directories'
      - id: '#dp3_execute.msout'
    run: ../lofar-cwl/steps/DP3.Execute.cwl
    label: DP3.Execute
    'sbg:x': 208
    'sbg:y': 127
doc: ''
