class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: bulk_rename
baseCommand:
  - bash 
  - bulk_rename.sh
inputs:
  - id: file_list
    type: 'File[]'
    inputBinding:
      position: 0
  - id: file_prefix
    type: string
  - id: file_suffix
    type: string?
outputs:
  - id: output
    type: 'File[]'
    outputBinding:
      glob: "tmp/$(inputs.file_prefix)*"
label: bulk_rename
requirements:
  - class: InitialWorkDirRequirement
    listing:
      - entryname: bulk_rename.sh
        entry: |
          #!/bin/bash
          set -e
          FILE_LIST=("\${@}")
          FILE_PREFIX=$(inputs.file_prefix)
          FILE_SUFFIX=$(inputs.file_suffix === null ? '' : inputs.file_suffix)
          mkdir tmp
          for i in "\${!FILE_LIST[@]}"; do 
            cp "\${FILE_LIST[\$i]}" "tmp/\${FILE_PREFIX}_\${i}\${FILE_SUFFIX}"
          done
        writable: false
  - class: InlineJavascriptRequirement