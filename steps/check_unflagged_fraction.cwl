class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: check_unflagged_fraction
baseCommand:
  - python3
inputs:
    - id: msin
      type: Directory
      doc: MS to check
      inputBinding:
        position: 0
    - id: min_fraction
      type: float?
      default: 0.5
      doc: Fraction of flagged data in a MS to be acceptable

label: check_unflagged_fraction
arguments:
  - '-c'
  - |
    import sys
    import json
    import shutil
    import os

    from check_unflagged_fraction import main as check_unflagged_fraction

    ms = sys.argv[1]
    min_fraction = float($(inputs.min_fraction))
    
    output = check_unflagged_fraction(ms, min_fraction=min_fraction, print_fraction=True)

    filename           = os.path.basename(output['flagged'])
    unflagged_fraction = output['unflagged_fraction']

    if filename != 'None':
        shutil.move(filename, filename.lstrip('out_'))
        
    cwl_output = {}
    cwl_output['unflagged_fraction'] = unflagged_fraction

    with open('./out.json', 'w') as fp:
        json.dump(cwl_output, fp)
        
outputs:
  - id: msout
    type: Directory[]
    outputBinding:
        glob: 'L*.dpppconcat'
  - id: unflagged_fraction
    type: float
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).unflagged_fraction)
  - id: logfile
    type: File[]?
    outputBinding:
      glob: 'check_unflagged_fraction*.log'
        
requirements:
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true

hints:
  - class: DockerRequirement
    dockerPull: lofareosc/prefactor:HBAcalibrator
stdout: check_unflagged_fraction.log
stderr: check_unflagged_fraction_err.log