class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: compare_station_list
baseCommand:
  - python3
inputs:
    - id: msin
      type: Directory[]
      doc: MS to compare with
      inputBinding:
        position: 0
    - id: h5parmdb
      type: File
      doc: H5parm database to compare with
    - id: solset_name
      type: string?
      doc: Name of the H5parm solset
      default: 'sol000'
    - id: filter
      type: string?
      default: '*&'
      doc: Filter these baselines for the comparison

label: compareStationList.py
arguments:
  - '-c'
  - |
    import sys
    import json
    from compareStationList import main as compareStationList
    
    mss = sys.argv[1:]
    h5parmdb = $(inputs.h5parmdb)['path']
    solset_name = '$(inputs.solset_name)'
    filter = '$(inputs.filter)'

    output = compareStationList(mss, h5parmdb, solset_name, filter)

    filter_out = output['filter']
    cwl_output = {"filter_out": filter_out}

    with open('./out.json', 'w') as fp:
        json.dump(cwl_output, fp)
outputs:
  - id: filter_out
    type: string
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).filter_out)
  - id: logfile
    type: File[]?
    outputBinding:
      glob: 'compareStationList*.log'
        
requirements:
  - class: InlineJavascriptRequirement

hints:
  DockerRequirement:
    dockerPull: lofareosc/prefactor:HBAcalibrator
stdout: compareStationList.log
stderr: compareStationList_err.log