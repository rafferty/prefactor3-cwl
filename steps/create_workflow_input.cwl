$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: create_workflow_input
label: create_workflow_input
class: CommandLineTool
cwlVersion: v1.0
inputs: 
  - id: msin
    type: Directory[]
    inputBinding:
      position: 0
    
outputs: 
  - id: output
    type: File
    outputBinding:
      glob: workflow_input.json
      
baseCommand: 
  - python3
  - script.py
doc: ''
requirements:
  InitialWorkDirRequirement:
    listing:
      - entryname: script.py
        entry: | 
          #!/usr/bin/env python3
          
          import sys
          import json
          import os
          dir_list = sys.argv[1:]
          rendered_leafs = []
          
          for dir in dir_list:
              file_name = dir.split(os.path.sep)[-1]
              rendered_leaf = {'class': 'Directory', 'path': file_name}
              rendered_leafs.append(rendered_leaf)  
            
          with open('workflow_input.json', 'w') as fp:
              json.dump({'msin': rendered_leafs}, fp, indent=4)