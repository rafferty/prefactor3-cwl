class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: directory_listing
baseCommand:
  - find
inputs:
  - id: base_directory
    type: Directory
    inputBinding:
      position: 0
  - id: name
    type: string
    inputBinding:
      position: 2
      prefix: '-name'
outputs:
  - id: output
    type: 'Directory[]'
    outputBinding:
      loadContents: true
      glob: out_file
      outputEval: |-
        ${
            var paths = self[0].contents.split("\n")

            var directories = []
            for(var path_idx in paths){
                var path = paths[path_idx]
                if(path === '') continue
                directories.push(
                    {"class": "Directory",
                     "path": path
                    }
                )
            }
            return directories
        }
label: DirectoryListing
arguments:
  - position: 1
    prefix: '-maxdepth'
    valueFrom: '1'
  - position: 1
    prefix: '-type'
    valueFrom: d
hints:
  - class: DockerRequirement
    dockerPull: 'lofareosc/prefactor-ci:master'
requirements:
  - class: InlineJavascriptRequirement
stdout: out_file
