class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: findRefAnt
baseCommand:
  - python3
inputs:
    - id: msin
      type: Directory
      doc: MS to compare with
      inputBinding:
        position: 0

label: findRefAnt.py
arguments:
  - '-c'
  - |
    import sys
    import json
    from findRefAnt import main as findRefAnt
    
    ms = sys.argv[1]

    flagged_fraction_dict = str(findRefAnt(ms))

    cwl_output  = {"flagged_fraction_dict": flagged_fraction_dict}

    with open('./out.json', 'w') as fp:
        json.dump(cwl_output, fp)
        
outputs:
  - id: flagged_fraction_dict
    type: string
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).flagged_fraction_dict)
  - id: logfile
    type: File?
    outputBinding:
      glob: findRefAnt.log    
        
requirements:
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
hints:
  - class: DockerRequirement
    dockerPull: lofareosc/prefactor:HBAcalibrator
stdout: findRefAnt.log
stderr: findRefAnt_err.log