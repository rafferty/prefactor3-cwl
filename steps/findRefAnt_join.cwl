class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: findRefAnt_join
baseCommand:
  - python3
  - script.py
inputs:
    - id: flagged_fraction_dict
      type: string[]?
      default: []
      doc: list of flagged antennas per MS
    - id: filter_station
      type: string?
      default: '*&'
      doc: Filter these baselines for the comparison
      
label: findRefAnt_join

outputs:
  - id: refant
    type: string
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).refant)
  - id: logfile
    type: File?
    outputBinding:
      glob: findRefAnt.log    
        
requirements:
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
      - entryname: input.json
        entry: $(inputs.flagged_fraction_dict)
      - entryname: script.py
        entry: |
            import sys
            import json
            import re
            import ast
    
            with open('input.json', 'r') as f_stream:
                flagged_fraction_dict_list = json.load(f_stream)
            filter_station = '$(inputs.filter_station)'
            no_station_selected = True
    
            while no_station_selected:
                print('Applying station filter ' + str(filter_station))
                flagged_fraction_data = {}
                no_station_selected = False
                for flagged_fraction_dict in flagged_fraction_dict_list:
                    entry = ast.literal_eval(flagged_fraction_dict)
                    antennas = entry.keys()
                    selected_stations = [ station_name for station_name in antennas if re.match(filter_station, station_name) ]
                    if len(selected_stations) == 0:
                        print('No stations left after filtering. Station(s) do(es) not exist in all subbands. No filter is used.')
                        filter_station = ''
                        no_station_selected = True
                        break
                    for antenna in selected_stations:
                        try:
                            flagged_fraction_data[antenna].append(float(entry[antenna]))
                        except KeyError:
                            flagged_fraction_data[antenna] = [float(entry[antenna])]

            flagged_fraction_list = []
            sorted_stations = sorted(flagged_fraction_data.keys())

            for antenna in sorted_stations:
                flagged_fraction = sum(flagged_fraction_data[antenna]) / len(flagged_fraction_data[antenna])
                flagged_fraction_list.append(flagged_fraction)
                try:
                    flagged_fraction_data[flagged_fraction].append(antenna)
                except KeyError:
                    flagged_fraction_data[flagged_fraction] = [antenna]

            min_flagged_fraction = min(flagged_fraction_list)
            refant = flagged_fraction_data[min_flagged_fraction][0]
            print('Selected station ' + str(refant) + ' as reference antenna. Fraction of flagged data is ' + '{:>3}'.format('{:.1f}'.format(min_flagged_fraction) + '%'))

            cwl_output = {'refant': str(refant)}

            with open('./out.json', 'w') as fp:
                json.dump(cwl_output, fp)
            
stdout: findRefAnt.log
stderr: findRefAnt_err.log
