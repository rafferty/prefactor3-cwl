class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: find_skymodel_target_py
baseCommand:
  - python3
inputs:
    - id: msin
      type: Directory[]
      doc: MS containing the target
      inputBinding:
        position: 0
    - id: SkymodelPath
      type: File?
      doc: File containing or putting the skymodel
    - id: Radius
      type: float?
      doc: Radius of the skymodel
      default: 5.0
    - id: Source
      type: string?
      doc: Source of the skymodel
      default: 'TGSS'
    - id: DoDownload
      type: boolean?
      doc: Download a new skymodel if given path is empty
      default: true

label: find_skymodel_target.py
arguments:
  - '-c'
  - |
    import sys
    import shutil
    import os
    null = None

    from download_skymodel_target import main as download_skymodel_target

    mss = sys.argv[1:]

    SkymodelPath = $(inputs.SkymodelPath)
    if SkymodelPath is None:
        SkymodelPath = os.getcwd() + "/target.skymodel"
    else:
        SkymodelPath = SkymodelPath["path"]

    Radius = $(inputs.Radius)
    Source = "$(inputs.Source)"
    DoDownload = "$(inputs.DoDownload)"
    
    output = download_skymodel_target(mss, SkymodelPath, Radius, DoDownload, Source)
    
    if "target.skymodel" not in SkymodelPath:
       shutil.copyfile(SkymodelPath, "target.skymodel")
   
outputs:
  - id: skymodel
    type: File?
    outputBinding:
      glob: target.skymodel
  - id: logfile
    type: File?
    outputBinding:
      glob: find_skymodel_target.log    
        
requirements:
  - class: InlineJavascriptRequirement

hints:
  DockerRequirement:
    dockerPull: lofareosc/prefactor:HBAcalibrator
stdout: find_skymodel_target.log