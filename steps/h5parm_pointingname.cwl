class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  lofar: 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
  sbg: 'https://www.sevenbridges.com/'
id: h5parm_pointingname
baseCommand:
  - python3
  - /usr/local/bin/h5parm_pointingname.py
inputs:
  - format: 'lofar:#H5Parm'
    id: h5parmFile
    type: File
    inputBinding:
      position: 0
    doc: List of h5parm files
  - default: 'target'
    id: solsetName
    type: string?
    inputBinding:
      position: 0
      prefix: '--solsetName'
    doc: Input solset name
  - default: 'POINTING'
    id: pointing
    type: string?
    inputBinding:
      position: 0
      prefix: '--pointing'
    doc: Name of the pointing
outputs:
  - id: outh5parm
    doc: Output h5parm
    type: File
    outputBinding:
      glob: $(inputs.h5parmFile.basename)
    format: lofar:#H5Parm
  - id: log
    type: File[]
    outputBinding:
      glob: 'h5parm_pointingname*.log'
label: h5parm_pointingname
requirements:
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.h5parmFile)
        writable: true
  - class: InlineJavascriptRequirement
hints:
  - class: DockerRequirement
    dockerPull: lofareosc/prefactor:HBAcalibrator
stdout: h5parm_pointingname.log
stderr: h5parm_pointingname_err.log
$schema:
  - 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
