class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: identify_bad_antennas_join
baseCommand:
  - python3
inputs:
    - id: flaggedants
      type: string[]?
      default: []
      doc: list of flagged antennas
    - id: filter
      type: string?
      default: '*&'
      doc: Filter these baselines for the comparison

label: identifyBadAntennas_join
arguments:
  - '-c'
  - |

    import sys
    import json

    flaggedants = $(inputs.flaggedants)
    filter = '$(inputs.filter)'


    flaggedants_list = [ flaggedant.split(',') for flaggedant in flaggedants ]
    flagged_antenna_list = set.intersection(*map(set, flaggedants_list))

    for flagged_antenna in flagged_antenna_list:
        if flagged_antenna != '':
            filter += ';!' + flagged_antenna + '*&&*'

    cwl_output  = {"filter": filter}

    with open('./out.json', 'w') as fp:
        json.dump(cwl_output, fp)

outputs:
  - id: filter_out
    type: string
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).filter)
  - id: logfile
    type: File?
    outputBinding:
      glob: identifyBadAntennas.log

requirements:
  - class: InlineJavascriptRequirement

stdout: identifyBadAntennas.log
stderr: identifyBadAntennas_err.log
