class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: plot_Ateamclipper
baseCommand:
  - python3
  - /usr/local/bin/plot_Ateamclipper.py
inputs:
  - id: clipper_output
    type: File
    inputBinding:
      position: 1
  - id: outputimage
    type: string?
    default: Ateamclipper.png
    inputBinding:
      position: 2
outputs:
  - id: output_imag
    doc: Output image
    type: File
    outputBinding:
      glob: $(inputs.outputimage)
label: plot_Ateamclipper
hints:
  - class: DockerRequirement
    dockerPull: lofareosc/prefactor:HBAcalibrator
