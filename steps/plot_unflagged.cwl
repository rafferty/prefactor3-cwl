class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: plot_unflagged
baseCommand:
  - python3
inputs:
  - id: msin
    type: 'Directory[]'
    inputBinding:
      position: 1
  - id: unflagged_fraction
    type: float[]
    inputBinding:
      position: 2
      
label: plot_unflagged
arguments:
  - '-c'
  - |
    import sys
    import os

    from plot_unflagged_fraction import main as plot_unflagged_fraction

    center = int((len(sys.argv) - 1) / 2 + 1)
    
    mss = sys.argv[1:center]
    unflagged_fraction = sys.argv[center:]
    
    output = plot_unflagged_fraction(ms_list = mss, frac_list = unflagged_fraction, outfile = os.getcwd() + '/unflagged_fraction.png')

outputs:
  - id: output_imag
    doc: Output image
    type: File
    outputBinding:
      glob: 'unflagged_fraction.png'
  - id: logfile
    type: File[]?
    outputBinding:
      glob: 'plot_unflagged_fraction*.log'

    
hints:
  - class: DockerRequirement
    dockerPull: 'lofareosc/prefactor:HBAcalibrator'
requirements:
  - class: InlineJavascriptRequirement
stdout: plot_unflagged_fraction.log
stderr: plot_unflagged_fraction_err.log