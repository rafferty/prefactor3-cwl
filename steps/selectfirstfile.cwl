class: ExpressionTool
cwlVersion: v1.0
id: select_first_file
inputs:
  - id: inputs
    type: File[]
    doc: input files
outputs:
  - id: output
    type: File

expression: |
  ${
    return {'output': inputs.inputs[0]}
  }
label: FileSelector

requirements:
  - class: InlineJavascriptRequirement
