class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: separate_results_directory
baseCommand:
  - echo "exploding directory"
inputs:
  - id: input
    type: Directory
    inputBinding:
      shellQuote: false
      position: 0
outputs:
  - id: quality_plots
    type: 'File[]'
    outputBinding:
      glob: $(inputs.input)/inspections/*
  - id: calibrated_ms
    type: 'Directory[]'
    outputBinding:
      glob: $(inputs.input)/out_*
  - id: calibrator_solutions
    type: File
    outputBinding:
      glob: |
        $(inputs.input)/cal_values/*
label: separate_results_directory
requirements:
  - class: ShellCommandRequirement
  - class: InlineJavascriptRequirement
