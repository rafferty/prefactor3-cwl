class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: sort_times_into_freqGroups
baseCommand:
  - python3
inputs:
    - id: msin
      type: Directory[]
      doc: MS to sort
      inputBinding:
        position: 0
    - id: numbands
      type: int?
      default: 10
      doc: Number of how many files should be grouped together in frequency.
    - id: NDPPPfill
      type: boolean?
      default: True
      doc: Add dummy file-names for missing frequencies, so that NDPPP can fill the data with flagged dummy data.
    - id: stepname
      type: string?
      default: '.ms'
      doc: Add this step-name into the file-names of the output files.
    - id: firstSB
      type: int?
      default: null
      doc: If set, then reference the grouping of files to this station-subband. As if a file with this station-subband would be included in the input files.
    - id: mergeLastGroup
      type: boolean?
      default: False
      doc: Add dummy file-names for missing frequencies, so that NDPPP can fill the data with flagged dummy data.
    - id: truncateLastSBs
      type: boolean?
      default: True
      doc: Add dummy file-names for missing frequencies, so that NDPPP can fill the data with flagged dummy data.

label: sort_times_into_freqGroups.py
arguments:
  - '-c'
  - |
    import sys
    import json
    from sort_times_into_freqGroups_CWL import main as sort_times_into_freqGroups
    null = None
    false = False
    true = True
    
    mss = sys.argv[1:]
    numbands = int($(inputs.numbands))
    stepname = "$(inputs.stepname)"
    NDPPPfill = $(inputs.NDPPPfill)
    mergeLastGroup = $(inputs.mergeLastGroup)
    truncateLastSBs = $(inputs.truncateLastSBs)
    firstSB = $(inputs.firstSB)

    output = sort_times_into_freqGroups(mss, numbands, NDPPPfill, stepname, mergeLastGroup, truncateLastSBs, firstSB)

    filenames  = output['filenames']
    groupnames = output['groupnames']
    total_bandwidth = output['total_bandwidth']
    
    cwl_output = {}
    cwl_output['groupnames'] = groupnames
    cwl_output['total_bandwidth'] = total_bandwidth

    with open('./filenames.json', 'w') as fp:
        json.dump(filenames, fp)
        
    with open('./out.json', 'w') as fp:
        json.dump(cwl_output, fp)
        
outputs:
  - id: filenames
    type: File
    outputBinding:
        glob: 'filenames.json'
  - id: groupnames
    type: string[]
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).groupnames)
  - id: total_bandwidth
    type: int
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).total_bandwidth)
  - id: logfile
    type: File?
    outputBinding:
      glob: sort_times_into_freqGroups.log    
        
requirements:
  - class: InlineJavascriptRequirement

hints:
  - class: DockerRequirement
    dockerPull: lofareosc/prefactor:HBAcalibrator
stdout: sort_times_into_freqGroups.log