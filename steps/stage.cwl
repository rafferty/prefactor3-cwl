class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: stage
baseCommand:
  - gfal-legacy-bringonline
inputs:
  - id: input
    type: string
    inputBinding:
      shellQuote: false
      position: 0
outputs:
  - id: output
    type: string
    outputBinding:
      outputEval: $(inputs.input)
label: stage
requirements:
  - class: InlineJavascriptRequirement
