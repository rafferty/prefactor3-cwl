class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  lofar: 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
  sbg: 'https://www.sevenbridges.com/'
id: structure_function
baseCommand:
  - python3
  - /usr/local/bin/getStructure_from_phases.py
inputs:
  - format: 'lofar:#H5Parm'
    id: h5parmFile
    type: File
    inputBinding:
      position: 0
    doc: List of h5parm files
  - default: 'target'
    id: solset
    type: string?
    inputBinding:
      position: 0
      prefix: '--solset'
    doc: Input solset name
  - default: 'phase000'
    id: soltab
    type: string?
    inputBinding:
      position: 0
      prefix: '--soltab'
    doc: Name of the soltab
  - default: 'POINTING'
    id: outbasename
    type: string?
    inputBinding:
      position: 0
      prefix: '--outbasename'
    doc: Namebase of the output files
outputs:
  - id: structure_plot
    doc: Output plot
    type: File
    outputBinding:
      glob: $(inputs.outbasename+'_structure.png')
  - id: structure_txt
    doc: Output text
    type: File
    outputBinding:
      glob: $(inputs.outbasename+'_structure.txt')
  - id: log
    type: File[]
    outputBinding:
      glob: 'structure_function*.log'
label: structure_function
requirements:
  - class: InlineJavascriptRequirement
hints:
  - class: DockerRequirement
    dockerPull: lofareosc/prefactor:HBAcalibrator
stdout: structure_function.log
stderr: structure_function_err.log
$schema:
  - 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
