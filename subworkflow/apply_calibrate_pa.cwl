class: Workflow
cwlVersion: v1.0
id: apply_calibrate
label: apply_calibrate
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: msin
    type: Directory
    'sbg:x': -1000
    'sbg:y': -200
  - id: do_smooth
    type: boolean
    'sbg:x': -1000
    'sbg:y': -100
  - id: flagunconverged
    type: boolean
    'sbg:x': -1000
    'sbg:y': 100
  - id: propagatesolutions
    type: boolean
    'sbg:x': -1000
    'sbg:y': 200
  - id: input_h5parm
    type: File
    'sbg:x': -1000
    'sbg:y': 300
outputs:
  - id: msout
    outputSource:
      - calib_cal/msout
    type: Directory
    'sbg:x': 1000
    'sbg:y': -200
  - id: BLsmooth.log
    outputSource:
      - BLsmooth/logfile
    type: File
    'sbg:x': 1000
    'sbg:y': -100
  - id: apply_cal.log
    outputSource:
      - concat_logfiles_applycal/output
    type: File
    'sbg:x': 1000
    'sbg:y': 0
  - id: calib_cal.log
    outputSource:
      - concat_logfiles_calib_cal/output
    type: File
    'sbg:x': 1000
    'sbg:y': 100
  - id: outh5parm
    outputSource:
      - calib_cal/h5parm
    type: File
    'sbg:x': 1000
    'sbg:y': 200
  - id: applybeam.log
    outputSource:
      - concat_logfiles_applybeam/output
    type: File
    'sbg:x': 1000
    'sbg:y': 300
steps:
  - id: applyPA
    in:
      - id: msin
        source: msin
      - id: msin_datacolumn
        default: DATA
      - id: parmdb
        source: input_h5parm
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: correction
        default: polalign
    out:
      - id: msout
      - id: logfile
    run: ../lofar-cwl/steps/applycal.cwl
    label: applyPA
    'sbg:x': -400
    'sbg:y': 0
  - id: applybeam
    in:
      - id: msin_datacolumn
        default: CORRECTED_DATA
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: updateweights
        default: 'true'
      - id: invert
        default: 'true'
      - id: beammode
        default: element
      - id: usechannelfreq
        default: 'false'
      - id: msin
        source: applyPA/msout
      - id: type
        default: applybeam
    out:
      - id: msout
      - id: logfile
    run: ../lofar-cwl/steps/applybeam.cwl
    label: applybeam
    'sbg:x': -200
    'sbg:y': 0
  - id: BLsmooth
    in:
      - id: msin
        source: applybeam/msout
      - id: do_smooth
        source: do_smooth
      - id: in_column_name
        default: CORRECTED_DATA
    out:
      - id: msout
      - id: logfile
    run: ../steps/blsmooth.cwl
    label: BLsmooth
    'sbg:x': 0
    'sbg:y': 0
  - id: calib_cal
    in:
      - id: msin
        source: BLsmooth/msout
      - id: msin_datacolumn
        default: SMOOTHED_DATA
      - id: msin_modelcolum
        default: MODEL_DATA
      - id: flagunconverged
        source: flagunconverged
      - id: propagate_solutions
        source: propagatesolutions
      - id: mode
        default: rotation+diagonal
    out:
      - id: msout
      - id: h5parm
      - id: logfile
    run: ../lofar-cwl/steps/ddecal.cwl
    'sbg:x': 500
    'sbg:y': 0
  - id: concat_logfiles_calib_cal
    in:
      - id: file_list
        source:
          - calib_cal/logfile
      - id: file_prefix
        default: calib_cal
    out:
      - id: output
    run: ../steps/concatenate_files.cwl
    label: concat_logfiles_calib_cal
    'sbg:x': 700
    'sbg:y': 200
  - id: concat_logfiles_applybeam
    in:
      - id: file_list
        source:
          - applybeam/logfile
      - id: file_prefix
        default: applybeam
    out:
      - id: output
    run: ../steps/concatenate_files.cwl
    label: concat_logfiles_applybeam
    'sbg:x': 0
    'sbg:y': 200
  - id: concat_logfiles_applycal
    in:
      - id: file_list
        source:
          - applyPA/logfile
      - id: file_prefix
        default: applycal
    out:
      - id: output
    run: ../steps/concatenate_files.cwl
    label: concat_logfiles_applycal
    'sbg:x': -200
    'sbg:y': 200
requirements: []
