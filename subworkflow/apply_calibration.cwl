class: Workflow
cwlVersion: v1.0
id: apply_calibration
label: apply_calibration
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: parmdb
    type: File
    'sbg:x': -604.3988647460938
    'sbg:y': -199.5
  - id: msin
    type: Directory
    'sbg:x': -679.001220703125
    'sbg:y': 36
outputs:
  - id: msout
    outputSource:
      - apply_FR/msout
    type: Directory
    'sbg:x': 551.6011352539062
    'sbg:y': -4.501007080078125
steps:
  - id: apply_PA
    in:
      - id: msin
        source: msin
      - id: msin_datacolumn
        default: DATA
      - id: parmdb
        source: parmdb
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: correction
        default: polalign
    out:
      - id: msout
    run: ../lofar-cwl/steps/applycal.cwl
    'sbg:x': -358
    'sbg:y': 4
  - id: apply_bandpass
    in:
      - id: msin
        source: apply_PA/msout
      - id: msin_datacolumn
        default: CORRECTED_DATA
      - id: parmdb
        source: parmdb
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: correction
        default: bandpass
      - id: updateweights
        default: true
    out:
      - id: msout
    run: ../lofar-cwl/steps/applycal.cwl
    'sbg:x': -133
    'sbg:y': -14
  - id: apply_FR
    in:
      - id: msin
        source: applybeam/msout
      - id: msin_datacolumn
        default: CORRECTED_DATA
      - id: parmdb
        source: parmdb
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: correction
        default: faraday
    out:
      - id: msout
    run: ../lofar-cwl/steps/applycal.cwl
    'sbg:x': 287
    'sbg:y': -36
  - id: applybeam
    in:
      - id: updateweights
        default: true
      - id: invert
        default: true
      - id: beammode
        default: element
      - id: msin
        source: apply_bandpass/msout
    out:
      - id: msout
    run: ../steps/applybeam.cwl
    label: applybeam
    'sbg:x': 62
    'sbg:y': -15
requirements:
  - class: SubworkflowFeatureRequirement
