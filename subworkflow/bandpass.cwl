class: Workflow
cwlVersion: v1.0
id: losoto_bandpass
label: losoto_bandpass
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: ampRange
    type: 'float[]?'
    'sbg:x': 397.4866943359375
    'sbg:y': 73.48989868164062
  - id: skipInternational
    type: boolean?
    'sbg:x': 401.4047546386719
    'sbg:y': -185.20863342285156
  - id: input_h5parm
    type: File
    'sbg:x': 395.389404296875
    'sbg:y': -51.39104080200195
  - id: max2interpolate
    type: int?
    'sbg:x': 398.02581787109375
    'sbg:y': -311.48223876953125
  - id: bandpass_freqresolution
    type: string
    'sbg:x': 404.9237365722656
    'sbg:y': -440.521484375
  - id: avg_freqresolution
    type: string?
    'sbg:x': 401.60693359375
    'sbg:y': 198.58273315429688
outputs:
  - id: output_h5parm
    outputSource:
      - smoothb/output_h5parm
    type: File
    'sbg:x': 2315
    'sbg:y': -50
  - id: logfiles
    outputSource:
      - duplicateAbkp/log
      - losoto_flag/log
      - flagbp/log
      - flagextend/log
      - merge/log
      - duplicateAbkp2/log
      - smooth/logfile
      - bandpass/logfile
      - interp/logfile
      - smoothb/logfile
    type: 'File[]'
    linkMerge: merge_flattened
    'sbg:x': 2315.6181640625
    'sbg:y': 352.22503662109375
steps:
  - id: duplicateAbkp
    in:
      - id: input_h5parm
        source: input_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: soltabOut
        default: amplitudeOrig000
    out:
      - id: output_h5parm
      - id: log
    run: ../lofar-cwl/steps/LoSoTo.Duplicate.cwl
    'sbg:x': 600
    'sbg:y': -50
  - id: losoto_flag
    in:
      - id: input_h5parm
        source: duplicateAbkp/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: axesToFlag
        default:
          - time
          - freq
      - id: order
        default:
          - 100
          - 40
      - id: maxCycles
        default: 1
      - id: maxRms
        default: 5
      - id: mode
        default: smooth
      - id: preflagzeros
        default: false
      - id: replace
        default: false
    out:
      - id: output_h5parm
      - id: log
    run: ../lofar-cwl/steps/LoSoTo.Flag.cwl
    label: flag
    'sbg:x': 750
    'sbg:y': -50
  - id: flagbp
    in:
      - id: input_h5parm
        source: losoto_flag/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: mode
        default: bandpass
      - id: ampRange
        source:
          - ampRange
      - id: skipInternational
        source: skipInternational
    out:
      - id: output_h5parm
      - id: log
    run: ../lofar-cwl/steps/LoSoTo.FlagStation.cwl
    'sbg:x': 900
    'sbg:y': -50
  - id: flagextend
    in:
      - id: input_h5parm
        source: flagbp/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: axesToExt
        default:
          - time
          - freq
      - id: size
        default:
          - 200
          - 80
      - id: percent
        default: 50
      - id: maxCycles
        default: 2
    out:
      - id: output_h5parm
      - id: log
    run: ../lofar-cwl/steps/LoSoTo.Flagextend.cwl
    'sbg:x': 1050
    'sbg:y': -50
  - id: merge
    in:
      - id: input_h5parm
        source: flagextend/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: mode
        default: copy
      - id: soltabImport
        default: amplitude000
    out:
      - id: output_h5parm
      - id: log
    run: ../lofar-cwl/steps/LoSoTo.Reweight.cwl
    'sbg:x': 1200
    'sbg:y': -50
  - id: duplicateAbkp2
    in:
      - id: input_h5parm
        source: merge/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: soltabOut
        default: amplitudeOrig001
    out:
      - id: output_h5parm
      - id: log
    run: ../lofar-cwl/steps/LoSoTo.Duplicate.cwl
    'sbg:x': 1350
    'sbg:y': -50
  - id: smooth
    in:
      - id: input_h5parm
        source: duplicateAbkp2/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: axesToSmooth
        default:
          - time
      - id: mode
        default: median
      - id: log
        default: true
      - id: replace
        default: false
    out:
      - id: output_h5parm
      - id: logfile
    run: ../lofar-cwl/steps/LoSoTo.Smooth.cwl
    'sbg:x': 1500
    'sbg:y': -50
  - id: bandpass
    in:
      - id: input_h5parm
        source: smooth/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: axesToSmooth
        default:
          - freq
      - id: size
        default:
          - 17
      - id: mode
        default: savitzky-golay
      - id: degree
        default: 2
      - id: log
        default: true
    out:
      - id: output_h5parm
      - id: logfile
    run: ../lofar-cwl/steps/LoSoTo.Smooth.cwl
    'sbg:x': 1650
    'sbg:y': -50
  - id: interp
    in:
      - id: input_h5parm
        source: bandpass/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: axisToRegrid
        default: freq
      - id: outSoltab
        default: bandpass
      - id: newdelta
        source: bandpass_freqresolution
      - id: delta
        source: avg_freqresolution
      - id: maxFlaggedWidth
        source: max2interpolate
      - id: log
        default: true
    out:
      - id: output_h5parm
      - id: logfile
    run: ../lofar-cwl/steps/LoSoTo.Interpolate.cwl
    'sbg:x': 1800
    'sbg:y': -50
  - id: smoothb
    in:
      - id: input_h5parm
        source: interp/output_h5parm
      - id: soltab
        default: sol000/bandpass
      - id: axesToSmooth
        default:
          - time
      - id: mode
        default: median
      - id: replace
        default: true
      - id: log
        default: true
    out:
      - id: output_h5parm
      - id: logfile
    run: ../lofar-cwl/steps/LoSoTo.Smooth.cwl
    label: smoothb
    'sbg:x': 1950
    'sbg:y': -50
requirements: []
