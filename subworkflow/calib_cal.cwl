class: Workflow
cwlVersion: v1.0
id: calibrate_calibrator
label: calibrate_calibrator
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: msin
    type: Directory
    'sbg:x': -1000
    'sbg:y': -100
  - id: do_smooth
    type: boolean
    'sbg:x': -1000
    'sbg:y': 0
  - id: sourcedb
    type: File
    'sbg:x': -1000
    'sbg:y': 100
outputs:
  - id: msout
    outputSource:
      - predict/msout
    type: Directory
    'sbg:x': 1000
    'sbg:y': 0
  - id: BLsmooth.log
    outputSource:
      - BLsmooth/logfile
    type: File
    'sbg:x': 1000
    'sbg:y': 100    
steps:
  - id: BLsmooth
    in:
      - id: msin
        source: msin
      - id: do_smooth
        source: do_smooth
    out:
      - id: msout
      - id: logfile
    run: ../steps/blsmooth.cwl
    label: BLsmooth
    'sbg:x': -500
    'sbg:y': 0
  - id: predict
    in:
      - id: msin
        source: BLsmooth/msout
      - id: msout_datacolumn
        default: MODEL_DATA
      - id: sources_db
        source: sourcedb
    out:
      - id: msout
    run: ../lofar-cwl/steps/predict.cwl
    'sbg:x': 0
    'sbg:y': 0
requirements:
  - class: ScatterFeatureRequirement
