class: Workflow
cwlVersion: v1.0
id: calibrate_target
label: calibrate_target
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: msin
    type: Directory
    'sbg:x': -1000
    'sbg:y': -200
  - id: skymodel
    type: File
    'sbg:x': -1000
    'sbg:y': -100
  - id: do_smooth
    type: boolean
    'sbg:x': -1000
    'sbg:y': 0
  - id: propagatesolutions
    type: boolean
    'sbg:x': -1000
    'sbg:y': 100
outputs:
  - id: msout
    outputSource:
      - calib_targ/msout
    type: Directory
    'sbg:x': 1000
    'sbg:y': -200
  - id: BLsmooth.log
    outputSource:
      - BLsmooth/logfile
    type: File
    'sbg:x': 1000
    'sbg:y': -100
  - id: gaincal.log
    outputSource:
      - concat_logfiles_gaincal/output
    type: File
    'sbg:x': 1000
    'sbg:y': 0
  - id: outh5parm
    outputSource:
      - calib_targ/h5parm
    type: File
    'sbg:x': 1000
    'sbg:y': 100
steps:
  - id: BLsmooth
    in:
      - id: msin
        source: msin
      - id: do_smooth
        source: do_smooth
      - id: in_column_name
        default: DATA
      - id: restore
        default: true
    out:
      - id: msout
      - id: logfile
    run: ../steps/blsmooth.cwl
    label: BLsmooth
    'sbg:x': -500
    'sbg:y': 0
  - id: calib_targ
    in:
      - id: msin
        source: BLsmooth/msout
      - id: msin_datacolumn
        default: SMOOTHED_DATA
      - id: msout_name
        default: .
      - id: blrange
        default:
          - 150
          - 9999999
      - id: caltype
        default: phaseonly
      - id: sourcedb
        source: skymodel
      - id: maxiter
        default: 50
      - id: solint
        default: 1
      - id: nchan
        default: 0
      - id: tolerance
        default: 1e-3
      - id: propagatesolutions
        source: propagatesolutions
      - id: usebeammodel
        default: true
      - id: usechannelfreq
        default: true
      - id: beammode
        default: array_factor
      - id: onebeamperpatch
        default: false
    out:
      - id: msout
      - id: h5parm
      - id: logfile
    run: ../lofar-cwl/steps/gaincal.cwl
    'sbg:x': 0
    'sbg:y': 0
  - id: concat_logfiles_gaincal
    in:
      - id: file_list
        source:
          - calib_targ/logfile
      - id: file_prefix
        default: gaincal
    out:
      - id: output
    run: ../steps/concatenate_files.cwl
    label: concat_logfiles_gaincal
    'sbg:x': 600
    'sbg:y': 400
requirements:
  - class: MultipleInputFeatureRequirement
