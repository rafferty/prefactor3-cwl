class: Workflow
cwlVersion: v1.0
id: losoto_ion
label: losoto_ion
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: input_h5parm
    type: File
    'sbg:x': -700
    'sbg:y': -400
  - id: fit3rdorder
    type: boolean?
    'sbg:x': -700
    'sbg:y': -200
  - id: refAnt
    type: string?
    'sbg:x': -700
    'sbg:y': 0
  - id: maxStddev
    type: float?
    'sbg:x': -700
    'sbg:y': 200
  - id: clock_smooth
    type: boolean?
    'sbg:x': -700
    'sbg:y': 400
outputs:
  - id: output_h5parm
    outputSource:
      - losoto_flagstation/output_h5parm
    type: File
    'sbg:x': 1500
    'sbg:y': 0
  - id: logfiles
    outputSource:
      - duplicatePbkp/log
      - losoto_clocktec/log
      - losoto_residual/log
      - duplicateCbkp/log
      - smooth/logfile
      - losoto_flagstation/log
    type: 'File[]'
    linkMerge: merge_flattened
    'sbg:x': 1500
    'sbg:y': -200
  - id: parset
    outputSource:
      - losoto_residual/output_h5parm
    type: File
    'sbg:x': 1500
    'sbg:y': 200
steps:
  - id: duplicatePbkp
    in:
      - id: input_h5parm
        source: input_h5parm
      - id: soltab
        default: sol000/phase000
      - id: soltabOut
        default: phaseOrig
    out:
      - id: output_h5parm
      - id: log
    run: ../lofar-cwl/steps/LoSoTo.Duplicate.cwl
    'sbg:x': 300
    'sbg:y': 0
  - id: losoto_clocktec
    in:
      - id: input_h5parm
        source: duplicatePbkp/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: clocksoltabOut
        default: clock
      - id: tecsoltabOut
        default: tec
      - id: offsetsoltabOut
        default: phase_offset
      - id: tec3rdsoltabOut
        default: tec3rd
      - id: FlagBadChannels
        default: false
      - id: CombinePol
        default: false
      - id: Fit3rdOrder
        source: fit3rdorder
      - id: Circular
        default: false
    out:
      - id: output_h5parm
      - id: parset
      - id: log
    run: ../lofar-cwl/steps/LoSoTo.ClockTec.cwl
    'sbg:x': 500
    'sbg:y': 0
  - id: duplicateCbkp
    in:
      - id: input_h5parm
        source: losoto_clocktec/output_h5parm
      - id: soltab
        default: sol000/clock
      - id: soltabOut
        default: clockOrig
    out:
      - id: output_h5parm
      - id: log
    run: ../lofar-cwl/steps/LoSoTo.Duplicate.cwl
    'sbg:x': 700
    'sbg:y': 0
  - id: smooth
    in:
      - id: input_h5parm
        source: duplicateCbkp/output_h5parm
      - id: execute
        source: clock_smooth
      - id: soltab
        default: sol000/clock
      - id: axesToSmooth
        default:
          - time
      - id: mode
        default: median
      - id: replace
        default: true
      - id: log
        default: false
    out:
      - id: output_h5parm
      - id: logfile
    run: ../lofar-cwl/steps/LoSoTo.Smooth.cwl
    'sbg:x': 900
    'sbg:y': 0
  - id: losoto_residual
    in:
      - id: input_h5parm
        source: smooth/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: soltabsToSub
        source:
          - fit3rdorder
        valueFrom: '$(self ? ["tec", "clock", "tec3rd"] : ["tec", "clock"])'
    out:
      - id: output_h5parm
      - id: log
    run: ../lofar-cwl/steps/LoSoTo.Residual.cwl
    'sbg:x': 1100
    'sbg:y': 0
  - id: losoto_flagstation
    in:
      - id: input_h5parm
        source: smooth/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: mode
        default: resid
      - id: maxStddev
        source: maxStddev
      - id: refAnt
        source: refAnt
      - id: soltabExport
        default: clock
    out:
      - id: output_h5parm
      - id: log
    run: ../lofar-cwl/steps/LoSoTo.FlagStation.cwl
    'sbg:x': 1300
    'sbg:y': 0
requirements:
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement
