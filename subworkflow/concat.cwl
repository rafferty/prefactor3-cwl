class: Workflow
cwlVersion: v1.0
id: concat
label: concat
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: msin
    type: 'Directory[]'
    'sbg:x': -1000
    'sbg:y': -300
  - id: group_id
    type: string
    'sbg:x': -1000
    'sbg:y': -200
  - id: groups_specification
    type: File
    'sbg:x': -1000
    'sbg:y': -100
  - id: filter_baselines
    type: string
    'sbg:x': -1000
    'sbg:y': 0
  - id: avg_timeresolution_concat
    type: int
    'sbg:x': -1000
    'sbg:y': 100
  - id: avg_freqresolution_concat
    type: string
    'sbg:x': -1000
    'sbg:y': 200
  - id: min_unflagged_fraction
    type: float
    'sbg:x': -1000
    'sbg:y': 300
outputs:
  - id: msout
    outputSource:
      - dpppconcat/msout
    type: Directory
    'sbg:x': 1000
    'sbg:y': 0
  - id: dpppconcat.log
    outputSource:
      - concat_logfiles_dpppconcat/output
    type: File
    'sbg:x': 1000
    'sbg:y': 100
steps:
  - id: filter_ms_group
    in:
      - id: group_id
        source: group_id
      - id: groups_specification
        source: groups_specification
      - id: measurement_sets
        source:
          - msin
    out:
      - id: output
      - id: selected_ms
    run: ../steps/filter_ms_group.cwl
    label: filter_ms_group
    'sbg:x': -500
    'sbg:y': 0
  - id: dpppconcat
    in:
      - id: msin
        source:
          - msin
      - id: msin_fname
        source: filter_ms_group/selected_ms
      - id: msout_name
        source: group_id
      - id: msin_datacolumn
        default: DATA
      - id: msout_datacolumn
        default: DATA
      - id: filter_baselines
        source: filter_baselines
      - id: filter_remove
        default: true
      - id: overwrite
        default: false
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: missingdata
        default: true
      - id: baseline
        source: filter_baselines
      - id: avg_timeresolution
        source: avg_timeresolution_concat
      - id: avg_freqresolution
        source: avg_freqresolution_concat
    out:
      - id: msout
      - id: logfile
    run: ../lofar-cwl/steps/dpppconcat.cwl
    label: dpppconcat
    'sbg:x': -200
    'sbg:y': 0
  - id: concat_logfiles_dpppconcat
    in:
      - id: file_list
        source:
          - dpppconcat/logfile
      - id: file_prefix
        default: dpppconcat
    out:
      - id: output
    run: ../steps/concatenate_files.cwl
    label: concat_logfiles_dpppconcat
    'sbg:x': 600
    'sbg:y': 200

requirements: []
