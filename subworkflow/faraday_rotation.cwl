class: Workflow
cwlVersion: v1.0
id: faraday_rotation
label: faraday_rotation
$namespaces:
  lofar: 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: refAnt
    type: string?
    'sbg:x': -451.3746643066406
    'sbg:y': -493.92510986328125
  - id: input_h5parm
    type: File
    'sbg:x': -743.1871948242188
    'sbg:y': -316.5615234375
outputs:
  - id: output_h5parm
    outputSource:
      - losoto_faraday/output_h5parm
    type: File
    format: 'lofar:#H5Parm'
    'sbg:x': 152.59103393554688
    'sbg:y': -289.2899169921875
  - id: logfiles
    outputSource:
      - losoto_duplicate/log
      - losoto_faraday/log
    type: 'File[]'
    linkMerge: merge_flattened
    'sbg:x': 300.5910339355469
    'sbg:y': 0
steps:
  - id: losoto_duplicate
    in:
      - id: input_h5parm
        source: input_h5parm
      - id: soltab
        default: sol000/phase000
      - id: soltabOut
        default: phaseOrig
    out:
      - id: output_h5parm
      - id: log
    run: ../lofar-cwl/steps/LoSoTo.Duplicate.cwl
    'sbg:x': -423.6229248046875
    'sbg:y': -287.8958435058594
  - id: losoto_faraday
    in:
      - id: input_h5parm
        source: losoto_duplicate/output_h5parm
      - id: soltab
        default: sol000/rotation000
      - id: soltabout
        default: faraday
      - id: refAnt
        source: refAnt
      - id: maxResidual
        default: 1
    out:
      - id: output_h5parm
      - id: log
    run: ../lofar-cwl/steps/LoSoTo.Faraday.cwl
    'sbg:x': -99.27792358398438
    'sbg:y': -126.80227661132812
requirements: []
$schema:
  - 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
