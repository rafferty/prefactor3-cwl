class: Workflow
cwlVersion: v1.0
id: ionosphere
label: ionosphere
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: msin
    type: 'Directory[]'
    'sbg:x': -590.3988647460938
    'sbg:y': -21.5
  - id: propagate_solutions
    type: boolean
    'sbg:x': -482.39886474609375
    'sbg:y': -260.5
  - id: flagunconverged
    type: boolean
    'sbg:x': -559.3988647460938
    'sbg:y': 118.5
  - id: do_smooth
    type: boolean
    'sbg:x': -420.39886474609375
    'sbg:y': 229.5
outputs:
  - id: outh5parm
    outputSource:
      - h5exp_cal_ion/outh5parm
    type: File
    'sbg:x': 558.2401123046875
    'sbg:y': -60.5
steps:
  - id: smooth_calibrate
    in:
      - id: msin
        source:
          - msin
      - id: mode
        default: diagonal
      - id: propagate_solutions
        source: propagate_solutions
      - id: flagunconverged
        source: flagunconverged
      - id: do_smooth
        source: do_smooth
    out:
      - id: h5parm
    run: ./smooth_calibrate.cwl
    label: smooth_calibrate
    'sbg:x': -225
    'sbg:y': -14
  - id: h5parm_collector
    in:
      - id: h5parmFiles
        source:
          - smooth_calibrate/h5parm
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: clobber
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../lofar-cwl/steps/H5ParmCollector.cwl
    label: h5imp_cal_ion
    'sbg:x': -42
    'sbg:y': -19
  - id: losoto_ion
    in:
      - id: input_h5parm
        source: h5parm_collector/outh5parm
    out:
      - id: output_h5parm
    run: ./losoto_ion.cwl
    label: losoto_ion
    'sbg:x': 140
    'sbg:y': -34
  - id: h5exp_cal_ion
    in:
      - id: h5parmFiles
        source:
          - losoto_ion/output_h5parm
      - id: outh5parmname
        default: cal_solutions.h5
      - id: squeeze
        default: true
      - id: verbose
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../lofar-cwl/steps/H5ParmCollector.cwl
    label: h5exp_cal_ion
    'sbg:x': 352
    'sbg:y': -31
requirements:
  - class: SubworkflowFeatureRequirement
