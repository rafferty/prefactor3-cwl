class: Workflow
cwlVersion: v1.0
id: losoto_bandpass
label: losoto_bandpass
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: ampRange
    type: 'float[]?'
    'sbg:x': 730.283935546875
    'sbg:y': 152.7667236328125
  - id: skipInternational
    type: string?
    'sbg:x': 742.662841796875
    'sbg:y': -221.30563354492188
  - id: input_h5parm
    type: File
    'sbg:x': 512.96240234375
    'sbg:y': -41.42390060424805
  - id: max2interpolate
    type: int?
    'sbg:x': 1227
    'sbg:y': -278
  - id: bandpass_freqresolution
    type: string
    'sbg:x': 1399
    'sbg:y': -354
  - id: avg_freqresolution
    type: string?
    'sbg:x': 1286.2171630859375
    'sbg:y': 127.25900268554688
outputs:
  - id: output_h5parm
    outputSource:
      - smoothb/output_h5parm
    type: File
    'sbg:x': 1798
    'sbg:y': -47
steps:
  - id: duplicateAbkp
    in:
      - id: input_h5parm
        source: input_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: soltabOut
        default: amplitudeOrig000
    out:
      - id: output_h5parm
    run: ../lofar-cwl/steps/LoSoTo.Duplicate.cwl
    'sbg:x': 642
    'sbg:y': -48
  - id: losoto_flag
    in:
      - id: input_h5parm
        source: duplicateAbkp/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: axesToFlag
        default:
          - time
          - freq
      - id: order
        default:
          - 100
          - 40
      - id: maxCycles
        default: 1
      - id: maxRms
        default: 5
      - id: mode
        default: smooth
    out:
      - id: output_h5parm
    run: ../lofar-cwl/steps/LoSoTo.Flag.cwl
    label: flag
    'sbg:x': 764
    'sbg:y': -47
  - id: flagbp
    in:
      - id: input_h5parm
        source: losoto_flag/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: mode
        default: bandpass
      - id: ampRange
        source:
          - ampRange
      - id: skipInternational
        source: skipInternational
    out:
      - id: output_h5parm
    run: ../lofar-cwl/steps/LoSoTo.FlagStation.cwl
    'sbg:x': 873
    'sbg:y': -55
  - id: flagextend
    in:
      - id: input_h5parm
        source: flagbp/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: axesToExt
        default:
          - time
          - freq
      - id: size
        default:
          - 200
          - 80
      - id: percent
        default: 50
      - id: maxCycles
        default: 2
    out:
      - id: output_h5parm
    run: ../lofar-cwl/steps/LoSoTo.Flagextend.cwl
    'sbg:x': 1005
    'sbg:y': -51
  - id: merge
    in:
      - id: input_h5parm
        source: flagextend/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: mode
        default: copy
      - id: soltabImport
        default: amplitude000
    out:
      - id: output_h5parm
    run: ../lofar-cwl/steps/LoSoTo.Reweight.cwl
    'sbg:x': 1121
    'sbg:y': -55
  - id: smooth
    in:
      - id: input_h5parm
        source: merge/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: axesToSmooth
        default:
          - time
      - id: mode
        default: median
      - id: log
        default: true
    out:
      - id: output_h5parm
    run: ../lofar-cwl/steps/LoSoTo.Smooth.cwl
    'sbg:x': 1265.0499267578125
    'sbg:y': -47.9036750793457
  - id: bandpass
    in:
      - id: input_h5parm
        source: smooth/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: axesToSmooth
        default:
          - freq
      - id: size
        default:
          - 17
      - id: mode
        default: savitzky-golay
      - id: degree
        default: 2
      - id: log
        default: true
    out:
      - id: output_h5parm
    run: ../lofar-cwl/steps/LoSoTo.Smooth.cwl
    'sbg:x': 1378.419921875
    'sbg:y': -52.9036750793457
  - id: interp
    in:
      - id: input_h5parm
        source: bandpass/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: axisToRegrid
        default: freq
      - id: outSoltab
        default: bandpass
      - id: newdelta
        source: bandpass_freqresolution
      - id: delta
        source: avg_freqresolution
      - id: maxFlaggedWidth
        source: max2interpolate
      - id: log
        default: true
    out:
      - id: output_h5parm
    run: ../lofar-cwl/steps/LoSoTo.Interpolate.cwl
    'sbg:x': 1507.4566650390625
    'sbg:y': -51.45471954345703
  - id: smoothb
    in:
      - id: input_h5parm
        source: interp/output_h5parm
      - id: soltab
        default: sol000/bandpass
      - id: axesToSmooth
        default:
          - time
      - id: mode
        default: median
      - id: replace
        default: true
      - id: log
        default: true
    out:
      - id: output_h5parm
    run: ../lofar-cwl/steps/LoSoTo.Smooth.cwl
    label: smoothb
    'sbg:x': 1648.5743408203125
    'sbg:y': -51.13884735107422
requirements: []
