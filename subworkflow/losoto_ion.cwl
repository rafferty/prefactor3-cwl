class: Workflow
cwlVersion: v1.0
id: losoto_ion
label: losoto_ion
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: input_h5parm
    type: File
    'sbg:x': -588.3988647460938
    'sbg:y': -147.5
  - id: fit3rdorder
    type: boolean?
    'sbg:x': 83.60113525390625
    'sbg:y': 53.5
  - id: refAnt
    type: string?
    'sbg:x': 500.74346923828125
    'sbg:y': -382.8847351074219
  - id: maxStddev
    type: float?
    'sbg:x': 374.1950988769531
    'sbg:y': -308.9609069824219
outputs:
  - id: output_h5parm
    outputSource:
      - losoto_flagstation/output_h5parm
    type: File
    'sbg:x': 775
    'sbg:y': -169
steps:
  - id: flag
    in:
      - id: input_h5parm
        source: input_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: axesToFlag
        default:
          - time
          - freq
      - id: order
        default:
          - 100
          - 40
      - id: maxCycles
        default: 1
      - id: maxRms
        default: 5
      - id: mode
        default: smooth
    out:
      - id: output_h5parm
    run: ../lofar-cwl/steps/LoSoTo.Flag.cwl
    'sbg:x': -442.3984375
    'sbg:y': -138.5
  - id: flagextend
    in:
      - id: input_h5parm
        source: flag/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: axesToExt
        default:
          - time
          - freq
      - id: size
        default:
          - 200
          - 80
      - id: percent
        default: 50
      - id: maxCycles
        default: 2
    out:
      - id: output_h5parm
    run: ../lofar-cwl/steps/LoSoTo.Flagextend.cwl
    'sbg:x': -255
    'sbg:y': -160
  - id: merge
    in:
      - id: input_h5parm
        source: flagextend/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: mode
        default: copy
      - id: soltabImport
        default: amplitude000
    out:
      - id: output_h5parm
    run: ../lofar-cwl/steps/LoSoTo.Reweight.cwl
    'sbg:x': -94
    'sbg:y': -162
  - id: duplicatePbkp
    in:
      - id: input_h5parm
        source: merge/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: soltabOut
        default: phaseOrig
    out:
      - id: output_h5parm
    run: ../lofar-cwl/steps/LoSoTo.Duplicate.cwl
    'sbg:x': 69
    'sbg:y': -162
  - id: losoto_flagstation
    in:
      - id: input_h5parm
        source: losoto_residual/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: maxStddev
        source: maxStddev
      - id: refAnt
        source: refAnt
      - id: soltabExport
        default: clock
    out:
      - id: output_h5parm
    run: ../lofar-cwl/steps/LoSoTo.FlagStation.cwl
    'sbg:x': 587
    'sbg:y': -172
  - id: losoto_clocktec
    in:
      - id: input_h5parm
        source: duplicatePbkp/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: clocksoltabOut
        default: clock
      - id: tecsoltabOut
        default: tec
      - id: offsetsoltabOut
        default: phase_offset
      - id: tec3rdsoltabOut
        default: tec3rd
      - id: combinePol
        default: true
      - id: fit3rdorder
        source: fit3rdorder
    out:
      - id: output_h5parm
    run: ../lofar-cwl/steps/LoSoTo.ClockTec.cwl
    'sbg:x': 217.72296142578125
    'sbg:y': -230.69259643554688
  - id: losoto_residual
    in:
      - id: input_h5parm
        source: losoto_clocktec/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: soltabsToSub
        source:
          - fit3rdorder
        valueFrom: '$(self ? ["tec", "clock", "tec3rd"] : ["tec", "clock"])'
    out:
      - id: output_h5parm
    run: ../lofar-cwl/steps/LoSoTo.Residual.cwl
    'sbg:x': 411.0426330566406
    'sbg:y': -168.98934936523438
requirements: []
