class: Workflow
cwlVersion: v1.0
id: ndppp_prep_cal
label: ndppp_prep_cal
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: baselines_to_flag
    type: 'string[]'
    'sbg:x': 732.2234497070312
    'sbg:y': 79.11939239501953
  - id: elevation_to_flag
    type: string
    'sbg:x': 815.8416137695312
    'sbg:y': -281.40777587890625
  - id: min_amplitude_to_flag
    type: float
    'sbg:x': 964.3058471679688
    'sbg:y': 191.110595703125
  - id: memoryperc
    type: int
    'sbg:x': 136.78256225585938
    'sbg:y': -341.9549560546875
  - id: raw_data
    type: boolean
    'sbg:x': 444.4574279785156
    'sbg:y': -343.6958312988281
  - id: demix
    type: boolean
    'sbg:x': 1736.6195068359375
    'sbg:y': -298.45196533203125
  - id: msin
    type: Directory?
    'sbg:x': 2306.83056640625
    'sbg:y': 318.2810363769531
  - id: msin_baseline
    type: string?
    'sbg:x': 2273.798583984375
    'sbg:y': 182.7539825439453
  - id: skymodel
    type: File
    'sbg:x': 1335.2567138671875
    'sbg:y': 148.68453979492188
  - id: timeresolution
    type: float
    'sbg:x': 2021.81787109375
    'sbg:y': -352.4759216308594
  - id: freqresolution
    type: string
    'sbg:x': 1999.5889892578125
    'sbg:y': 170.7563934326172
  - id: demix_timestep
    type: int
    'sbg:x': 1371.962646484375
    'sbg:y': 307.4278259277344
  - id: demix_freqstep
    type: int
    'sbg:x': 1465.0855712890625
    'sbg:y': 450.8930358886719
  - id: demix_baseline
    type: string
    'sbg:x': 1585.496826171875
    'sbg:y': 588.0062866210938
  - id: target_source
    type: string
    'sbg:x': 1439.47314453125
    'sbg:y': -325.6144104003906
  - id: ntimechunk
    type: int
    'sbg:x': 1149.4033203125
    'sbg:y': 349.2409362792969
  - id: subtract_sources
    type: 'string[]?'
    'sbg:x': 1257.9327392578125
    'sbg:y': -290.095703125
outputs:
  - id: msout
    outputSource:
      - dppp/msout
    type: Directory
    'sbg:x': 2856.537841796875
    'sbg:y': 53.67763900756836
  - id: logfile
    outputSource:
      - concat_logfiles_dppp/output
    type: File
    'sbg:x': 2857.9423828125
    'sbg:y': 253.50680541992188
  - id: parset
    outputSource:
      - parsetgenerator/parset
    type: File
    'sbg:x': 2598.6884765625
    'sbg:y': -303.1161804199219
steps:
  - id: flagbaseline
    in:
      - id: steps
        source:
          - process_raw_data_select/selected_steps
      - id: step_name
        default: flagbaseline
      - id: baseline
        source:
          - baselines_to_flag
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.PreflaggerStepGenerator.cwl
    'sbg:x': 955.2450561523438
    'sbg:y': -75.71149444580078
  - id: flagelev
    in:
      - id: steps
        source:
          - flagbaseline/augmented_steps
      - id: step_name
        default: flagelev
      - id: elevation
        source: elevation_to_flag
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.PreflaggerStepGenerator.cwl
    'sbg:x': 1187.4771728515625
    'sbg:y': -83
  - id: process_raw_data_select
    in:
      - id: select_a
        source: raw_data
      - id: a_steps
        source:
          - aoflag/augmented_steps
      - id: b_steps
        default: []
    out:
      - id: selected_steps
    run: ../lofar-cwl/steps/DP3.Selector.cwl
    label: Selector
    'sbg:x': 621.4892578125
    'sbg:y': -71.31844329833984
  - id: flagamp
    in:
      - id: steps
        source:
          - flagelev/augmented_steps
      - id: step_name
        default: flagamp
      - id: amplmin
        source: min_amplitude_to_flag
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.PreflaggerStepGenerator.cwl
    'sbg:x': 1385.993408203125
    'sbg:y': -42.28850555419922
  - id: flagedge
    in:
      - id: steps
        default: []
      - id: step_name
        default: flagedge
      - id: chan
        default: '[0..nchan/32-1,31*nchan/32..nchan-1]'
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.PreflaggerStepGenerator.cwl
    'sbg:x': 122.25408935546875
    'sbg:y': -54.06163024902344
  - id: aoflag
    in:
      - id: steps
        source:
          - flagedge/augmented_steps
      - id: step_name
        default: aoflag
      - id: memoryperc
        source: memoryperc
      - id: keepstatistics
        default: false
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.AOFlaggerStepGenerator.cwl
    'sbg:x': 369.9876708984375
    'sbg:y': -38.086280822753906
  - id: demix_select
    in:
      - id: select_a
        source: demix
      - id: a_steps
        source:
          - demixstepgenerator/augmented_steps
      - id: b_steps
        source:
          - flagamp/augmented_steps
    out:
      - id: selected_steps
    run: ../lofar-cwl/steps/DP3.Selector.cwl
    label: demix?
    'sbg:x': 1974.7406005859375
    'sbg:y': -32.03063201904297
  - id: demixstepgenerator
    in:
      - id: steps
        source:
          - flagamp/augmented_steps
      - id: step_name
        default: demixer
      - id: baseline
        source: demix_baseline
      - id: demixtimestep
        source: demix_timestep
      - id: demixfreqstep
        source: demix_freqstep
      - id: ntimechunk
        source: ntimechunk
      - id: skymodel
        source: skymodel
      - id: subtractsources
        source:
          - subtract_sources
      - id: targetsource
        source: target_source
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.DemixerStepGenerator.cwl
    'sbg:x': 1676.6593017578125
    'sbg:y': 90.7158203125
  - id: parsetgenerator
    in:
      - id: steps
        source:
          - average_step_generator/augmented_steps
    out:
      - id: parset
      - id: input_files
      - id: input_directories
      - id: output_file_names
      - id: output_directory_names
    run: ../lofar-cwl/steps/DP3.ParsetGenerator.cwl
    'sbg:x': 2340.85400390625
    'sbg:y': -79.32791900634766
  - id: dppp
    in:
      - id: parset
        source: parsetgenerator/parset
      - id: msin
        source: msin
      - id: msout_name
        source: msin
        valueFrom: $("out_"+self.basename)
      - id: secondary_files
        source:
          - parsetgenerator/input_files
      - id: secondary_directories
        source:
          - parsetgenerator/input_directories
      - id: output_file_names
        source: parsetgenerator/output_file_names
      - id: output_directory_names
        source: parsetgenerator/output_directory_names
      - id: autoweight
        source: raw_data
      - id: baseline
        source: msin_baseline
      - id: writefullresflag
        default: false
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
    out:
      - id: msout
      - id: secondary_output_files
      - id: secondary_output_directories
      - id: logfile
    run: ../lofar-cwl/steps/DPPP.cwl
    'sbg:x': 2669.29345703125
    'sbg:y': -42.129730224609375
  - id: average_step_generator
    in:
      - id: steps
        source:
          - demix_select/selected_steps
      - id: step_name
        default: averager
      - id: timeresolution
        source: timeresolution
      - id: freqresolution
        source: freqresolution
      - id: minpoints
        default: 1
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.AveragerStepGenerator.cwl
    'sbg:x': 2158.297119140625
    'sbg:y': -55.93873977661133
  - id: concat_logfiles_dppp
    in:
      - id: file_list
        source:
          - dppp/logfile
      - id: file_prefix
        default: dppp
    out:
      - id: output
    run: ../steps/concatenate_files.cwl
    label: concat_logfiles_dppp
    'sbg:x': 2759.2265625
    'sbg:y': 140.91714477539062
requirements:
  - class: SubworkflowFeatureRequirement
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement
