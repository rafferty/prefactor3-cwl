class: Workflow
cwlVersion: v1.0
id: ndppp_prep_targ
label: ndppp_prep_targ
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: baselines_to_flag
    type: 'string[]'
    'sbg:x': 732.2234497070312
    'sbg:y': 79.11939239501953
  - id: elevation_to_flag
    type: string
    'sbg:x': 815.8416137695312
    'sbg:y': -281.40777587890625
  - id: min_amplitude_to_flag
    type: float
    'sbg:x': 964.3058471679688
    'sbg:y': 191.110595703125
  - id: memoryperc
    type: int
    'sbg:x': 136.78256225585938
    'sbg:y': -341.9549560546875
  - id: raw_data
    type: boolean
    'sbg:x': 464.3541259765625
    'sbg:y': -200.52120971679688
  - id: demix
    type: boolean
    'sbg:x': 1647.1820068359375
    'sbg:y': -314.6533508300781
  - id: msin
    type: Directory?
    'sbg:x': 3819.599853515625
    'sbg:y': 423.25006103515625
  - id: skymodel
    type: File
    'sbg:x': 1335.2567138671875
    'sbg:y': 148.68453979492188
  - id: timeresolution
    type: float
    'sbg:x': 3344.8515625
    'sbg:y': -733.585693359375
  - id: freqresolution
    type: string
    'sbg:x': 3358.795654296875
    'sbg:y': 298.08917236328125
  - id: demix_timestep
    type: int
    'sbg:x': 1371.962646484375
    'sbg:y': 307.4278259277344
  - id: demix_freqstep
    type: int
    'sbg:x': 1465.0855712890625
    'sbg:y': 450.8930358886719
  - id: process_baselines_target
    type: string
    'sbg:x': 1585.496826171875
    'sbg:y': 588.0062866210938
  - id: target_source
    type: string
    'sbg:x': 1439.47314453125
    'sbg:y': -325.6144104003906
  - id: ntimechunk
    type: int
    'sbg:x': 1149.4033203125
    'sbg:y': 349.2409362792969
  - id: subtract_sources
    type: 'string[]?'
    'sbg:x': 1257.9327392578125
    'sbg:y': -290.095703125
  - id: parmdb
    type: File
    'sbg:x': 1930.5469970703125
    'sbg:y': 298.99664306640625
  - id: apply_tec_correction
    type: boolean
    'sbg:x': 2310.848876953125
    'sbg:y': 446.3658752441406
  - id: apply_rm_correction
    type: boolean
    'sbg:x': 2461.30224609375
    'sbg:y': 719.8994140625
  - id: apply_phase_correction
    type: boolean
    'sbg:x': 2544.00341796875
    'sbg:y': 844.8623657226562
  - id: apply_clock_correction
    type: boolean
    'sbg:x': 2238.046630859375
    'sbg:y': 327.0130920410156
  - id: apply_beam_correction
    type: boolean
    'sbg:x': 2391.738037109375
    'sbg:y': 585.3051147460938
  - id: filter_baselines
    type: string
    'sbg:x': 1880
    'sbg:y': -380
  - id: updateweights
    type: boolean
    'sbg:x': 2234.90185546875
    'sbg:y': -393.8929748535156
outputs:
  - id: prep_logfile
    outputSource:
      - concat_logfiles_dppp/output
    type: File?
    'sbg:x': 5500
    'sbg:y': 100
  - id: predict_logfile
    outputSource:
      - concat_logfiles_predict/output
    type: File?
    'sbg:x': 5500
    'sbg:y': 0
  - id: clipper_logfile
    outputSource:
      - concat_logfiles_clipper/output
    type: File?
    'sbg:x': 5500
    'sbg:y': -100
  - id: msout
    outputSource:
      - predict/msout
    type: Directory
    'sbg:x': 5502.318359375
    'sbg:y': -279.49835205078125
  - id: clipper_output
    outputSource:
      - Ateamclipper/output
    type: File
    'sbg:x': 5502.318359375
    'sbg:y': -500
  - id: parset
    outputSource:
      - dp3_execute/parset
    type: File
    'sbg:x': 5427.662109375
    'sbg:y': -765.5377197265625
steps:
  - id: flagbaseline
    in:
      - id: steps
        source:
          - process_raw_data_select/selected_steps
      - id: step_name
        default: flagbaseline
      - id: baseline
        source:
          - baselines_to_flag
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.PreflaggerStepGenerator.cwl
    'sbg:x': 955.2450561523438
    'sbg:y': -75.71149444580078
  - id: flagelev
    in:
      - id: steps
        source:
          - flagbaseline/augmented_steps
      - id: step_name
        default: flagelev
      - id: elevation
        source: elevation_to_flag
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.PreflaggerStepGenerator.cwl
    'sbg:x': 1187.4771728515625
    'sbg:y': -83
  - id: process_raw_data_select
    in:
      - id: select_a
        source: raw_data
      - id: a_steps
        source:
          - aoflag/augmented_steps
      - id: b_steps
        default: []
    out:
      - id: selected_steps
    run: ../lofar-cwl/steps/DP3.Selector.cwl
    label: Selector
    'sbg:x': 621.4892578125
    'sbg:y': -71.31844329833984
  - id: flagamp
    in:
      - id: steps
        source:
          - flagelev/augmented_steps
      - id: step_name
        default: flagamp
      - id: amplmin
        source: min_amplitude_to_flag
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.PreflaggerStepGenerator.cwl
    'sbg:x': 1385.993408203125
    'sbg:y': -42.28850555419922
  - id: flagedge
    in:
      - id: steps
        default: []
      - id: step_name
        default: flagedge
      - id: chan
        default: '[0..nchan/32-1,31*nchan/32..nchan-1]'
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.PreflaggerStepGenerator.cwl
    'sbg:x': 122.25408935546875
    'sbg:y': -54.06163024902344
  - id: aoflag
    in:
      - id: steps
        source:
          - flagedge/augmented_steps
      - id: step_name
        default: aoflag
      - id: memoryperc
        source: memoryperc
      - id: keepstatistics
        default: false
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.AOFlaggerStepGenerator.cwl
    'sbg:x': 369.9876708984375
    'sbg:y': -38.086280822753906
  - id: demix_select
    in:
      - id: select_a
        source: demix
      - id: a_steps
        source:
          - demixstepgenerator/augmented_steps
      - id: b_steps
        source:
          - flagamp/augmented_steps
    out:
      - id: selected_steps
    run: ../lofar-cwl/steps/DP3.Selector.cwl
    label: demix?
    'sbg:x': 1888.8828125
    'sbg:y': -24.5985050201416
  - id: demixstepgenerator
    in:
      - id: steps
        source:
          - flagamp/augmented_steps
      - id: step_name
        default: demixer
      - id: baseline
        source: process_baselines_target
      - id: demixtimestep
        source: demix_timestep
      - id: demixfreqstep
        source: demix_freqstep
      - id: ntimechunk
        source: ntimechunk
      - id: skymodel
        source: skymodel
      - id: subtractsources
        source:
          - subtract_sources
      - id: targetsource
        source: target_source
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.DemixerStepGenerator.cwl
    'sbg:x': 1676.6593017578125
    'sbg:y': 90.7158203125
  - id: average_step_generator
    in:
      - id: steps
        source:
          - prep_target_applycal/selected_steps
      - id: step_name
        default: averager
      - id: minpoints
        default: 1
      - id: timeresolution
        source: timeresolution
      - id: freqresolution
        source: freqresolution
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.AveragerStepGenerator.cwl
    'sbg:x': 3743.862060546875
    'sbg:y': -426.2447814941406
  - id: concat_logfiles_dppp
    in:
      - id: file_list
        source:
          - dp3_execute/logfile
      - id: file_prefix
        default: dppp
    out:
      - id: output
    run: ../steps/concatenate_files.cwl
    label: concat_logfiles_dppp
    'sbg:x': 4543.36376953125
    'sbg:y': -6.564670085906982
  - id: concat_logfiles_predict
    in:
      - id: file_list
        source:
          - predict/logfile
      - id: file_prefix
        default: predict_targ
    out:
      - id: output
    run: ../steps/concatenate_files.cwl
    label: concat_logfiles_predict
    'sbg:x': 4700
    'sbg:y': -6.564670085906982
  - id: concat_logfiles_clipper
    in:
      - id: file_list
        source:
          - Ateamclipper/logfile
      - id: file_prefix
        default: Ateamclipper
    out:
      - id: output
    run: ../steps/concatenate_files.cwl
    label: concat_logfiles_clipper
    'sbg:x': 4900
    'sbg:y': -6.564670085906982
  - id: dp3_execute
    in:
      - id: msout_name
        source: msin
        valueFrom: $("out_"+self.basename)
      - id: msin
        source: msin
      - id: steps
        source:
          - average_step_generator/augmented_steps
      - id: autoweight
        default: false
        source: raw_data
    out:
      - id: secondary_output_files
      - id: secondary_output_directories
      - id: msout
      - id: logfile
      - id: parset
    run: ../lofar-cwl/steps/DP3.Execute.cwl
    label: DP3.Execute
    'sbg:x': 4154.9287109375
    'sbg:y': -344.5472106933594
  - id: filter
    in:
      - id: steps
        source:
          - demix_select/selected_steps
      - id: step_name
        default: filter
      - id: baseline
        source: filter_baselines
      - id: remove
        default: true
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.FilterStepGenerator.cwl
    'sbg:x': 2049.48486328125
    'sbg:y': -18.005611419677734
  - id: applyPA
    in:
      - id: steps
        source:
          - filter/augmented_steps
      - id: step_name
        default: apply_pa_1
      - id: parmdb
        source: parmdb
      - id: solset
        default: calibrator
      - id: correction
        default: polalign
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.ApplyCalStepGenerator.cwl
    'sbg:x': 2205.820068359375
    'sbg:y': 7.381086349487305
  - id: applyBandpass
    in:
      - id: steps
        source:
          - applyPA/augmented_steps
      - id: step_name
        default: apply_bandpass_1
      - id: parmdb
        source: parmdb
      - id: solset
        default: calibrator
      - id: correction
        default: bandpass
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.ApplyCalStepGenerator.cwl
    'sbg:x': 2420.382568359375
    'sbg:y': -149.9799346923828
  - id: prep_target_applycal
    in:
      - id: steps
        source:
          - applyBandpass/augmented_steps
      - id: apply_clock_correction
        source: apply_clock_correction
      - id: apply_tec_correction
        source: apply_tec_correction
      - id: apply_rm_correction
        source: apply_rm_correction
      - id: apply_beam_correction
        source: apply_beam_correction
      - id: apply_phase_correction
        source: apply_phase_correction
      - id: parmdb
        source: parmdb
      - id: updateweights
        source: updateweights
    out:
      - id: selected_steps
    run: ./prep_target_applycal.cwl
    label: prep_target_applycal
    'sbg:x': 2728.697998046875
    'sbg:y': -18.151018142700195
  - id: predict
    in:
      - id: msin
        source: dp3_execute/msout
      - id: msin_datacolumn
        default: DATA
      - id: msout_datacolumn
        default: MODEL_DATA
      - id: sources_db
        source: skymodel
      - id: sources
        default:
          - VirA_4_patch
          - CygAGG
          - CasA_4_patch
          - TauAGG
      - id: usebeammodel
        default: true
      - id: onebeamperpatch
        default: true
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
    out:
      - id: msout
      - id: logfile
    run: ../lofar-cwl/steps/filter_predict.cwl
    'sbg:x': 4500
    'sbg:y': -344.5472106933594
  - id: Ateamclipper
    in:
      - id: msin
        source:
          - predict/msout
    out:
      - id: msout
      - id: logfile
      - id: output
    run: ../lofar-cwl/steps/Ateamclipper.cwl
    label: Ateamclipper
    'sbg:x': 4700
    'sbg:y': -344.5472106933594
requirements:
  - class: SubworkflowFeatureRequirement
  - class: InlineJavascriptRequirement
  - class: StepInputExpressionRequirement
