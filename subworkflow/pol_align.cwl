class: Workflow
cwlVersion: v1.0
id: pol_align
label: PolAlign
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
  lofar: 'https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl'
inputs:
  - id: refAnt
    type: string?
    'sbg:x': -451.3746643066406
    'sbg:y': -493.92510986328125
  - id: input_h5parm
    type: File
    'sbg:x': -743.1871948242188
    'sbg:y': -316.5615234375
  - id: fit_offset_PA
    type: boolean?
    'sbg:x': -743.1871948242188
    'sbg:y': -316.5615234375
outputs:
  - id: output_h5parm
    outputSource:
      - losoto_residual/output_h5parm
    type: File
    format: lofar:#H5Parm
    'sbg:x': 152.59103393554688
    'sbg:y': -289.2899169921875
  - id: logfiles
    outputSource:
      - losoto_duplicate/log
      - losoto_polalign/log
      - losoto_residual/log
    linkMerge: merge_flattened
    type: 'File[]'

steps:
  - id: losoto_duplicate
    in:
      - id: input_h5parm
        source: input_h5parm
      - id: soltab
        default: sol000/phase000
      - id: soltabOut
        default: phaseOrig
    out:
      - id: output_h5parm
      - id: log
    run: ../lofar-cwl/steps/LoSoTo.Duplicate.cwl
    'sbg:x': -423.6229248046875
    'sbg:y': -287.8958435058594
  - id: losoto_polalign
    in:
      - id: input_h5parm
        source: losoto_duplicate/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: soltabout
        default: polalign
      - id: average
        default: true
      - id: replace
        default: true
      - id: maxResidual
        default: 1.0
      - id: fitOffset
        source: fit_offset_PA
      - id: refAnt
        source: refAnt
    out:
      - id: output_h5parm
      - id: log
    run: ../lofar-cwl/steps/LoSoTo.Polalign.cwl
    'sbg:x': -203.663818359375
    'sbg:y': -292.61700439453125
  - id: losoto_residual
    in:
      - id: input_h5parm
        source: losoto_polalign/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: soltabsToSub
        default:
          - polalign
    out:
      - id: output_h5parm
      - id: log
    run: ../lofar-cwl/steps/LoSoTo.Residual.cwl
    'sbg:x': -0.012040258385241032
    'sbg:y': -287.7294006347656
requirements: []
