class: Workflow
cwlVersion: v1.0
id: predict_calibrate
label: predict_calibrate
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: msin
    type: Directory
    'sbg:x': -1000
    'sbg:y': -200
  - id: do_smooth
    type: boolean
    'sbg:x': -1000
    'sbg:y': -100
  - id: sourcedb
    type: File
    'sbg:x': -1000
    'sbg:y': 0
  - id: flagunconverged
    type: boolean
    'sbg:x': -1000
    'sbg:y': 100
  - id: propagatesolutions
    type: boolean
    'sbg:x': -1000
    'sbg:y': 200
outputs:
  - id: msout
    outputSource:
      - calib_cal/msout
    type: Directory
    'sbg:x': 1000
    'sbg:y': -200
  - id: BLsmooth.log
    outputSource:
      - BLsmooth/logfile
    type: File
    'sbg:x': 1000
    'sbg:y': -100
  - id: predict_cal.log
    outputSource:
      - concat_logfiles_predict/output
    type: File
    'sbg:x': 1000
    'sbg:y': 0
  - id: calib_cal.log
    outputSource:
      - concat_logfiles_calib_cal/output
    type: File
    'sbg:x': 1000
    'sbg:y': 100
  - id: outh5parm
    outputSource:
      - calib_cal/h5parm
    type: File
    'sbg:x': 1000
    'sbg:y': 200
steps:
  - id: BLsmooth
    in:
      - id: msin
        source: msin
      - id: do_smooth
        source: do_smooth
    out:
      - id: msout
      - id: logfile
    run: ../steps/blsmooth.cwl
    label: BLsmooth
    'sbg:x': -500
    'sbg:y': 0
  - id: predict
    in:
      - id: msin
        source: BLsmooth/msout
      - id: msin_datacolumn
        default: SMOOTHED_DATA
      - id: msout_datacolumn
        default: MODEL_DATA
      - id: sources_db
        source: sourcedb
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
    out:
      - id: msout
      - id: logfile
    run: ../lofar-cwl/steps/predict.cwl
    'sbg:x': 0
    'sbg:y': 0
  - id: calib_cal
    in:
      - id: msin
        source: predict/msout
      - id: msin_datacolumn
        default: SMOOTHED_DATA
      - id: msin_modelcolum
        default: MODEL_DATA
      - id: propagate_solutions
        source: propagatesolutions
      - id: flagunconverged
        source: flagunconverged
      - id: mode
        default: rotation+diagonal
    out:
      - id: msout
      - id: h5parm
      - id: logfile
    run: ../lofar-cwl/steps/ddecal.cwl
    'sbg:x': 500
    'sbg:y': 0
  - id: concat_logfiles_predict
    in:
      - id: file_list
        source:
          - predict/logfile
      - id: file_prefix
        default: predict
    out:
      - id: output
    run: ../steps/concatenate_files.cwl
    label: concat_logfiles_predict
    'sbg:x': 500
    'sbg:y': 200
  - id: concat_logfiles_calib_cal
    in:
      - id: file_list
        source:
          - calib_cal/logfile
      - id: file_prefix
        default: calib_cal
    out:
      - id: output
    run: ../steps/concatenate_files.cwl
    label: concat_logfiles_calib_cal
    'sbg:x': 700
    'sbg:y': 200
requirements: []
