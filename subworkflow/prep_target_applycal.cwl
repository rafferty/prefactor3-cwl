class: Workflow
cwlVersion: v1.0
id: prep_target_applycal
label: prep_target_applycal
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: steps
    type: 'Any[]?'
    'sbg:x': 1566.9437255859375
    'sbg:y': 173.2586669921875
  - id: apply_clock_correction
    type: boolean
    'sbg:x': 1622.751953125
    'sbg:y': -235.5788116455078
  - id: apply_tec_correction
    type: boolean
    'sbg:x': 1990.8035888671875
    'sbg:y': -230.75811767578125
  - id: apply_rm_correction
    type: boolean
    'sbg:x': 2502.2421875
    'sbg:y': -225.68701171875
  - id: apply_beam_correction
    type: boolean
    'sbg:x': 2273.170166015625
    'sbg:y': -251.86672973632812
  - id: apply_phase_correction
    type: boolean
    'sbg:x': 2750.948974609375
    'sbg:y': -241.39480590820312
  - id: parmdb
    type: File
    'sbg:x': 2007.115234375
    'sbg:y': 685.0719604492188
  - id: updateweights
    type: boolean
    'sbg:x': 2167.219970703125
    'sbg:y': 422.6792297363281
outputs:
  - id: selected_steps
    outputSource:
      - apply_phase/selected_steps
    type: Any
    'sbg:x': 3129.244873046875
    'sbg:y': 165.6987762451172
steps:
  - id: applyClock
    in:
      - id: steps
        source:
          - steps
      - id: step_name
        default: apply_clock
      - id: parmdb
        source: parmdb
      - id: solset
        default: calibrator
      - id: correction
        default: clock
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.ApplyCalStepGenerator.cwl
    'sbg:x': 1747.8311767578125
    'sbg:y': 285.2012634277344
  - id: applyTec
    in:
      - id: steps
        source:
          - apply_clock/selected_steps
      - id: step_name
        default: apply_tec
      - id: parmdb
        source: parmdb
      - id: solset
        default: calibrator
      - id: correction
        default: tec
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.ApplyCalStepGenerator.cwl
    'sbg:x': 2056.49462890625
    'sbg:y': 277.8923034667969
  - id: applyPhase
    in:
      - id: steps
        source:
          - apply_rm/selected_steps
      - id: step_name
        default: apply_phase
      - id: parmdb
        source: parmdb
      - id: solset
        default: calibrator
      - id: correction
        default: phaseOrig
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.ApplyCalStepGenerator.cwl
    'sbg:x': 2805.44873046875
    'sbg:y': 299.8974304199219
  - id: applyRM
    in:
      - id: steps
        source:
          - apply_beam/selected_steps
      - id: step_name
        default: apply_rm
      - id: parmdb
        source: parmdb
      - id: solset
        default: target
      - id: correction
        default: RMextract
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.ApplyCalStepGenerator.cwl
    'sbg:x': 2545.4775390625
    'sbg:y': 291.2240295410156
  - id: applybeam
    in:
      - id: steps
        source:
          - apply_tec/selected_steps
      - id: step_name
        default: apply_beam
      - id: usechannelfreq
        default: true
      - id: updateweights
        source: updateweights
      - id: invert
        default: true
      - id: beammode
        default: default
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.ApplyBeamStepGenerator.cwl
    'sbg:x': 2321
    'sbg:y': 285.3694763183594
  - id: apply_beam
    in:
      - id: select_a
        source: apply_beam_correction
      - id: a_steps
        source:
          - applybeam/augmented_steps
      - id: b_steps
        default: []
        source:
          - apply_tec/selected_steps
    out:
      - id: selected_steps
    run: ../lofar-cwl/steps/DP3.Selector.cwl
    label: apply_beam
    'sbg:x': 2441.14599609375
    'sbg:y': 167.0730438232422
  - id: apply_rm
    in:
      - id: select_a
        source: apply_rm_correction
      - id: a_steps
        source:
          - applyRM/augmented_steps
      - id: b_steps
        default: []
        source:
          - apply_beam/selected_steps
    out:
      - id: selected_steps
    run: ../lofar-cwl/steps/DP3.Selector.cwl
    label: apply_rm?
    'sbg:x': 2648.432373046875
    'sbg:y': 167.062255859375
  - id: apply_clock
    in:
      - id: select_a
        source: apply_clock_correction
      - id: a_steps
        source:
          - applyClock/augmented_steps
      - id: b_steps
        default: []
        source:
          - steps
    out:
      - id: selected_steps
    run: ../lofar-cwl/steps/DP3.Selector.cwl
    label: apply_clock?
    'sbg:x': 1909.072998046875
    'sbg:y': 167.82034301757812
  - id: apply_tec
    in:
      - id: select_a
        source: apply_tec_correction
      - id: a_steps
        source:
          - applyTec/augmented_steps
      - id: b_steps
        default: []
        source:
          - apply_clock/selected_steps
    out:
      - id: selected_steps
    run: ../lofar-cwl/steps/DP3.Selector.cwl
    label: apply_tec?
    'sbg:x': 2192.246826171875
    'sbg:y': 166.15206909179688
  - id: apply_phase
    in:
      - id: select_a
        source: apply_phase_correction
      - id: a_steps
        source:
          - applyPhase/augmented_steps
      - id: b_steps
        default: []
        source:
          - apply_rm/selected_steps
    out:
      - id: selected_steps
    run: ../lofar-cwl/steps/DP3.Selector.cwl
    label: apply_phase
    'sbg:x': 2927.22412109375
    'sbg:y': 162.4778289794922
requirements:
  - class: SubworkflowFeatureRequirement
