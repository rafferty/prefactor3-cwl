class: Workflow
cwlVersion: v1.0
id: smooth_calibrate
label: smooth_calibrate
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: msin
    type: 'Directory[]'
    'sbg:x': -684
    'sbg:y': -41
  - id: mode
    type:
      type: enum
      symbols:
        - scalarcomplexgain
        - scalarphase
        - scalaramplitude
        - tec
        - tecandphase
        - fulljones
        - diagonal
        - phaseonly
        - amplitudeonly
        - rotation
        - rotation+diagonal
      name: mode
    'sbg:x': -431
    'sbg:y': 145
  - id: propagate_solutions
    type: boolean
    'sbg:x': -265.89886474609375
    'sbg:y': -222.5
  - id: flagunconverged
    type: boolean
    'sbg:x': -300
    'sbg:y': 234
  - id: do_smooth
    type: boolean
    'sbg:x': -674.8988647460938
    'sbg:y': 185.5
outputs:
  - id: h5parm
    outputSource:
      - calib_cal/h5parm
    type: File[]
    'sbg:x': 104.91718292236328
    'sbg:y': -19.33123779296875
steps:
  - id: blsmooth
    in:
      - id: msin
        source: msin
      - id: do_smooth
        source: do_smooth
      - id: in_column_name
        default: DATA
      - id: out_column
        default: SMOOTHED_DATA
      - id: restore
        default: true
    out:
      - id: msout
    run: ../steps/blsmooth.cwl
    label: smooth_corrected
    scatter:
      - msin
    scatterMethod: flat_crossproduct
    'sbg:x': -394
    'sbg:y': -29
  - id: calib_cal
    in:
      - id: msin
        source: blsmooth/msout
      - id: msin_datacolumn
        default: SMOOTHED_DATA
      - id: propagate_solutions
        source: propagate_solutions
      - id: flagunconverged
        source: flagunconverged
      - id: flagdivergedonly
        default: true
      - id: mode
        source: mode
    out:
      - id: msout
      - id: h5parm
    run: ../lofar-cwl/steps/ddecal.cwl
    scatter:
      - msin
    'sbg:x': -124
    'sbg:y': -43
requirements:
  - class: ScatterFeatureRequirement
