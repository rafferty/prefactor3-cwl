class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: cat
baseCommand:
  - cat
inputs:
  - id: files
    type:
      - File
      - type: array
        items: File
    inputBinding:
      position: 0
outputs:
  - id: output
    type: stdout
label: cat
