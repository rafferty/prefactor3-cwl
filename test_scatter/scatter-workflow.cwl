class: Workflow
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: message_array
    type: 'string[]'
    'sbg:x': -62
    'sbg:y': 97
outputs:
  - id: output
    outputSource:
      - cat_2/output
    type: File
    'sbg:x': 778.4581909179688
    'sbg:y': 95.88174438476562
steps:
  - id: echo
    in:
      - id: message
        source: message_array
    out:
      - id: echo_out
    run: 1st-tool.cwl
    scatter:
      - message
    'sbg:x': 173
    'sbg:y': -17
  - id: cat
    in:
      - id: files
        source:
          - echo/echo_out
    out:
      - id: output
    run: ./cat.cwl
    label: cat
    'sbg:x': 378.203125
    'sbg:y': 21.5
  - id: 1st_tool
    in:
      - id: message
        source: message_array
    out:
      - id: echo_out
    run: ./1st-tool.cwl
    scatter:
      - message
    'sbg:x': 176.203125
    'sbg:y': 231.5
  - id: cat_1
    in:
      - id: files
        source:
          - 1st_tool/echo_out
    out:
      - id: output
    run: ./cat.cwl
    label: cat
    'sbg:x': 454.203125
    'sbg:y': 219
  - id: cat_2
    in:
      - id: files
        source:
          - cat/output
          - cat_1/output
    out:
      - id: output
    run: ./cat.cwl
    label: cat
    'sbg:x': 648.4573364257812
    'sbg:y': 104.00638580322266
requirements:
  - class: ScatterFeatureRequirement
  - class: MultipleInputFeatureRequirement
