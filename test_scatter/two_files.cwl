class: Workflow
cwlVersion: v1.0
id: two_files
label: two_files
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: message
    type: string
    'sbg:x': -327.3938293457031
    'sbg:y': -142
  - id: message_1
    type: string
    'sbg:x': -332.8984375
    'sbg:y': 94
outputs:
  - id: echo_out
    outputSource: combine/out_files
    type: 'File[]'
    'sbg:x': 112
    'sbg:y': -88
steps:
  - id: 1st_tool
    in:
      - id: message
        source: message
    out:
      - id: echo_out
    run: ./1st-tool.cwl
    'sbg:x': -88
    'sbg:y': -185
  - id: 1st_tool_1
    in:
      - id: message
        source: message_1
    out:
      - id: echo_out
    run: ./1st-tool.cwl
    'sbg:x': -89
    'sbg:y': -27
  - id: combine
    in: 
      input_file:
        source: 
          - 1st_tool_1/echo_out
          - 1st_tool/echo_out
    out:
      - id: out_files
    run:
      class: ExpressionTool
      inputs: 
        input_file:
          type: File[]
      outputs: 
        out_files:
          type: File[]
      expression: | 
        ${
          var output = []
          for(var index in inputs.input_file){
              var current_file = inputs.input_file[index]
              current_file.basename = 'output_' + index 
              output.push(current_file)
          }
          return {out_files: output}
        }
requirements: 
 - class: MultipleInputFeatureRequirement
 - class: InlineJavascriptRequirement
