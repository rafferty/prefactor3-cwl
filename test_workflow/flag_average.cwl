class: Workflow
cwlVersion: v1.0
id: test
label: test
$namespaces:
  sbg: 'https://www.sevenbridges.com'
inputs:
  - id: msin
    type: Directory
    'sbg:x': -883.489990234375
    'sbg:y': -201.6757354736328
outputs:
  - id: msout
    outputSource:
      - dppp/msout
    type: Directory
    'sbg:x': -616.7398071289062
    'sbg:y': -472
steps:
  - id: aoflagger_step_generator
    in: []
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.AOFlaggerStepGenerator.cwl
    'sbg:x': -1357.640625
    'sbg:y': -729.5
  - id: average_step_generator
    in:
      - id: steps
        source:
          - aoflagger_step_generator/augmented_steps
    out:
      - id: augmented_steps
    run: ../lofar-cwl/steps/DP3.AveragerStepGenerator.cwl
    'sbg:x': -1125
    'sbg:y': -729
  - id: parset_generator
    in:
      - id: steps
        source:
          - average_step_generator/augmented_steps
    out:
      - id: parset
      - id: input_files
      - id: input_directories
      - id: output_file_names
      - id: output_directory_names
    run: ../lofar-cwl/steps/DP3.ParsetGenerator.cwl
    'sbg:x': -1174
    'sbg:y': -525
  - id: dppp
    in:
      - id: parset
        source: parset_generator/parset
      - id: msin
        source: msin
      - id: secondary_files
        source:
          - parset_generator/input_files
      - id: output_file_names
        source: parset_generator/output_file_names
      - id: output_directory_names
        source: parset_generator/output_directory_names
    out:
      - id: msout
      - id: secondary_output_files
      - id: secondary_output_directories
    run: ../lofar-cwl/steps/DPPP.cwl
    'sbg:x': -806.3514404296875
    'sbg:y': -505.4043273925781
requirements:
  - class: SubworkflowFeatureRequirement
