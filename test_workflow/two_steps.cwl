class: Workflow
cwlVersion: v1.0
id: prefactor
$namespaces:
  sbg: 'https://www.sevenbridges.com'
inputs:
  - id: elevation_to_flag
    type: string
    'sbg:x': -1451
    'sbg:y': -474
  - id: memoryperc
    type: int
    'sbg:x': -1456
    'sbg:y': -576
  - id: min_amplitude_to_flag
    type: float
    'sbg:x': -1442
    'sbg:y': -712
  - id: msin_array
    type: 'Directory[]'
    'sbg:x': -1482.3096923828125
    'sbg:y': -821.4769897460938
  - id: raw_data
    type: boolean
    'sbg:x': -1450
    'sbg:y': -934.9942626953125
  - id: demix
    type: boolean
    'sbg:x': -1449.65625
    'sbg:y': -357.9942321777344
outputs:
  - id: demix_parmdb
    outputSource:
      - ndppp_prep_cal/demix_parmdb
    type: Directory
    'sbg:x': -795.9163208007812
    'sbg:y': -304
steps:
  - id: ndppp_prep_cal
    scatter: msin
    in:
      - id: elevation_to_flag
        source: elevation_to_flag
      - id: min_amplitude_to_flag
        source: min_amplitude_to_flag
      - id: memoryperc
        source: memoryperc
      - id: raw_data
        source: raw_data
      - id: demix
        source: demix
      - id: msin
        source: msin
    out:
      - id: msout
      - id: demix_parmdb
    run: ../subworkflow/ndppp_prep_cal.cwl
    label: ndppp_prep_cal
    'sbg:x': -1057.995849609375
    'sbg:y': -464.80194091796875
requirements:
  SubworkflowFeatureRequirement: {}
  ScatterFeatureRequirement: {}
  
