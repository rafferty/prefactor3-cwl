#!/usr/bin/env python3
import sys


def print_step(line): 
    line_parsed = list(map(lambda x: x.strip(), line)) 
    if len(line_parsed) < 4:
        return
    parameter_name, type_name, default, doc = line_parsed
    parameter_name = parameter_name.strip('.').replace('<step>.', '')
    print('- id: %s\n  type: %s\n  default: %s\n  doc: %s\n'
          '  inputBinding:\n    prefix: %s=\n    separate: false' % (parameter_name,
                                                              type_name,
                                                              default,
                                                              doc,
                                                              parameter_name)) 


lines = [line.split('\t') for line in open(sys.argv[1]).read().splitlines()]
for line in lines:
    if line:
        print_step(line)
