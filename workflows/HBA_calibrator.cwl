class: Workflow
cwlVersion: v1.0
id: HBA_calibrator
label: HBA_calibrator
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: msin
    type: 'Directory[]'
    'sbg:x': -1000
    'sbg:y': -1900
  - id: refant
    type: string?
    default: 'CS00.*'
    'sbg:x': -1000
    'sbg:y': -1800
  - id: flag_baselines
    type: 'string[]?'
    default: []
    'sbg:x': -1000
    'sbg:y': -1700
  - id: process_baselines_cal
    type: string?
    default: '*&'
    'sbg:x': -1000
    'sbg:y': -1600
  - id: filter_baselines
    type: string?
    default: '*&'
    'sbg:x': -1000
    'sbg:y': -1500
  - id: fit_offset_PA
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': -1400
  - id: do_smooth
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': -1300
  - id: rfistrategy
    type: string?
    default: HBAdefault.rfis
    'sbg:x': -1000
    'sbg:y': -1200
  - id: max2interpolate
    type: int?
    default: 30
    'sbg:x': -1000
    'sbg:y': -1100
  - id: ampRange
    type: 'float[]?'
    default:
      - 0
      - 0
    'sbg:x': -1000
    'sbg:y': -1000
  - id: skip_international
    type: boolean?
    default: true
    'sbg:x': -1000
    'sbg:y': -900
  - id: raw_data
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': -800
  - id: propagatesolutions
    type: boolean?
    default: true
    'sbg:x': -1000
    'sbg:y': -700
  - id: flagunconverged
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': -600
  - id: maxStddev
    type: float?
    default: -1.0
    'sbg:x': -1000
    'sbg:y': -500
  - id: solutions2transfer
    type: File?
    'sbg:x': -1000
    'sbg:y': -400
  - id: antennas2transfer
    type: string?
    default: '[FUSPID].*'
    'sbg:x': -1000
    'sbg:y': -300
  - id: do_transfer
    type: boolean
    default: false
    'sbg:x': -1000
    'sbg:y': -200
  - id: trusted_sources
    type: string
    default: '3C48,3C147,3C196,3C295,3C380'
    'sbg:x': -1000
    'sbg:y': -100
  - id: demix_sources
    type: 'string[]?'
    default:
      - CasA
      - CygA
    'sbg:x': -1000
    'sbg:y': 0
  - id: demix_target
    type: string?
    default: ''
    'sbg:x': -1000
    'sbg:y': 100
  - id: demix_freqstep
    type: int?
    default: 16
    'sbg:x': -1000
    'sbg:y': 200
  - id: demix_timestep
    type: int?
    default: 10
    'sbg:x': -1000
    'sbg:y': 300
  - id: demix
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': 400
  - id: ion_3rd
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': 500
  - id: clock_smooth
    type: boolean?
    default: true
    'sbg:x': -1000
    'sbg:y': 600
  - id: tables2export
    type: string?
    default: clock
    'sbg:x': -1000
    'sbg:y': 700
  - id: final_apply
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': 800
  - id: max_dppp_threads
    type: int?
    default: 10
    'sbg:x': -1000
    'sbg:y': 900
  - id: memoryperc
    type: int?
    default: 20
    'sbg:x': -1000
    'sbg:y': 1000
  - id: min_length
    type: int?
    default: 50
    'sbg:x': -1000
    'sbg:y': 1100
  - id: overhead
    type: float?
    default: 0.8
    'sbg:x': -1000
    'sbg:y': 1200
  - id: min_separation
    type: int?
    default: 30
    'sbg:x': -1000
    'sbg:y': 1300
  - id: max_separation_arcmin
    type: float?
    default: 1.0
    'sbg:x': -1000
    'sbg:y': 1400
  - id: calibrator_path_skymodel
    type: Directory?
    'sbg:x': -1000
    'sbg:y': 1500
  - id: A-Team_skymodel
    type: File?
    'sbg:x': -1000
    'sbg:y': 1600
  - id: avg_timeresolution
    type: float?
    default: 4
    'sbg:x': -1000
    'sbg:y': 1700
  - id: avg_freqresolution
    type: string?
    default: 48.82kHz
    'sbg:x': -1000
    'sbg:y': 1800
  - id: bandpass_freqresolution
    type: string?
    default: 195.3125kHz
    'sbg:x': -1000
    'sbg:y': 1900
outputs:
  - id: results
    outputSource:
      - save_results/log_dir
    type: Directory
    'sbg:x': 1500
    'sbg:y': 0
steps:
  - id: prefactor
    in:
      - id: msin
        source:
          - msin
      - id: refant
        source: refant
      - id: flag_baselines
        source:
          - flag_baselines
      - id: process_baselines_cal
        source: process_baselines_cal
      - id: filter_baselines
        source: filter_baselines
      - id: fit_offset_PA
        source: fit_offset_PA
      - id: do_smooth
        source: do_smooth
      - id: rfistrategy
        source: rfistrategy
      - id: max2interpolate
        source: max2interpolate
      - id: ampRange
        source:
          - ampRange
      - id: skip_international
        source: skip_international
      - id: raw_data
        source: raw_data
      - id: propagatesolutions
        source: propagatesolutions
      - id: flagunconverged
        source: flagunconverged
      - id: maxStddev
        source: maxStddev
      - id: solutions2transfer
        source: solutions2transfer
      - id: antennas2transfer
        source: antennas2transfer
      - id: do_transfer
        source: do_transfer
      - id: trusted_sources
        source: trusted_sources
      - id: demix_sources
        source:
          - demix_sources
      - id: demix_target
        source: demix_target
      - id: demix_freqstep
        source: demix_freqstep
      - id: demix_timestep
        source: demix_timestep
      - id: demix
        source: demix
      - id: ion_3rd
        source: ion_3rd
      - id: clock_smooth
        source: clock_smooth
      - id: tables2export
        source: tables2export
      - id: final_apply
        source: final_apply
      - id: max_dppp_threads
        source: max_dppp_threads
      - id: memoryperc
        source: memoryperc
      - id: min_length
        source: min_length
      - id: overhead
        source: overhead
      - id: min_separation
        source: min_separation
      - id: max_separation_arcmin
        source: max_separation_arcmin
      - id: calibrator_path_skymodel
        source: calibrator_path_skymodel
      - id: A-Team_skymodel
        source: A-Team_skymodel
      - id: avg_timeresolution
        source: avg_timeresolution
      - id: avg_freqresolution
        source: avg_freqresolution
      - id: bandpass_freqresolution
        source: bandpass_freqresolution
    out:
      - id: logfiles
      - id: solutions
      - id: inspection
    run: ./prefactor_calibrator.cwl
    label: prefactor_calibrator
    'sbg:x': 0
    'sbg:y': 0
  - id: save_logfiles
    in:
      - id: log_files
        linkMerge: merge_flattened
        source:
          - prefactor/logfiles
      - id: sub_directory_name
        default: logs
    out:
      - id: log_dir
    run: ./../steps/collectlog.cwl
    label: save_logfiles
    'sbg:x': 1000
    'sbg:y': -200
  - id: save_inspection
    in:
      - id: log_files
        linkMerge: merge_flattened
        source:
          - prefactor/inspection
      - id: sub_directory_name
        default: inspection
    out:
      - id: log_dir
    run: ./../steps/collectlog.cwl
    label: save_inspection
    'sbg:x': 1000
    'sbg:y': 0
  - id: save_solutions
    in:
      - id: log_files
        source:
          - prefactor/solutions
      - id: sub_directory_name
        default: cal_values
    out:
      - id: log_dir
    run: ./../steps/collectlog.cwl
    label: save_solutions
    'sbg:x': 1000
    'sbg:y': 200
  - id: save_results
    in:
      - id: log_files
        linkMerge: merge_flattened
        source:
          - save_solutions/log_dir
          - save_inspection/log_dir
          - save_logfiles/log_dir
      - id: sub_directory_name
        default: results
    out:
      - id: log_dir
    run: ./../steps/collectlog.cwl
    label: save_results
    'sbg:x': 1200
    'sbg:y': 0
requirements:
  - class: SubworkflowFeatureRequirement
  - class: MultipleInputFeatureRequirement
  
