class: Workflow
cwlVersion: v1.0
id: HBA_target
label: HBA_target
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: msin
    type: 'Directory[]'
    'sbg:x': -1000
    'sbg:y': -1700
  - id: cal_solutions
    type: 'File'
    'sbg:x': -1000
    'sbg:y': -1600
  - id: refant
    type: string?
    default: 'CS00.*'
    'sbg:x': -1000
    'sbg:y': -1500
  - id: flag_baselines
    type: 'string[]?'
    default: []
    'sbg:x': -1000
    'sbg:y': -1400
  - id: process_baselines_target
    type: string?
    default: '[CR]S*&'
    'sbg:x': -1000
    'sbg:y': -1300
  - id: filter_baselines
    type: string?
    default: '[CR]S*&'
    'sbg:x': -1000
    'sbg:y': -1200
  - id: do_smooth
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': -1100
  - id: rfistrategy
    type: string?
    default: HBAdefault.rfis
    'sbg:x': -1000
    'sbg:y': -1000
  - id: min_unflagged_fraction
    type: float?
    default: 0.5
    'sbg:x': -1000
    'sbg:y': -900
  - id: compression_bitrate
    type: int?
    default: 16
    'sbg:x': -1000
    'sbg:y': -800
  - id: raw_data
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': -700
  - id: propagatesolutions
    type: boolean?
    default: true
    'sbg:x': -1000
    'sbg:y': -600
  - id: demix_sources
    type: 'string[]?'
    default:
      - CasA
      - CygA
    'sbg:x': -1000
    'sbg:y': -500
  - id: demix_target
    type: string?
    default: ''
    'sbg:x': -1000
    'sbg:y': -400
  - id: demix_freqstep
    type: int?
    default: 16
    'sbg:x': -1000
    'sbg:y': -300
  - id: demix_timestep
    type: int?
    default: 10
    'sbg:x': -1000
    'sbg:y': -200
  - id: demix
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': -100
  - id: apply_tec
    type: boolean
    default: false
    'sbg:x': -1000
    'sbg:y':  0
  - id: apply_clock
    type: boolean
    default: true
    'sbg:x': -1000
    'sbg:y': 100
  - id: apply_phase
    type: boolean
    default: false
    'sbg:x': -1000
    'sbg:y': 200
  - id: apply_RM
    type: boolean
    default: true
    'sbg:x': -1000
    'sbg:y': 300
  - id: apply_beam
    type: boolean
    default: true
    'sbg:x': -1000
    'sbg:y': 400
  - id: clipATeam
    type: boolean?
    default: true
    'sbg:x': -1000
    'sbg:y': 500
  - id: gsmcal_step
    type: string?
    default: 'phase'
    'sbg:x': -1000
    'sbg:y': 600
  - id: updateweights
    type: boolean?
    default: true
    'sbg:x': -1000
    'sbg:y': 700
  - id: max_dppp_threads
    type: int?
    default: 10
    'sbg:x': -1000
    'sbg:y': 800
  - id: memoryperc
    type: int?
    default: 20
    'sbg:x': -1000
    'sbg:y': 900
  - id: min_length
    type: int?
    default: 5
    'sbg:x': -1000
    'sbg:y': 1000
  - id: overhead
    type: float?
    default: 0.7
    'sbg:x': -1000
    'sbg:y': 1100
  - id: min_separation
    type: int?
    default: 30
    'sbg:x': -1000
    'sbg:y': 1200
  - id: A-Team_skymodel
    type: File?
    'sbg:x': -1000
    'sbg:y': 1300
  - id: target_skymodel
    type: File?
    'sbg:x': -1000
    'sbg:y': 1400
  - id: use_target
    type: boolean?
    default: true
    'sbg:x': -1000
    'sbg:y': 1500
  - id: skymodel_source
    type: string?
    default: 'TGSS'
    'sbg:x': -1000
    'sbg:y': 1600
  - id: avg_timeresolution
    type: int?
    default: 4
    'sbg:x': -1000
    'sbg:y': 1700
  - id: avg_freqresolution
    type: string?
    default: 48.82kHz
    'sbg:x': -1000
    'sbg:y': 1800
  - id: avg_timeresolution_concat
    type: int?
    default: 8
    'sbg:x': -1000
    'sbg:y': 1900
  - id: avg_freqresolution_concat
    type: string?
    default: 97.64kHz
    'sbg:x': -1000
    'sbg:y': 2000
  - id: num_SBs_per_group
    type: int?
    default: 10
    'sbg:x': -1000
    'sbg:y': 2100
  - id: reference_stationSB
    type: int?
    default: null
    'sbg:x': -1000
    'sbg:y': 2200
  - id: ionex_server
    type: string?
    default: 'ftp://ftp.aiub.unibe.ch/CODE/'
    'sbg:x': -1000
    'sbg:y': 2300
  - id: ionex_prefix
    type: string?
    default: 'CODG'
    'sbg:x': -1000
    'sbg:y': 2400
  - id: proxy_server
    type: string?
    default: null
    'sbg:x': -1000
    'sbg:y': 2500
  - id: proxy_port
    type: string?
    default: null
    'sbg:x': -1000
    'sbg:y': 2600
  - id: proxy_type
    type: string?
    default: null
    'sbg:x': -1000
    'sbg:y': 2700
  - id: proxy_user
    type: string?
    default: null
    'sbg:x': -1000
    'sbg:y': 2800
  - id: proxy_pass
    type: string?
    default: null
    'sbg:x': -1000
    'sbg:y': 2900
outputs:
  - id: results
    outputSource:
      - save_results/log_dir
    type: Directory
    'sbg:x': 1500
    'sbg:y': 0
steps:
  - id: prefactor
    in:
      - id: msin
        source:
          - msin
      - id: cal_solutions
        source: cal_solutions
      - id: refant
        source: refant
      - id: flag_baselines
        source:
          - flag_baselines
      - id: process_baselines_target
        source: process_baselines_target
      - id: filter_baselines
        source: filter_baselines
      - id: do_smooth
        source: do_smooth
      - id: rfistrategy
        source: rfistrategy
      - id: min_unflagged_fraction
        source: min_unflagged_fraction
      - id: compression_bitrate
        source: compression_bitrate
      - id: raw_data
        source: raw_data
      - id: propagatesolutions
        source: propagatesolutions
      - id: apply_tec
        source: apply_tec
      - id: apply_clock
        source: apply_clock
      - id: apply_phase
        source: apply_phase
      - id: apply_RM
        source: apply_RM
      - id: apply_beam
        source: apply_beam
      - id: demix_sources
        source:
          - demix_sources
      - id: demix_target
        source: demix_target
      - id: demix_freqstep
        source: demix_freqstep
      - id: demix_timestep
        source: demix_timestep
      - id: demix
        source: demix
      - id: clipATeam
        source: clipATeam
      - id: updateweights
        source: updateweights
      - id: max_dppp_threads
        source: max_dppp_threads
      - id: memoryperc
        source: memoryperc
      - id: min_length
        source: min_length
      - id: overhead
        source: overhead
      - id: min_separation
        source: min_separation
      - id: A-Team_skymodel
        source: A-Team_skymodel
      - id: target_skymodel
        source: target_skymodel
      - id: use_target
        source: use_target
      - id: skymodel_source
        source: skymodel_source
      - id: avg_timeresolution
        source: avg_timeresolution
      - id: avg_freqresolution
        source: avg_freqresolution
      - id: avg_timeresolution_concat
        source: avg_timeresolution_concat
      - id: avg_freqresolution_concat
        source: avg_freqresolution_concat
      - id: num_SBs_per_group
        source: num_SBs_per_group
      - id: reference_stationSB
        source: reference_stationSB
      - id: ionex_server
        source: ionex_server
      - id: ionex_prefix
        source: ionex_prefix
      - id: proxy_server
        source: proxy_server
      - id: proxy_port
        source: proxy_port
      - id: proxy_type
        source: proxy_type
      - id: proxy_user
        source: proxy_user
      - id: proxy_pass
        source: proxy_pass
    out:
      - id: logfiles
      - id: msout
      - id: solutions
      - id: inspection
    run: ./prefactor_target.cwl
    label: prefactor_target
    'sbg:x': 0
    'sbg:y': 0
  - id: save_logfiles
    in:
      - id: log_files
        linkMerge: merge_flattened
        source:
          - prefactor/logfiles
      - id: sub_directory_name
        default: logs
    out:
      - id: log_dir
    run: ./../steps/collectlog.cwl
    label: save_logfiles
    'sbg:x': 1000
    'sbg:y': -200
  - id: save_inspection
    in:
      - id: log_files
        linkMerge: merge_flattened
        source:
          - prefactor/inspection
      - id: sub_directory_name
        default: inspection
    out:
      - id: log_dir
    run: ./../steps/collectlog.cwl
    label: save_inspection
    'sbg:x': 1000
    'sbg:y': 0
  - id: save_solutions
    in:
      - id: log_files
        source:
          - prefactor/solutions
      - id: sub_directory_name
        default: cal_values
    out:
      - id: log_dir
    run: ./../steps/collectlog.cwl
    label: save_solutions
    'sbg:x': 1000
    'sbg:y': 200
  - id: save_results
    in:
      - id: log_files
        linkMerge: merge_flattened
        source:
          - save_solutions/log_dir
          - save_inspection/log_dir
          - save_logfiles/log_dir
          - prefactor/msout
      - id: sub_directory_name
        default: results
    out:
      - id: log_dir
    run: ./../steps/collectlog.cwl
    label: save_results
    'sbg:x': 1200
    'sbg:y': 0
requirements:
  - class: SubworkflowFeatureRequirement
  - class: MultipleInputFeatureRequirement
