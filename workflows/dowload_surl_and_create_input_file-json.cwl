class: Workflow
cwlVersion: v1.0
id: dowload_surl_and_create_input_file_json
label: dowload_surl_and_create_input_file.json

inputs:
  - id: surl_list
    type: File
outputs:
  - id: results
    outputSource:
      - _h_b_a_calibrator/results
    type: Directory
  - id: logs
    outputSource:
      - _h_b_a_calibrator/logs
    type: Directory
steps:
  - id: read_surl_list
    in:
      - id: surl_list
        source: surl_list
    out:
      - id: surls
    run: ../steps/read_surl_list.cwl
    label: ReadSurlList
  - id: surl_copy
    in:
      - id: surl
        source: stage/output
    out:
      - id: output
    run: ../steps/surl_copy.cwl
    label: surl_copy
    scatter:
      - surl
  - id: untar
    in:
      - id: tar_file
        source: surl_copy/output
    out:
      - id: uncompressed
    run: ../steps/untar.cwl
    label: untar
    scatter:
      - tar_file
  - id: stage
    in:
      - id: input
        source: read_surl_list/surls
    out:
      - id: output
    run: ../steps/stage.cwl
    label: stage
    scatter:
      - input
  - id: _h_b_a_calibrator
    in:
      - id: msin
        source:
          - untar/uncompressed
    out:
      - id: logs
      - id: results
    run: ./HBA_calibrator.cwl
    label: HBA_calibrator
requirements:
  - class: SubworkflowFeatureRequirement
  - class: ScatterFeatureRequirement
  - class: InlineJavascriptRequirement
  - class: StepInputExpressionRequirement
