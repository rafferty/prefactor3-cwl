class: Workflow
cwlVersion: v1.0
id: prefactor_calibrator
label: prefactor_calibrator
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: msin
    type: 'Directory[]'
    'sbg:x': -1000
    'sbg:y': -1900
  - id: refant
    type: string?
    default: 'CS00.*'
    'sbg:x': -1000
    'sbg:y': -1800
  - id: flag_baselines
    type: 'string[]?'
    default: []
    'sbg:x': -1000
    'sbg:y': -1700
  - id: process_baselines_cal
    type: string?
    default: '*&'
    'sbg:x': -1000
    'sbg:y': -1600
  - id: filter_baselines
    type: string?
    default: '*&'
    'sbg:x': -1000
    'sbg:y': -1500
  - id: fit_offset_PA
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': -1400
  - id: do_smooth
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': -1300
  - id: rfistrategy
    type: string?
    default: HBAdefault.rfis
    'sbg:x': -1000
    'sbg:y': -1200
  - id: max2interpolate
    type: int?
    default: 30
    'sbg:x': -1000
    'sbg:y': -1100
  - id: ampRange
    type: 'float[]?'
    default:
      - 0
      - 0
    'sbg:x': -1000
    'sbg:y': -1000
  - id: skip_international
    type: boolean?
    default: true
    'sbg:x': -1000
    'sbg:y': -900
  - id: raw_data
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': -800
  - id: propagatesolutions
    type: boolean?
    default: true
    'sbg:x': -1000
    'sbg:y': -700
  - id: flagunconverged
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': -600
  - id: maxStddev
    type: float?
    default: -1
    'sbg:x': -1000
    'sbg:y': -500
  - id: solutions2transfer
    type: File?
    'sbg:x': -1000
    'sbg:y': -400
  - id: antennas2transfer
    type: string?
    default: '[FUSPID].*'
    'sbg:x': -1000
    'sbg:y': -300
  - id: do_transfer
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': -200
  - id: trusted_sources
    type: string
    default: '3C48,3C147,3C196,3C295'
    'sbg:x': -1000
    'sbg:y': -100
  - id: demix_sources
    type: 'string[]?'
    default:
      - CasA
      - CygA
    'sbg:x': -1000
    'sbg:y': 0
  - id: demix_target
    type: string?
    default: ''
    'sbg:x': -1000
    'sbg:y': 100
  - id: demix_freqstep
    type: int?
    default: 16
    'sbg:x': -1000
    'sbg:y': 200
  - id: demix_timestep
    type: int?
    default: 10
    'sbg:x': -1000
    'sbg:y': 300
  - id: demix
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': 400
  - id: ion_3rd
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': 500
  - id: clock_smooth
    type: boolean?
    default: true
    'sbg:x': -1000
    'sbg:y': 600
  - id: tables2export
    type: string?
    default: clock
    'sbg:x': -1000
    'sbg:y': 700
  - id: final_apply
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': 800
  - id: max_dppp_threads
    type: int?
    default: 10
    'sbg:x': -1000
    'sbg:y': 900
  - id: memoryperc
    type: int?
    default: 20
    'sbg:x': -1000
    'sbg:y': 1000
  - id: min_length
    type: int?
    default: 50
    'sbg:x': -1000
    'sbg:y': 1100
  - id: overhead
    type: float?
    default: 0.8
    'sbg:x': -1000
    'sbg:y': 1200
  - id: min_separation
    type: int?
    default: 30
    'sbg:x': -1000
    'sbg:y': 1300
  - id: max_separation_arcmin
    type: float?
    default: 1
    'sbg:x': -1000
    'sbg:y': 1400
  - id: calibrator_path_skymodel
    type: Directory?
    'sbg:x': -1000
    'sbg:y': 1500
  - id: A-Team_skymodel
    type: File?
    'sbg:x': -1000
    'sbg:y': 1600
  - id: avg_timeresolution
    type: float?
    default: 4
    'sbg:x': -1000
    'sbg:y': 1700
  - id: avg_freqresolution
    type: string?
    default: 48.82kHz
    'sbg:x': -1000
    'sbg:y': 1800
  - id: bandpass_freqresolution
    type: string?
    default: 195.3125kHz
    'sbg:x': -1000
    'sbg:y': 1900
outputs:
  - id: inspection
    linkMerge: merge_flattened
    outputSource:
      - prep/check_Ateam_separation.png 
      - pa/inspection
      - fr/inspection
      - bp/inspection
      - ion/inspection
    type: File[]
    'sbg:x': 2000
    'sbg:y': -600
  - id: solutions
    outputSource:
      - ion/outsolutions
    type: File
    'sbg:x': 2000
    'sbg:y': -500
  - id: logfiles
    outputSource:
      - prep/logfiles
      - concat_logfiles_RefAnt/output
      - pa/logfiles
      - fr/logfiles
      - bp/logfiles
      - ion/logfiles
    type: 'File[]'
    linkMerge: merge_flattened
    'sbg:x': 2000
    'sbg:y': 600
steps:
  - id: prep
    in:
      - id: msin
        source:
          - msin
      - id: filter_baselines
        source: filter_baselines
      - id: raw_data
        source: raw_data
      - id: propagatesolutions
        source: propagatesolutions
      - id: flagunconverged
        source: flagunconverged
      - id: demix
        source: demix
      - id: max_dppp_threads
        source: max_dppp_threads
      - id: memoryperc
        source: memoryperc
      - id: flag_baselines
        source:
          - flag_baselines
      - id: avg_timeresolution
        source: avg_timeresolution
      - id: avg_freqresolution
        source: avg_freqresolution
      - id: process_baselines_cal
        source: process_baselines_cal
      - id: demix_timestep
        source: demix_timestep
      - id: demix_freqstep
        source: demix_freqstep
      - id: demix_target
        source: demix_target
      - id: demix_sources
        source:
          - demix_sources
      - id: min_length
        source: min_length
      - id: overhead
        source: overhead
      - id: min_separation
        source: min_separation
      - id: do_smooth
        source: do_smooth
      - id: max_separation_arcmin
        source: max_separation_arcmin
      - id: A-Team_skymodel
        source: A-Team_skymodel
      - id: calibrator_path_skymodel
        source: calibrator_path_skymodel
    out:
      - id: outh5parm
      - id: logfiles
      - id: outh5parm_logfile
      - id: check_Ateam_separation.png
      - id: msout
      - id: parset
      - id: calibrator_name
    run: ./prefactor_calibrator/prep.cwl
    label: prep
    'sbg:x': 0
    'sbg:y': 0
  - id: findRefAnt
    in:
      - id: msin
        source: prep/msout
    out:
      - id: flagged_fraction_dict
      - id: logfile
    run: ./../steps/findRefAnt.cwl
    label: findRefAnt
    scatter:
      - msin
    'sbg:x': 300
    'sbg:y': 0
  - id: findRefAnt_join
    in:
      - id: flagged_fraction_dict
        source: findRefAnt/flagged_fraction_dict
      - id: filter_station
        source: refant
    out:
      - id: refant
      - id: logfile
    run: ./../steps/findRefAnt_join.cwl
    label: findRefAnt_join
    'sbg:x': 500
    'sbg:y': 0
  - id: concat_logfiles_RefAnt
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - findRefAnt/logfile
          - findRefAnt_join/logfile
      - id: file_prefix
        default: findRefAnt
    out:
      - id: output
    run: ./../steps/concatenate_files.cwl
    label: concat_logfiles_RefAnt
    'sbg:x': 700
    'sbg:y': 300

  - id: pa
    in:
      - id: msin
        source:
          - prep/msout
      - id: h5parm
        source:
          - prep/outh5parm
      - id: refant
        source: findRefAnt_join/refant
      - id: inh5parm_logfile
        source:
          - prep/outh5parm_logfile
      - id: flagunconverged
        source: flagunconverged
      - id: propagatesolutions
        source: propagatesolutions
      - id: do_smooth
        source: do_smooth
      - id: fit_offset_PA
        source: fit_offset_PA
    out:
      - id: msout
      - id: outsolutions
      - id: inspection
      - id: logfiles
      - id: outh5parm
      - id: outh5parm_logfile
    run: ./prefactor_calibrator/pa.cwl
    label: PA
    'sbg:x': 700
    'sbg:y': 0
  - id: fr
    in:
      - id: msin
        source:
          - pa/msout
      - id: h5parm
        source:
          - pa/outh5parm
      - id: refant
        source: findRefAnt_join/refant
      - id: inh5parm_logfile
        source:
          - pa/outh5parm_logfile
      - id: flagunconverged
        source: flagunconverged
      - id: propagatesolutions
        source: propagatesolutions
      - id: do_smooth
        source: do_smooth
      - id: insolutions
        source: pa/outsolutions
    out:
      - id: msout
      - id: outsolutions
      - id: inspection
      - id: logfiles
      - id: outh5parm
      - id: outh5parm_logfile
    run: ./prefactor_calibrator/fr.cwl
    label: FR
    'sbg:x': 1000
    'sbg:y': 0
  - id: bp
    in:
      - id: msin
        source:
          - fr/msout
      - id: h5parm
        source:
          - fr/outh5parm
      - id: refant
        source: findRefAnt_join/refant
      - id: inh5parm_logfile
        source:
          - fr/outh5parm_logfile
      - id: flagunconverged
        source: flagunconverged
      - id: propagatesolutions
        source: propagatesolutions
      - id: ampRange
        source: ampRange
      - id: skipInternational
        source: skip_international
      - id: max2interpolate
        source: max2interpolate
      - id: bandpass_freqresolution
        source: bandpass_freqresolution
      - id: avg_freqresolution
        source: avg_freqresolution
      - id: do_smooth
        source: do_smooth
      - id: insolutions
        source: fr/outsolutions
      - id: solutions2transfer
        source: solutions2transfer
      - id: antennas2transfer
        source: antennas2transfer
      - id: do_transfer
        source: do_transfer
      - id: trusted_sources
        source: trusted_sources
      - id: max_separation_arcmin
        source: max_separation_arcmin
      - id: calibrator_name
        source: prep/calibrator_name
    out:
      - id: outsolutions
      - id: inspection
      - id: logfiles
      - id: outh5parm
      - id: outh5parm_logfile
    run: ./prefactor_calibrator/bp.cwl
    label: BP
    'sbg:x': 1300
    'sbg:y': 0
  - id: ion
    in:
      - id: h5parm
        source:
          - bp/outh5parm
      - id: refant
        source: findRefAnt_join/refant
      - id: inh5parm_logfile
        source:
          - bp/outh5parm_logfile
      - id: insolutions
        source: bp/outsolutions
      - id: maxStddev
        source: maxStddev
      - id: ion_3rd
        source: ion_3rd
      - id: tables2export
        source: tables2export
      - id: clock_smooth
        source: clock_smooth
      - id: calibrator_name
        source: prep/calibrator_name
    out:
      - id: outsolutions
      - id: inspection
      - id: logfiles
    run: ./prefactor_calibrator/ion.cwl
    label: ion
    'sbg:x': 1600
    'sbg:y': 0
requirements:
  - class: SubworkflowFeatureRequirement
  - class: ScatterFeatureRequirement