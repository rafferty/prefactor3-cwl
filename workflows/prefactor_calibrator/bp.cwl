class: Workflow
cwlVersion: v1.0
id: bp
label: BP
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: max_separation_arcmin
    type: float
    'sbg:x': -1000
    'sbg:y': -800
  - id: calibrator_name
    type: string
    'sbg:x': -1000
    'sbg:y': -700
  - id: ampRange
    type: 'float[]?'
    'sbg:x': -1000
    'sbg:y': -600
  - id: skipInternational
    type: boolean?
    'sbg:x': -1000
    'sbg:y': -500
  - id: max2interpolate
    type: int?
    'sbg:x': -1000
    'sbg:y': -400
  - id: bandpass_freqresolution
    type: string
    'sbg:x': -1000
    'sbg:y': -300
  - id: avg_freqresolution
    type: string?
    'sbg:x': -1000
    'sbg:y': -200
  - id: flagunconverged
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': -100
  - id: propagatesolutions
    type: boolean?
    default: true
    'sbg:x': -1000
    'sbg:y': 0
  - id: msin
    type: 'Directory[]'
    'sbg:x': -1000
    'sbg:y': 100
  - id: h5parm
    type: File
    'sbg:x': -1000
    'sbg:y': 200
  - id: solutions2transfer
    type: File?
    'sbg:x': -1000
    'sbg:y': 300
  - id: inh5parm_logfile
    type: 'File[]'
    'sbg:x': -1000
    'sbg:y': 400
  - id: do_smooth
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': 500
  - id: insolutions
    type: File
    'sbg:x': -1000
    'sbg:y': 600
  - id: antennas2transfer
    type: string?
    'sbg:x': -1000
    'sbg:y': 700
  - id: do_transfer
    type: boolean?
    'sbg:x': -1000
    'sbg:y': 800
  - id: trusted_sources
    type: string?
    'sbg:x': -1000
    'sbg:y': 900
outputs:
  - id: outh5parm
    outputSource:
      - h5parm_collector/outh5parm
    type: File
    'sbg:x': 1000
    'sbg:y': -200
  - id: inspection
    outputSource:
      - losoto_plot_A1/output_plots
      - losoto_plot_A2/output_plots
      - losoto_plot_B1/output_plots
      - losoto_plot_B2/output_plots
      - losoto_plot_B3/output_plots
      - transfer_solutions/plots
    type: 'File[]?'
    linkMerge: merge_flattened
    'sbg:x': 1000
    'sbg:y': 0
  - id: outsolutions
    outputSource:
      - transfer_solutions/outh5parm
    type: File
    'sbg:x': 1000
    'sbg:y': 100
  - id: logfiles
    outputSource:
      - concat_logfiles_bp/output
      - concat_logfiles_calib/output
      - concat_logfiles_blsmooth/output
      - concat_logfiles_apply/output
      - concat_logfiles_apply_pa/output
      - concat_logfiles_apply_fr/output
      - concat_logfiles_beam/output
    type: 'File[]'
    linkMerge: merge_flattened
    'sbg:x': 1000
    'sbg:y': 200
  - id: outh5parm_logfile
    outputSource:
      - h5parm_collector/log
    type: 'File[]'
    'sbg:x': 1000
    'sbg:y': 300
steps:
  - id: bandpass
    in:
      - id: ampRange
        source:
          - ampRange
      - id: skipInternational
        source: skipInternational
      - id: input_h5parm
        source: h5parm
      - id: max2interpolate
        source: max2interpolate
      - id: bandpass_freqresolution
        source: bandpass_freqresolution
      - id: avg_freqresolution
        source: avg_freqresolution
    out:
      - id: output_h5parm
      - id: logfiles
    run: ../../subworkflow/bandpass.cwl
    label: bandpass
    'sbg:x': -500
    'sbg:y': 0
  - id: losoto_plot_A1
    in:
      - id: input_h5parm
        source: bandpass/output_h5parm
      - id: soltab
        default: sol000/amplitudeOrig000
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: plotFlag
        default: true
      - id: prefix
        default: ampBFlag_
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../lofar-cwl/steps/LoSoTo.Plot.cwl
    label: losoto_plot_A1
    'sbg:x': -200
    'sbg:y': -200
  - id: losoto_plot_A2
    in:
      - id: input_h5parm
        source: bandpass/output_h5parm
      - id: soltab
        default: sol000/amplitudeOrig001
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: plotFlag
        default: true
      - id: prefix
        default: ampAFlag_
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../lofar-cwl/steps/LoSoTo.Plot.cwl
    label: losoto_plot_A2
    'sbg:x': -200
    'sbg:y': -100
  - id: losoto_plot_B1
    in:
      - id: input_h5parm
        source: bandpass/output_h5parm
      - id: soltab
        default: sol000/bandpass
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: plotFlag
        default: true
      - id: prefix
        default: bandpass_
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../lofar-cwl/steps/LoSoTo.Plot.cwl
    label: losoto_plot_B1
    'sbg:x': -200
    'sbg:y': 0
  - id: losoto_plot_B2
    in:
      - id: input_h5parm
        source: bandpass/output_h5parm
      - id: soltab
        default: sol000/bandpass
      - id: axesInPlot
        default:
          - freq
      - id: axisInTable
        default: ant
      - id: axisInCol
        default: pol
      - id: time.minmaxstep
        default:
          - 0
          - 100000000000000000000
          - 500000
      - id: plotFlag
        default: true
      - id: prefix
        default: bandpass_
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../lofar-cwl/steps/LoSoTo.Plot.cwl
    label: losoto_plot_B2
    'sbg:x': -200
    'sbg:y': 100
  - id: losoto_plot_B3
    in:
      - id: input_h5parm
        source: bandpass/output_h5parm
      - id: soltab
        default: sol000/bandpass
      - id: axesInPlot
        default:
          - freq
      - id: axisInCol
        default: ant
      - id: time.minmaxstep
        default:
          - 0
          - 100000000000000000000
          - 500000
      - id: plotFlag
        default: true
      - id: prefix
        default: bandpass_
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../lofar-cwl/steps/LoSoTo.Plot.cwl
    label: losoto_plot_B3
    'sbg:x': -200
    'sbg:y': 200
  - id: losoto_plot_B1_trans
    in:
      - id: input_h5parm
        source: bandpass/output_h5parm
      - id: soltab
        default: sol000/bandpass
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: plotFlag
        default: true
      - id: prefix
        default: bandpass_transfer_
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../lofar-cwl/steps/LoSoTo.Plot.cwl
    label: losoto_plot_B1_trans
    'sbg:x': -200
    'sbg:y': 300
  - id: losoto_plot_B2_trans
    in:
      - id: input_h5parm
        source: bandpass/output_h5parm
      - id: soltab
        default: sol000/bandpass
      - id: axesInPlot
        default:
          - freq
      - id: axisInTable
        default: ant
      - id: axisInCol
        default: pol
      - id: time.minmaxstep
        default:
          - 0
          - 100000000000000000000
          - 500000
      - id: plotFlag
        default: true
      - id: prefix
        default: bandpass_transfer_
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../lofar-cwl/steps/LoSoTo.Plot.cwl
    label: losoto_plot_B2_trans
    'sbg:x': -200
    'sbg:y': 400
  - id: losoto_plot_B3_trans
    in:
      - id: input_h5parm
        source: bandpass/output_h5parm
      - id: soltab
        default: sol000/bandpass
      - id: axesInPlot
        default:
          - freq
      - id: axisInCol
        default: ant
      - id: time.minmaxstep
        default:
          - 0
          - 100000000000000000000
          - 500000
      - id: plotFlag
        default: true
      - id: prefix
        default: bbandpass_transfer_
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../lofar-cwl/steps/LoSoTo.Plot.cwl
    label: losoto_plot_B3_trans
    'sbg:x': -200
    'sbg:y': 500
  - id: concat_logfiles_bp
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - inh5parm_logfile
          - bandpass/logfiles
          - losoto_plot_A1/logfile
          - losoto_plot_A2/logfile
          - losoto_plot_B1/logfile
          - losoto_plot_B2/logfile
          - losoto_plot_B3/logfile
          - write_solutions/log
          - h5parm_pointingname/log
          - transfer_solutions/log
      - id: file_prefix
        default: losoto_bandpass
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_bp
    'sbg:x': 500
    'sbg:y': 500
  - id: write_solutions
    in:
      - id: h5parmFile
        source: bandpass/output_h5parm
      - id: outsolset
        default: calibrator
      - id: insoltab
        default: bandpass
      - id: input_file
        source: insolutions
      - id: squeeze
        default: true
      - id: verbose
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/h5parmcat.cwl
    label: write_solutions
    'sbg:x': -200
    'sbg:y': 700
  - id: apply_calibrate_bp
    in:
      - id: msin
        source: msin
      - id: do_smooth
        source: do_smooth
      - id: flagunconverged
        source: flagunconverged
      - id: propagatesolutions
        source: propagatesolutions
      - id: input_h5parm
        source: transfer_solutions/outh5parm
    out:
      - id: apply_fr.log
      - id: BLsmooth.log
      - id: apply_cal.log
      - id: calib_cal.log
      - id: outh5parm
      - id: apply_pa.log
      - id: applybeam.log
    run: ../../subworkflow/apply_calibrate_bp.cwl
    label: apply_calibrate_bp
    scatter:
      - msin
    'sbg:x': 500
    'sbg:y': 0
  - id: concat_logfiles_blsmooth
    in:
      - id: file_list
        source:
          - apply_calibrate_bp/BLsmooth.log
      - id: file_prefix
        default: blsmooth_bandpass
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_blsmooth
    'sbg:x': 750
    'sbg:y': 300
  - id: concat_logfiles_apply_pa
    in:
      - id: file_list
        source:
          - apply_calibrate_bp/apply_pa.log
      - id: file_prefix
        default: apply_pa_bandpass
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_apply_pa
    'sbg:x': 750
    'sbg:y': 400
  - id: concat_logfiles_apply_fr
    in:
      - id: file_list
        source:
          - apply_calibrate_bp/apply_fr.log
      - id: file_prefix
        default: apply_fr_bandpass
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_apply_fr
    'sbg:x': 750
    'sbg:y': 500
  - id: concat_logfiles_apply
    in:
      - id: file_list
        source:
          - apply_calibrate_bp/apply_cal.log
      - id: file_prefix
        default: apply_cal_bandpass
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_apply
    'sbg:x': 750
    'sbg:y': 600
  - id: concat_logfiles_beam
    in:
      - id: file_list
        source:
          - apply_calibrate_bp/applybeam.log
      - id: file_prefix
        default: applybeam_bandpass
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_beam
    'sbg:x': 750
    'sbg:y': 700
  - id: concat_logfiles_calib
    in:
      - id: file_list
        source:
          - apply_calibrate_bp/calib_cal.log
      - id: file_prefix
        default: calib_cal_bandpass
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_calib
    'sbg:x': 750
    'sbg:y': 800
  - id: h5parm_collector
    in:
      - id: h5parmFiles
        source:
          - apply_calibrate_bp/outh5parm
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: clobber
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../../lofar-cwl/steps/H5ParmCollector.cwl
    label: H5parm_collector
    'sbg:x': 750
    'sbg:y': -300
  - id: h5parm_pointingname
    in:
      - id: h5parmFile
        source: write_solutions/outh5parm
      - id: solsetName
        default: calibrator
      - id: pointing
        source: calibrator_name
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/h5parm_pointingname.cwl
    label: h5parm_pointingname
    'sbg:x': 0
    'sbg:y': 300
  - id: transfer_solutions
    in:
      - id: h5parm
        source: h5parm_pointingname/outh5parm
      - id: refh5parm
        source: solutions2transfer
      - id: insolset
        default: calibrator
      - id: outsolset
        default: calibrator
      - id: insoltab
        default: bandpass
      - id: outsoltab
        default: bandpass
      - id: antenna
        source: antennas2transfer
      - id: do_transfer
        source: do_transfer
      - id: trusted
        source: trusted_sources
      - id: max_separation_arcmin
        source: max_separation_arcmin
      - id: parset
        source: concat_parset/output
    out:
      - id: outh5parm
      - id: log
      - id: plots
    run: ../../steps/transfer_solutions.cwl
    label: transfer_solutions
    'sbg:x': 200
    'sbg:y': 300
  - id: concat_parset
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - losoto_plot_B1_trans/parset
          - losoto_plot_B2_trans/parset
          - losoto_plot_B3_trans/parset
      - id: file_prefix
        default: losoto
      - id: file_suffix
        default: parset
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_parset
    'sbg:x': 0
    'sbg:y': 500
requirements:
  - class: SubworkflowFeatureRequirement
  - class: ScatterFeatureRequirement
  - class: MultipleInputFeatureRequirement
