class: Workflow
cwlVersion: v1.0
id: ion
label: ion
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: calibrator_name
    type: string
    'sbg:x': -1000
    'sbg:y': -200
  - id: clock_smooth
    type: boolean
    'sbg:x': -1000
    'sbg:y': -100
  - id: ion_3rd
    type: boolean
    'sbg:x': -1000
    'sbg:y': 0
  - id: refant
    type: string?
    'sbg:x': -1000
    'sbg:y': 100
  - id: h5parm
    type: File
    'sbg:x': -1000
    'sbg:y': 200
  - id: tables2export
    type: string
    'sbg:x': -1000
    'sbg:y': 300
  - id: inh5parm_logfile
    type: 'File[]'
    'sbg:x': -1000
    'sbg:y': 400
  - id: maxStddev
    type: float
    'sbg:x': -1000
    'sbg:y': 500
  - id: insolutions
    type: File
    'sbg:x': -1000
    'sbg:y': 600
outputs:
  - id: inspection
    outputSource:
      - losoto_plot_P3/output_plots
      - losoto_plot_Pd/output_plots
      - losoto_plot_tec/output_plots
      - losoto_plot_tec3rd/output_plots
      - losoto_plot_clock/output_plots
      - losoto_plot_Pr/output_plots
    type: 'File[]?'
    linkMerge: merge_flattened
    'sbg:x': 1000
    'sbg:y': 0
  - id: outsolutions
    outputSource:
      - h5parm_pointingname/outh5parm
    type: File
    'sbg:x': 1000
    'sbg:y': 100
  - id: logfiles
    outputSource:
      - concat_logfiles_ion/output
    type: 'File[]'
    linkMerge: merge_flattened
    'sbg:x': 1000
    'sbg:y': 200
steps:
  - id: clocktec
    in:
      - id: input_h5parm
        source: h5parm
      - id: maxStddev
        source: maxStddev
      - id: fit3rdorder
        source: ion_3rd
      - id: clock_smooth
        source: clock_smooth
    out:
      - id: output_h5parm
      - id: logfiles
      - id: parset
    run: ../../subworkflow/clocktec.cwl
    label: clocktec
    'sbg:x': -500
    'sbg:y': 0
  - id: losoto_plot_P3
    in:
      - id: input_h5parm
        source: clocktec/output_h5parm
      - id: soltab
        default: sol000/phaseOrig
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        default: ion_ph
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../lofar-cwl/steps/LoSoTo.Plot.cwl
    label: losoto_plot_P3
    'sbg:x': 0
    'sbg:y': -200
  - id: losoto_plot_Pd
    in:
      - id: input_h5parm
        source: clocktec/output_h5parm
      - id: soltab
        default: sol000/phaseOrig
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: axisDiff
        default: pol
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        default: ion_ph_poldif
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../lofar-cwl/steps/LoSoTo.Plot.cwl
    label: losoto_plot_Pd
    'sbg:x': 0
    'sbg:y': -100
  - id: losoto_plot_tec
    in:
      - id: input_h5parm
        source: clocktec/output_h5parm
      - id: soltab
        default: sol000/tec
      - id: axesInPlot
        default:
          - time
      - id: axisInTable
        default: ant
      - id: plotFlag
        default: false
      - id: refAnt
        source: refant
      - id: prefix
        default: tec
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../lofar-cwl/steps/LoSoTo.Plot.cwl
    label: losoto_plot_tec
    'sbg:x': 0
    'sbg:y': 0
  - id: losoto_plot_tec3rd
    in:
      - id: input_h5parm
        source: clocktec/output_h5parm
      - id: soltab
        default: 'sol000/tec3rd'
      - id: axesInPlot
        default:
          - time
      - id: axisInTable
        default: ant
      - id: plotFlag
        default: false
      - id: refAnt
        source: refant
      - id: prefix
        default: tec3rd
      - id: execute
        source: ion_3rd
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../lofar-cwl/steps/LoSoTo.Plot.cwl
    label: losoto_plot_tec3rd
    'sbg:x': 0
    'sbg:y': 100
  - id: losoto_plot_clock
    in:
      - id: input_h5parm
        source: clocktec/output_h5parm
      - id: soltab
        default: sol000/clockOrig
      - id: axesInPlot
        default:
          - time
      - id: axisInTable
        default: ant
      - id: plotFlag
        default: false
      - id: refAnt
        source: refant
      - id: prefix
        default: clock
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../lofar-cwl/steps/LoSoTo.Plot.cwl
    label: losoto_plot_clock
    'sbg:x': 0
    'sbg:y': 200
  - id: losoto_plot_Pr
    in:
      - id: input_h5parm
        source: clocktec/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: axisDiff
        default: pol
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        default: ion_ph-res
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../lofar-cwl/steps/LoSoTo.Plot.cwl
    label: losoto_plot_Pr
    'sbg:x': 0
    'sbg:y': 300
  - id: concat_logfiles_ion
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - inh5parm_logfile
          - clocktec/logfiles
          - losoto_plot_P3/logfile
          - losoto_plot_Pd/logfile
          - losoto_plot_tec/logfile
          - losoto_plot_tec3rd/logfile
          - losoto_plot_clock/logfile
          - losoto_plot_Pr/logfile
          - write_solutions/log
          - h5parm_pointingname/log
      - id: file_prefix
        default: losoto_ion
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_ion
    'sbg:x': 500
    'sbg:y': 500
  - id: write_solutions
    in:
      - id: h5parmFile
        source: clocktec/output_h5parm
      - id: outsolset
        default: calibrator
      - id: insoltab
        source: tables2export
      - id: input_file
        source: insolutions
      - id: squeeze
        default: true
      - id: verbose
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/h5parmcat.cwl
    label: write_solutions
    'sbg:x': -200
    'sbg:y': 700
  - id: h5parm_pointingname
    in:
      - id: h5parmFile
        source: write_solutions/outh5parm
      - id: solsetName
        default: calibrator
      - id: pointing
        source: calibrator_name
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/h5parm_pointingname.cwl
    label: h5parm_pointingname
    'sbg:x': 0
    'sbg:y': 700
requirements:
  - class: SubworkflowFeatureRequirement
  - class: MultipleInputFeatureRequirement
