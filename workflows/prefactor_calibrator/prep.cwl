class: Workflow
cwlVersion: v1.0
id: prep
label: prep
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: msin
    type: 'Directory[]'
    'sbg:x': -515.1839599609375
    'sbg:y': 330.674560546875
  - id: filter_baselines
    type: string
    'sbg:x': 254.234375
    'sbg:y': 684
  - id: raw_data
    type: boolean
    'sbg:x': 0
    'sbg:y': 563
  - id: propagatesolutions
    type: boolean
    'sbg:x': 0
    'sbg:y': 670
  - id: flagunconverged
    type: boolean
    'sbg:x': 559.319091796875
    'sbg:y': 837.5
  - id: demix
    type: boolean
    'sbg:x': 254.234375
    'sbg:y': 1326
  - id: max_dppp_threads
    type: int?
    default: 10
    'sbg:x': 254.234375
    'sbg:y': 214
  - id: memoryperc
    type: int
    'sbg:x': 254.234375
    'sbg:y': 107
  - id: flag_baselines
    type: 'string[]'
    'sbg:x': 254.234375
    'sbg:y': 442
  - id: avg_timeresolution
    type: float
    'sbg:x': 254.234375
    'sbg:y': 1554
  - id: avg_freqresolution
    type: string
    'sbg:x': 254.234375
    'sbg:y': 1661
  - id: process_baselines_cal
    type: string
    'sbg:x': 0
    'sbg:y': 777
  - id: demix_timestep
    type: int
    'sbg:x': 254.234375
    'sbg:y': 898
  - id: demix_freqstep
    type: int
    'sbg:x': 254.234375
    'sbg:y': 1219
  - id: demix_target
    type: string
    'sbg:x': 254.234375
    'sbg:y': 1005
  - id: demix_sources
    type: 'string[]'
    'sbg:x': 254.234375
    'sbg:y': 1112
  - id: min_length
    type: int?
    'sbg:x': 360
    'sbg:y': 1919.42626953125
  - id: overhead
    type: float?
    'sbg:x': 180
    'sbg:y': 1903.963134765625
  - id: min_separation
    type: int?
    'sbg:x': 0
    'sbg:y': 884
  - id: do_smooth
    type: boolean?
    default: false
    'sbg:x': 559.319091796875
    'sbg:y': 944.5
  - id: max_separation_arcmin
    type: float?
    'sbg:x': 0
    'sbg:y': 991
  - id: calibrator_path_skymodel
    type: Directory?
    'sbg:x': 0
    'sbg:y': 1098
  - id: A-Team_skymodel
    type: File?
    'sbg:x': 0
    'sbg:y': 1205
  - id: elevation
    type: string
    default: 0deg..15deg
    'sbg:x': 254.234375
    'sbg:y': 791
  - id: amplmin
    type: float
    default: 1.e-30
    'sbg:x': 254.234375
    'sbg:y': 1768
outputs:
  - id: outh5parm
    outputSource:
      - h5parm_collector/outh5parm
    type: File
    'sbg:x': 1554.5106201171875
    'sbg:y': 884
  - id: check_Ateam_separation.png
    outputSource:
      - check_ateam_separation/output_imag
    type: File
    'sbg:x': 559.319091796875
    'sbg:y': 1051.5
  - id: msout
    outputSource:
      - predict_calibrate/msout
    type: 'Directory[]'
    'sbg:x': 1329.937255859375
    'sbg:y': 663
  - id: parset
    outputSource:
      - ndppp_prep_cal/parset
    type: 'File[]'
    'sbg:x': 1027.490966796875
    'sbg:y': 884
  - id: calibrator_name
    outputSource:
      - find_skymodel_cal/model_name
    type: string
    'sbg:x': 559.319091796875
    'sbg:y': 1158.5
  - id: outh5parm_logfile
    outputSource:
      - h5parm_collector/log
    type: 'File[]'
    'sbg:x': 1554.5106201171875
    'sbg:y': 777
  - id: logfiles
    outputSource:
      - concat_logfiles_calib/output
      - ms_concat/logfile
      - concat_logfiles_predict/output
      - concat_logfiles_blsmooth/output
      - make_sourcedb/log
      - find_skymodel_cal/logfile
      - concat_logfiles_prep_cal/output
      - make_sourcedb_ateam/log
      - check_ateam_separation/logfile
      - aoflag/logfile
    type: 'File[]'
    linkMerge: merge_flattened
    'sbg:x': 1554.5106201171875
    'sbg:y': 991
steps:
  - id: select
    in:
      - id: input
        source:
          - msin
    out:
      - id: output
    run: ../../steps/selectfirstdirectory.cwl
    label: select_only_first
    'sbg:x': -303.7861022949219
    'sbg:y': 542.37646484375
  - id: ndppp_prep_cal
    in:
      - id: baselines_to_flag
        default: []
        source:
          - flag_baselines
      - id: elevation_to_flag
        source: elevation
      - id: min_amplitude_to_flag
        source: amplmin
      - id: memoryperc
        default: 20
        source: memoryperc
      - id: raw_data
        default: false
        source: raw_data
      - id: demix
        default: false
        source: demix
      - id: msin
        linkMerge: merge_flattened
        source:
          - msin
      - id: msin_baseline
        default: '*'
        source: filter_baselines
      - id: skymodel
        source: make_sourcedb_ateam/sourcedb
      - id: timeresolution
        default: 1
        source: avg_timeresolution
      - id: freqresolution
        default: 12.21kHz
        source: avg_freqresolution
      - id: demix_timestep
        default: 1
        source: demix_timestep
      - id: demix_freqstep
        default: 1
        source: demix_freqstep
      - id: demix_baseline
        default: '*'
        source: process_baselines_cal
      - id: target_source
        source: demix_target
      - id: ntimechunk
        default: 10
        source: max_dppp_threads
      - id: subtract_sources
        source:
          - demix_sources
    out:
      - id: msout
      - id: logfile
      - id: parset
    run: ../../subworkflow/ndppp_prep_cal.cwl
    label: ndppp_prep_cal
    scatter:
      - msin
    'sbg:x': 559.319091796875
    'sbg:y': 497.5
  - id: predict_calibrate
    in:
      - id: msin
        linkMerge: merge_flattened
        source:
          - aoflag/output_ms
      - id: do_smooth
        source: do_smooth
      - id: sourcedb
        source: make_sourcedb/sourcedb
      - id: flagunconverged
        source: flagunconverged
      - id: propagatesolutions
        source: propagatesolutions
    out:
      - id: msout
      - id: BLsmooth.log
      - id: predict_cal.log
      - id: calib_cal.log
      - id: outh5parm
    run: ../../subworkflow/predict_calibrate.cwl
    label: predict_calibrate
    scatter:
      - msin
    'sbg:x': 1027.490966796875
    'sbg:y': 749
  - id: ms_concat
    in:
      - id: min_length
        source: min_length
      - id: overhead
        source: overhead
      - id: msin
        linkMerge: merge_flattened
        source:
          - ndppp_prep_cal/msout
    out:
      - id: concat_meta_ms
      - id: concat_additional_ms
      - id: ms_outs
      - id: logfile
    run: ../../lofar-cwl/steps/ms_concat.cwl
    label: ms_concat
    'sbg:x': 700
    'sbg:y': 749
  - id: aoflag
    in:
      - id: msin
        linkMerge: merge_flattened
        source:
          - ndppp_prep_cal/msout
      - id: concat_meta_ms
        source: ms_concat/concat_meta_ms
      - id: concat_additional_ms
        source: ms_concat/concat_additional_ms
      - id: verbose
        default: true
      - id: combine-spws
        default: true
    out:
      - id: output_ms
      - id: logfile
    run: ../../steps/aoflag.cwl
    label: aoflag
    'sbg:x': 850
    'sbg:y': 749
  - id: check_ateam_separation
    in:
      - id: ms
        source:
          - select/output
      - id: min_separation
        source: min_separation
    out:
      - id: output_imag
      - id: logfile
    run: ../../steps/check_ateam_separation.cwl
    label: check_Ateam_separation
    'sbg:x': 254.234375
    'sbg:y': 1440
  - id: make_sourcedb_ateam
    in:
      - id: sky_model
        source: A-Team_skymodel
      - id: logname
        default: make_sourcedb_ateam.log
    out:
      - id: sourcedb
      - id: log
    run: ../../lofar-cwl/steps/makesourcedb.cwl
    label: make_sourcedb_ateam
    'sbg:x': 254.234375
    'sbg:y': 328
  - id: find_skymodel_cal
    in:
      - id: msin
        source:
          - select/output
      - id: skymodels
        source: calibrator_path_skymodel
      - id: max_separation_arcmin
        source: max_separation_arcmin
    out:
      - id: output_models
      - id: model_name
      - id: logfile
    run: ../../steps/find_skymodel_cal.cwl
    label: find_skymodel_cal
    'sbg:x': 254.234375
    'sbg:y': 563
  - id: make_sourcedb
    in:
      - id: sky_model
        source: find_skymodel_cal/output_models
      - id: output_file_name
        default: calibrator.sourcedb
    out:
      - id: sourcedb
      - id: log
    run: ../../lofar-cwl/steps/makesourcedb.cwl
    label: make_sourcedb
    'sbg:x': 559.319091796875
    'sbg:y': 723.5
  - id: concat_logfiles_prep_cal
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - ndppp_prep_cal/logfile
      - id: file_prefix
        default: ndppp_prep_cal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_prep_cal
    'sbg:x': 1027.490966796875
    'sbg:y': 991
  - id: concat_logfiles_blsmooth
    in:
      - id: file_list
        source:
          - predict_calibrate/BLsmooth.log
      - id: file_prefix
        default: blsmooth_prep
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_blsmooth
    'sbg:x': 1329.937255859375
    'sbg:y': 1105
  - id: concat_logfiles_predict
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - predict_calibrate/predict_cal.log
      - id: file_prefix
        default: predict_cal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_predict
    'sbg:x': 1329.937255859375
    'sbg:y': 891
  - id: concat_logfiles_calib
    in:
      - id: file_list
        source:
          - predict_calibrate/calib_cal.log
      - id: file_prefix
        default: calib_cal_prep
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_calib
    'sbg:x': 1329.937255859375
    'sbg:y': 998
  - id: h5parm_collector
    in:
      - id: h5parmFiles
        source:
          - predict_calibrate/outh5parm
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: clobber
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../../lofar-cwl/steps/H5ParmCollector.cwl
    label: H5parm_collector
    'sbg:x': 1329.937255859375
    'sbg:y': 777
requirements:
  - class: InlineJavascriptRequirement
  - class: StepInputExpressionRequirement
  - class: SubworkflowFeatureRequirement
  - class: ScatterFeatureRequirement
