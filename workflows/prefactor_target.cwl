class: Workflow
cwlVersion: v1.0
id: prefactor_target
label: prefactor_target
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: msin
    type: 'Directory[]'
    'sbg:x': -1000
    'sbg:y': -1700
  - id: cal_solutions
    type: File
    'sbg:x': -1000
    'sbg:y': -1600
  - id: refant
    type: string?
    default: CS00.*
    'sbg:x': -1000
    'sbg:y': -1500
  - id: flag_baselines
    type: 'string[]?'
    default: []
    'sbg:x': -1000
    'sbg:y': -1400
  - id: process_baselines_target
    type: string?
    default: '[CR]S*&'
    'sbg:x': -1000
    'sbg:y': -1300
  - id: filter_baselines
    type: string?
    default: '[CR]S*&'
    'sbg:x': -1000
    'sbg:y': -1200
  - id: do_smooth
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': -1100
  - id: rfistrategy
    type: string?
    default: HBAdefault.rfis
    'sbg:x': -1000
    'sbg:y': -1000
  - id: min_unflagged_fraction
    type: float?
    default: 0.5
    'sbg:x': -1000
    'sbg:y': -900
  - id: compression_bitrate
    type: int?
    default: 16
    'sbg:x': -1000
    'sbg:y': -800
  - id: raw_data
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': -700
  - id: propagatesolutions
    type: boolean?
    default: true
    'sbg:x': -1000
    'sbg:y': -600
  - id: demix_sources
    type: 'string[]?'
    default:
      - CasA
      - CygA
    'sbg:x': -1000
    'sbg:y': -500
  - id: demix_target
    type: string?
    default: ''
    'sbg:x': -1000
    'sbg:y': -400
  - id: demix_freqstep
    type: int?
    default: 16
    'sbg:x': -1000
    'sbg:y': -300
  - id: demix_timestep
    type: int?
    default: 10
    'sbg:x': -1000
    'sbg:y': -200
  - id: demix
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': -100
  - id: apply_tec
    type: boolean
    default: false
    'sbg:x': -1000
    'sbg:y': 0
  - id: apply_clock
    type: boolean
    default: true
    'sbg:x': -1000
    'sbg:y': 100
  - id: apply_phase
    type: boolean
    default: false
    'sbg:x': -1000
    'sbg:y': 200
  - id: apply_RM
    type: boolean
    default: true
    'sbg:x': -1000
    'sbg:y': 300
  - id: apply_beam
    type: boolean
    default: true
    'sbg:x': -1000
    'sbg:y': 400
  - id: clipATeam
    type: boolean?
    default: true
    'sbg:x': -1000
    'sbg:y': 500
  - id: gsmcal_step
    type: string?
    default: phase
    'sbg:x': -1000
    'sbg:y': 600
  - id: updateweights
    type: boolean?
    default: true
    'sbg:x': -1000
    'sbg:y': 700
  - id: max_dppp_threads
    type: int?
    default: 10
    'sbg:x': -1000
    'sbg:y': 800
  - id: memoryperc
    type: int?
    default: 20
    'sbg:x': -1000
    'sbg:y': 900
  - id: min_length
    type: int?
    default: 5
    'sbg:x': -1000
    'sbg:y': 1000
  - id: overhead
    type: float?
    default: 0.7
    'sbg:x': -1000
    'sbg:y': 1100
  - id: min_separation
    type: int?
    default: 30
    'sbg:x': -1000
    'sbg:y': 1200
  - id: A-Team_skymodel
    type: File?
    'sbg:x': -1000
    'sbg:y': 1300
  - id: target_skymodel
    type: File?
    'sbg:x': -1000
    'sbg:y': 1400
  - id: use_target
    type: boolean?
    default: true
    'sbg:x': -1000
    'sbg:y': 1500
  - id: skymodel_source
    type: string?
    default: TGSS
    'sbg:x': -1000
    'sbg:y': 1600
  - id: avg_timeresolution
    type: float?
    default: 4
    'sbg:x': -1000
    'sbg:y': 1700
  - id: avg_freqresolution
    type: string?
    default: 48.82kHz
    'sbg:x': -1000
    'sbg:y': 1800
  - id: avg_timeresolution_concat
    type: int?
    default: 8
    'sbg:x': -1000
    'sbg:y': 1900
  - id: avg_freqresolution_concat
    type: string?
    default: 97.64kHz
    'sbg:x': -1000
    'sbg:y': 2000
  - id: num_SBs_per_group
    type: int?
    default: 10
    'sbg:x': -1000
    'sbg:y': 2100
  - id: reference_stationSB
    type: int?
    default: null
    'sbg:x': -1000
    'sbg:y': 2200
  - id: ionex_server
    type: string?
    default: 'ftp://ftp.aiub.unibe.ch/CODE/'
    'sbg:x': -1000
    'sbg:y': 2300
  - id: ionex_prefix
    type: string?
    default: CODG
    'sbg:x': -1000
    'sbg:y': 2400
  - id: proxy_server
    type: string?
    default: null
    'sbg:x': -1000
    'sbg:y': 2500
  - id: proxy_port
    type: string?
    default: null
    'sbg:x': -1000
    'sbg:y': 2600
  - id: proxy_type
    type: string?
    default: null
    'sbg:x': -1000
    'sbg:y': 2700
  - id: proxy_user
    type: string?
    default: null
    'sbg:x': -1000
    'sbg:y': 2800
  - id: proxy_pass
    type: string?
    default: null
    'sbg:x': -1000
    'sbg:y': 2900
outputs:
  - id: inspection
    outputSource:
      - prep/inspection
      - gsmcal/inspection
      - finalize/inspection
    type: 'File[]'
    linkMerge: merge_flattened
    'sbg:x': 2000
    'sbg:y': -600
  - id: solutions
    outputSource:
      - finalize/solutions
    type: File
    'sbg:x': 2000
    'sbg:y': -500
  - id: msout
    outputSource:
      - finalize/msout
    type: 'Directory[]'
    'sbg:x': 2000
    'sbg:y': 0
  - id: logfiles
    outputSource:
      - prep/logfiles
      - concat_logfiles_RefAnt/output
      - gsmcal/logfiles
      - finalize/logfiles
    type: 'File[]'
    linkMerge: merge_flattened
    'sbg:x': 2000
    'sbg:y': 600
steps:
  - id: prep
    in:
      - id: msin
        source:
          - msin
      - id: cal_solutions
        source: cal_solutions
      - id: flag_baselines
        source:
          - flag_baselines
      - id: process_baselines_target
        source: process_baselines_target
      - id: filter_baselines
        source: filter_baselines
      - id: raw_data
        source: raw_data
      - id: propagatesolutions
        source: propagatesolutions
      - id: demix_sources
        source:
          - demix_sources
      - id: demix_target
        source: demix_target
      - id: demix_freqstep
        source: demix_freqstep
      - id: demix_timestep
        source: demix_timestep
      - id: demix
        source: demix
      - id: apply_tec
        source: apply_tec
      - id: apply_clock
        source: apply_clock
      - id: apply_phase
        source: apply_phase
      - id: apply_RM
        source: apply_RM
      - id: apply_beam
        source: apply_beam
      - id: clipATeam
        source: clipATeam
      - id: updateweights
        source: updateweights
      - id: max_dppp_threads
        source: max_dppp_threads
      - id: memoryperc
        source: memoryperc
      - id: min_separation
        source: min_separation
      - id: A-Team_skymodel
        source: A-Team_skymodel
      - id: avg_timeresolution
        source: avg_timeresolution
      - id: avg_freqresolution
        source: avg_freqresolution
      - id: ionex_server
        source: ionex_server
      - id: ionex_prefix
        source: ionex_prefix
      - id: proxy_server
        source: proxy_server
      - id: proxy_port
        source: proxy_port
      - id: proxy_type
        source: proxy_type
      - id: proxy_user
        source: proxy_user
      - id: proxy_pass
        source: proxy_pass
    out:
      - id: outh5parm
      - id: inspection
      - id: msout
      - id: logfiles
    run: ./prefactor_target/prep.cwl
    label: prep
    'sbg:x': 0
    'sbg:y': 0
  - id: findRefAnt
    in:
      - id: msin
        source: prep/msout
    out:
      - id: flagged_fraction_dict
      - id: logfile
    run: ./../steps/findRefAnt.cwl
    label: findRefAnt
    scatter:
      - msin
    'sbg:x': 400
    'sbg:y': 0
  - id: findRefAnt_join
    in:
      - id: flagged_fraction_dict
        source:
          - findRefAnt/flagged_fraction_dict
      - id: filter_station
        source: refant
    out:
      - id: refant
      - id: logfile
    run: ./../steps/findRefAnt_join.cwl
    label: findRefAnt_join
    'sbg:x': 600
    'sbg:y': 0
  - id: concat_logfiles_RefAnt
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - findRefAnt/logfile
          - findRefAnt_join/logfile
      - id: file_prefix
        default: findRefAnt
    out:
      - id: output
    run: ./../steps/concatenate_files.cwl
    label: concat_logfiles_RefAnt
    'sbg:x': 800
    'sbg:y': 300
  - id: gsmcal
    in:
      - id: msin
        source:
          - prep/msout
      - id: filter_baselines
        source: process_baselines_target
      - id: num_SBs_per_group
        source: num_SBs_per_group
      - id: reference_stationSB
        source: reference_stationSB
      - id: use_target
        source: use_target
      - id: target_skymodel
        source: target_skymodel
      - id: skymodel_source
        source: skymodel_source
      - id: do_smooth
        source: do_smooth
      - id: propagatesolutions
        source: propagatesolutions
      - id: avg_timeresolution_concat
        source: avg_timeresolution_concat
      - id: avg_freqresolution_concat
        source: avg_freqresolution_concat
      - id: min_unflagged_fraction
        source: min_unflagged_fraction
      - id: refant
        source: findRefAnt_join/refant
      - id: overhead
        source: overhead
      - id: min_length
        source: min_length
    out:
      - id: msout
      - id: outh5parm
      - id: bad_antennas
      - id: total_bandwidth
      - id: outh5parm_logfile
      - id: inspection
      - id: logfiles
    run: ./prefactor_target/gsmcal.cwl
    label: gsmcal
    'sbg:x': 900
    'sbg:y': 0
  - id: finalize
    in:
      - id: msin
        source:
          - gsmcal/msout
      - id: input_h5parm
        source: gsmcal/outh5parm
      - id: inh5parm_logfile
        source: gsmcal/outh5parm_logfile
      - id: gsmcal_step
        source: gsmcal_step
      - id: process_baselines_target
        source: process_baselines_target
      - id: bad_antennas
        source: gsmcal/bad_antennas
      - id: insolutions
        source: prep/outh5parm
      - id: compression_bitrate
        source: compression_bitrate
      - id: skymodel_source
        source: skymodel_source
      - id: total_bandwidth
        source: gsmcal/total_bandwidth
    out:
      - id: msout
      - id: solutions
      - id: logfiles
      - id: inspection
    run: ./prefactor_target/finalize.cwl
    label: finalize
    'sbg:x': 1200
    'sbg:y': 0
requirements:
  - class: SubworkflowFeatureRequirement
  - class: ScatterFeatureRequirement
  - class: MultipleInputFeatureRequirement
