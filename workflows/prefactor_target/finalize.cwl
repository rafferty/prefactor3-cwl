class: Workflow
cwlVersion: v1.0
id: finalize
label: finalize
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: msin
    type: 'Directory[]'
    'sbg:x': -1000
    'sbg:y': -400
  - id: input_h5parm
    type: File
    'sbg:x': -1000
    'sbg:y': -300
  - id: inh5parm_logfile
    type: File
    'sbg:x': -1000
    'sbg:y': -200
  - id: gsmcal_step
    type: string
    'sbg:x': -1000
    'sbg:y': -100
  - id: process_baselines_target
    type: string
    'sbg:x': -1000
    'sbg:y': 0
  - id: bad_antennas
    type: string
    'sbg:x': -1000
    'sbg:y': 100
  - id: insolutions
    type: File
    'sbg:x': -1000
    'sbg:y': 200
  - id: compression_bitrate
    type: int
    'sbg:x': -1000
    'sbg:y': 300
  - id: skymodel_source
    type: string
    'sbg:x': -1000
    'sbg:y': 400
  - id: total_bandwidth
    type: int
    'sbg:x': -1000
    'sbg:y': 500
outputs:
  - id: msout
    outputSource:
      - apply_gsmcal/msout
    type: 'Directory[]'
    'sbg:x': 1300
    'sbg:y': 0
  - id: solutions
    outputSource:
      - h5parm_pointingname/outh5parm
    type: File
    'sbg:x': 1300
    'sbg:y': 200
  - id: logfiles
    outputSource:
      - concat_logfiles_applygsm/output
      - concat_logfiles_solutions/output
      - concat_logfiles_structure/output
      - concat_logfiles_wsclean/output
    type: 'File[]'
    linkMerge: merge_flattened
    'sbg:x': 1300
    'sbg:y': 800
  - id: inspection
    outputSource:
      - structure_function/structure_plot
      - wsclean/image
    type: 'File[]?'
    linkMerge: merge_flattened
    'sbg:x': 1300
    'sbg:y': 500
steps:
  - id: add_missing_stations
    in:
      - id: h5parm
        source: input_h5parm
      - id: refh5parm
        source: insolutions
      - id: solset
        default: sol000
      - id: refsolset
        default: target
      - id: soltab_in
        source: gsmcal_step
        valueFrom: $(self+'000')
      - id: soltab_out
        source:
          - skymodel_source
          - gsmcal_step
        valueFrom: $(self.join(''))
      - id: filter
        source: process_baselines_target
      - id: bad_antennas
        source: bad_antennas
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/add_missing_stations.cwl
    label: add_missing_stations
    'sbg:x': -200
    'sbg:y': -300
  - id: apply_gsmcal
    in:
      - id: msin
        source: msin
      - id: msout_name
        linkMerge: merge_flattened
        source:
          - msin
        valueFrom: $(self.nameroot+'_pre-cal.ms')
      - id: msin_datacolumn
        default: DATA
      - id: parmdb
        source: write_solutions/outh5parm
      - id: msout_datacolumn
        default: DATA
      - id: correction
        source:
          - skymodel_source
          - gsmcal_step
        valueFrom: $(self.join(''))
      - id: solset
        default: target
      - id: storagemanager
        default: Dysco
      - id: databitrate
        source: compression_bitrate
    out:
      - id: msout
      - id: logfile
    run: ../../lofar-cwl/steps/applytarget.cwl
    label: apply_gsmcal
    scatter:
      - msin
      - msout_name
    scatterMethod: flat_crossproduct
    'sbg:x': 400
    'sbg:y': 0
  - id: average
    in:
      - id: msin
        source: apply_gsmcal/msout
      - id: msout_name
        linkMerge: merge_flattened
        source:
          - apply_gsmcal/msout
        valueFrom: $(self.nameroot+'_wsclean.ms')
      - id: msin_datacolumn
        default: DATA
      - id: msout_datacolumn
        default: DATA
      - id: overwrite
        default: false
      - id: storagemanager
        default: Dysco
      - id: avg_timestep
        default: 2
      - id: avg_freqstep
        default: 2
    out:
      - id: msout
      - id: logfile
    run: ../../lofar-cwl/steps/average.cwl
    label: average
    scatter:
      - msin
    'sbg:x': 600
    'sbg:y': -200
  - id: wsclean
    in:
      - id: msin
        source:
          - average/msout
      - id: image_scale
        default: 15asec
      - id: niter
        default: 1000000
      - id: nmiter
        default: 5
      - id: multiscale
        default: true
      - id: mgain
        default: 0.8
      - id: ncpu
        default: 4
      - id: parallel-gridding
        default: 1
      - id: parallel-deconvolution
        default: 1500
      - id: parallel-reordering
        default: 4
      - id: channels-out
        source: total_bandwidth
        valueFrom: $(Math.round(self/1000000))
      - id: join-channels
        default: true
      - id: taper-gaussian
        default: 40asec
      - id: weighting
        default: briggs -0.5
      - id: maxuvw-m
        default: 20000
      - id: image_name
        source: get_targetname/targetname
    out:
      - id: dirty_image
      - id: image
      - id: logfile
    run: ../../lofar-cwl/steps/wsclean.cwl
    label: wsclean
    'sbg:x': 800
    'sbg:y': -200
  - id: merge_array_files
    in:
      - id: input
        source:
          - apply_gsmcal/logfile
    out:
      - id: output
    run: ../../steps/merge_array_files.cwl
    label: merge_array_files
    'sbg:x': 600
    'sbg:y': 0
  - id: write_solutions
    in:
      - id: h5parmFile
        source: add_missing_stations/outh5parm
      - id: outsolset
        default: target
      - id: insoltab
        source:
          - skymodel_source
          - gsmcal_step
        valueFrom: $(self.join(''))
      - id: input_file
        source: insolutions
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: history
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/h5parmcat.cwl
    label: write_solutions
    'sbg:x': 200
    'sbg:y': 500
  - id: h5parm_pointingname
    in:
      - id: h5parmFile
        source: write_solutions/outh5parm
      - id: solsetName
        default: target
      - id: pointing
        source: get_targetname/targetname
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/h5parm_pointingname.cwl
    label: h5parm_pointingname
    'sbg:x': 400
    'sbg:y': 200
  - id: structure_function
    in:
      - id: h5parmFile
        source: write_solutions/outh5parm
      - id: solset
        default: target
      - id: soltab
        source:
          - skymodel_source
          - gsmcal_step
        valueFrom: $(self.join(''))
      - id: outbasename
        source: get_targetname/targetname
    out:
      - id: structure_plot
      - id: structure_txt
      - id: log
    run: ../../steps/structure_function.cwl
    label: structure_function
    'sbg:x': 500
    'sbg:y': 300
  - id: concat_logfiles_applygsm
    in:
      - id: file_list
        source:
          - merge_array_files/output
      - id: file_prefix
        default: apply_gsmcal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_applygsm
    'sbg:x': 750
    'sbg:y': 500
  - id: concat_logfiles_solutions
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - inh5parm_logfile
          - add_missing_stations/log
          - write_solutions/log
          - get_targetname/logfile
          - h5parm_pointingname/log
      - id: file_prefix
        default: losoto_gsmcal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_solutions
    'sbg:x': 500
    'sbg:y': 500
  - id: concat_logfiles_structure
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - structure_function/log
          - structure_function/structure_txt
      - id: file_prefix
        source: get_targetname/targetname
        valueFrom: $(self+'_structure')
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_structure
    'sbg:x': 950
    'sbg:y': 500
  - id: concat_logfiles_wsclean
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - wsclean/logfile
      - id: file_prefix
        default: wsclean
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_wsclean
    'sbg:x': 1100
    'sbg:y': 500
  - id: get_targetname
    in:
      - id: msin
        linkMerge: merge_flattened
        source:
          - msin
    out:
      - id: targetname
      - id: logfile
    run: ../../steps/get_targetname.cwl
    label: get_targetname
    'sbg:x': 200
    'sbg:y': 200
requirements:
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement
  - class: ScatterFeatureRequirement
  - class: MultipleInputFeatureRequirement
