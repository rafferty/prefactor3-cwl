class: Workflow
cwlVersion: v1.0
id: gsmcal
label: gsmcal
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: msin
    type: 'Directory[]'
    'sbg:x': -1000
    'sbg:y': -400
  - id: filter_baselines
    type: string
    'sbg:x': -1000
    'sbg:y': -300
  - id: num_SBs_per_group
    type: int
    'sbg:x': -1000
    'sbg:y': -200
  - id: reference_stationSB
    type: int
    default: null
    'sbg:x': -1000
    'sbg:y': -100
  - id: use_target
    type: boolean
    'sbg:x': -1000
    'sbg:y': 0
  - id: target_skymodel
    type: File?
    'sbg:x': -1000
    'sbg:y': 100
  - id: skymodel_source
    type: string
    'sbg:x': -1000
    'sbg:y': 200
  - id: do_smooth
    type: boolean
    'sbg:x': -1000
    'sbg:y': 300
  - id: propagatesolutions
    type: boolean
    'sbg:x': -1000
    'sbg:y': 400
  - id: avg_timeresolution_concat
    type: int
    'sbg:x': -1000
    'sbg:y': 500
  - id: avg_freqresolution_concat
    type: string
    'sbg:x': -1000
    'sbg:y': 600
  - id: min_unflagged_fraction
    type: float
    'sbg:x': -1000
    'sbg:y': 700
  - id: refant
    type: string
    'sbg:x': -1000
    'sbg:y': 800
  - id: min_length
    type: int?
    default: 5
    'sbg:x': -1000
    'sbg:y': 900
  - id: overhead
    type: float?
    default: 0.7
    'sbg:x': -1000
    'sbg:y': 1000
outputs:
  - id: msout
    outputSource:
      - calibrate_target/msout
    type: 'Directory[]'
    'sbg:x': 1900
    'sbg:y': 0
  - id: outh5parm
    outputSource:
      - h5parm_collector/outh5parm
    type: File
    'sbg:x': 1900
    'sbg:y': 200
  - id: bad_antennas
    outputSource:
      - identifybadantennas_join/filter_out
    type: string
    'sbg:x': 1900
    'sbg:y': 400
  - id: outh5parm_logfile
    outputSource:
      - concat_logfiles_losoto/output
    type: File
    'sbg:x': 1900
    'sbg:y': 600
  - id: inspection
    outputSource:
      - losoto_plot_P/output_plots
      - losoto_plot_P2/output_plots
      - losoto_plot_Pd/output_plots
      - losoto_plot_Pd2/output_plots
      - plot_unflagged/output_imag
    type: 'File[]'
    linkMerge: merge_flattened
    'sbg:x': 1900
    'sbg:y': 800
  - id: logfiles
    outputSource:
      - concat_logfiles_identify/output
      - sort_times_into_freqGroups/logfile
      - find_skymodel_target/logfile
      - make_sourcedb_target/log
      - concat_logfiles_calib/output
      - concat_logfiles_dpppconcat/output
      - concat_logfiles_blsmooth/output
      - concat_logfiles_unflagged/output
      - ms_concat/logfile
      - aoflag/logfile
    type: 'File[]'
    linkMerge: merge_flattened
    'sbg:x': 1900
    'sbg:y': 1000
  - id: total_bandwidth
    outputSource:
      - sort_times_into_freqGroups/total_bandwidth
    type: int
    'sbg:x': 1900
    'sbg:y': 1200
steps:
  - id: identifybadantennas
    in:
      - id: msin
        source: msin
    out:
      - id: flaggedants
      - id: logfile
    run: ../../steps/identify_bad_antennas.cwl
    label: identifybadantennas
    scatter:
      - msin
    'sbg:x': -200
    'sbg:y': -300
  - id: identifybadantennas_join
    in:
      - id: flaggedants
        source:
          - identifybadantennas/flaggedants
      - id: filter
        source: filter_baselines
    out:
      - id: filter_out
      - id: logfile
    run: ../../steps/identify_bad_antennas_join.cwl
    label: identifybadantennas_join
    'sbg:x': 0
    'sbg:y': -300
  - id: sort_times_into_freqGroups
    in:
      - id: msin
        source:
          - msin
      - id: numbands
        source: num_SBs_per_group
      - id: NDPPPfill
        default: true
      - id: stepname
        default: .dpppconcat
      - id: firstSB
        source: reference_stationSB
      - id: truncateLastSBs
        default: false
    out:
      - id: filenames
      - id: groupnames
      - id: total_bandwidth
      - id: logfile
    run: ../../steps/sort_times_into_freqGroups.cwl
    label: sorttimesintofreqGroups
    'sbg:x': -200
    'sbg:y': 0
  - id: find_skymodel_target
    in:
      - id: msin
        source:
          - msin
      - id: SkymodelPath
        source: target_skymodel
      - id: Radius
        default: 5
      - id: Source
        source: skymodel_source
      - id: DoDownload
        source: use_target
    out:
      - id: skymodel
      - id: logfile
    run: ../../steps/find_skymodel_target.cwl
    label: find_skymodel_target
    'sbg:x': -200
    'sbg:y': -500
  - id: make_sourcedb_target
    in:
      - id: sky_model
        source: find_skymodel_target/skymodel
      - id: output_file_name
        default: target.sourcedb
      - id: logname
        default: make_sourcedb_target.log
    out:
      - id: sourcedb
      - id: log
    run: ../../lofar-cwl/steps/makesourcedb.cwl
    label: make_sourcedb_target
    'sbg:x': 0
    'sbg:y': -500
  - id: concat_logfiles_dpppconcat
    in:
      - id: file_list
        source:
          - concat/dpppconcat.log
      - id: file_prefix
        default: dpppconcat
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_dpppconcat
    'sbg:x': 1600
    'sbg:y': 100
  - id: concat_logfiles_blsmooth
    in:
      - id: file_list
        source:
          - calibrate_target/BLsmooth.log
      - id: file_prefix
        default: blsmooth_target
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_blsmooth
    'sbg:x': 1600
    'sbg:y': 300
  - id: concat_logfiles_calib
    in:
      - id: file_list
        source:
          - calibrate_target/gaincal.log
      - id: file_prefix
        default: gaincal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_calib
    'sbg:x': 1600
    'sbg:y': 500
  - id: concat_logfiles_losoto
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - h5parm_collector/log
          - losoto_plot_P/logfile
          - losoto_plot_P2/logfile
          - losoto_plot_Pd/logfile
          - losoto_plot_Pd2/logfile
      - id: file_prefix
        default: losoto_gsmcal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_losoto
    'sbg:x': 1600
    'sbg:y': 700
  - id: concat_logfiles_unflagged
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - merge_array_files/output
          - plot_unflagged/logfile
      - id: file_prefix
        default: check_unflagged_fraction
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_unflagged
    'sbg:x': 1600
    'sbg:y': 900
  - id: concat_logfiles_identify
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - identifybadantennas/logfile
          - identifybadantennas_join/logfile
      - id: file_prefix
        default: identifyBadAntennas
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_identify
    'sbg:x': 1600
    'sbg:y': -100
  - id: concat
    in:
      - id: msin
        source:
          - msin
      - id: group_id
        source: sort_times_into_freqGroups/groupnames
      - id: groups_specification
        source: sort_times_into_freqGroups/filenames
      - id: filter_baselines
        source: identifybadantennas_join/filter_out
      - id: avg_timeresolution_concat
        source: avg_timeresolution_concat
      - id: avg_freqresolution_concat
        source: avg_freqresolution_concat
      - id: min_unflagged_fraction
        source: min_unflagged_fraction
    out:
      - id: msout
      - id: dpppconcat.log
    run: ../../subworkflow/concat.cwl
    label: concat
    scatter:
      - group_id
    'sbg:x': 200
    'sbg:y': 0
  - id: ms_concat
    in:
      - id: min_length
        source: min_length
      - id: overhead
        source: overhead
      - id: msin
        source:
          - concat/msout
    out:
      - id: concat_meta_ms
      - id: concat_additional_ms
      - id: ms_outs
      - id: logfile
    run: ../../lofar-cwl/steps/ms_concat.cwl
    label: ms_concat
    'sbg:x': 400
    'sbg:y': 0
  - id: aoflag
    in:
      - id: msin
        source:
          - ms_concat/ms_outs
      - id: concat_meta_ms
        source: ms_concat/concat_meta_ms
      - id: concat_additional_ms
        source: ms_concat/concat_additional_ms
      - id: verbose
        default: true
      - id: combine-spws
        default: true
    out:
      - id: output_ms
      - id: logfile
    run: ../../steps/aoflag.cwl
    label: aoflag
    'sbg:x': 600
    'sbg:y': 0
  - id: check_unflagged_fraction
    in:
      - id: msin
        source: aoflag/output_ms
      - id: min_fraction
        source: min_unflagged_fraction
    out:
      - id: msout
      - id: unflagged_fraction
      - id: logfile
    run: ../../steps/check_unflagged_fraction.cwl
    label: check_unflagged_fraction
    scatter:
      - msin
    'sbg:x': 800
    'sbg:y': 0
  - id: merge_array
    in:
      - id: input
        source:
          - check_unflagged_fraction/msout
    out:
      - id: output
    run: ../../steps/merge_array.cwl
    label: merge_array
    'sbg:x': 1000
    'sbg:y': -100
  - id: merge_array_files
    in:
      - id: input
        source:
          - check_unflagged_fraction/logfile
    out:
      - id: output
    run: ../../steps/merge_array_files.cwl
    label: merge_array_files
    'sbg:x': 1000
    'sbg:y': 100
  - id: calibrate_target
    in:
      - id: msin
        source: merge_array/output
      - id: skymodel
        source: make_sourcedb_target/sourcedb
      - id: do_smooth
        source: do_smooth
      - id: propagatesolutions
        source: propagatesolutions
    out:
      - id: msout
      - id: BLsmooth.log
      - id: gaincal.log
      - id: outh5parm
    run: ../../subworkflow/calib_targ.cwl
    label: calibrate_target
    scatter:
      - msin
    'sbg:x': 1200
    'sbg:y': 0
  - id: h5parm_collector
    in:
      - id: h5parmFiles
        source:
          - calibrate_target/outh5parm
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: clobber
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../../lofar-cwl/steps/H5ParmCollector.cwl
    label: H5parm_collector
    'sbg:x': 1200
    'sbg:y': -300
  - id: plot_unflagged
    in:
      - id: msin
        source:
          - concat/msout
      - id: unflagged_fraction
        source:
          - check_unflagged_fraction/unflagged_fraction
    out:
      - id: output_imag
      - id: logfile
    run: ../../steps/plot_unflagged.cwl
    label: plot_unflagged
    'sbg:x': 1400
    'sbg:y': -400
  - id: losoto_plot_P
    in:
      - id: input_h5parm
        source: h5parm_collector/outh5parm
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        default: ph_
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../lofar-cwl/steps/LoSoTo.Plot.cwl
    label: losoto_plot_P
    'sbg:x': 1400
    'sbg:y': 200
  - id: losoto_plot_P2
    in:
      - id: input_h5parm
        source: h5parm_collector/outh5parm
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
      - id: axisInTable
        default: ant
      - id: axisInCol
        default: pol
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        default: ph_
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../lofar-cwl/steps/LoSoTo.Plot.cwl
    label: losoto_plot_P2
    'sbg:x': 1400
    'sbg:y': 50
  - id: losoto_plot_Pd
    in:
      - id: input_h5parm
        source: h5parm_collector/outh5parm
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: axisDiff
        default: pol
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        default: ph_poldif_
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../lofar-cwl/steps/LoSoTo.Plot.cwl
    label: losoto_plot_Pd
    'sbg:x': 1400
    'sbg:y': -100
  - id: losoto_plot_Pd2
    in:
      - id: input_h5parm
        source: h5parm_collector/outh5parm
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
      - id: axisInTable
        default: ant
      - id: axisDiff
        default: pol
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        default: ph_poldif_
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../lofar-cwl/steps/LoSoTo.Plot.cwl
    label: losoto_plot_Pd2
    'sbg:x': 1400
    'sbg:y': -250
requirements:
  - class: SubworkflowFeatureRequirement
  - class: ScatterFeatureRequirement
  - class: MultipleInputFeatureRequirement
