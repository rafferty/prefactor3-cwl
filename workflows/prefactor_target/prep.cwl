class: Workflow
cwlVersion: v1.0
id: prep
label: prep
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
inputs:
  - id: msin
    type: 'Directory[]'
    'sbg:x': -1000
    'sbg:y': -1700
  - id: cal_solutions
    type: File
    'sbg:x': -1000
    'sbg:y': -1600
  - id: flag_baselines
    type: 'string[]?'
    default: []
    'sbg:x': -1000
    'sbg:y': -1500
  - id: process_baselines_target
    type: string?
    default: '[CR]S*&'
    'sbg:x': -1000
    'sbg:y': -1400
  - id: filter_baselines
    type: string?
    default: '[CR]S*&'
    'sbg:x': -1000
    'sbg:y': -1300
  - id: rfistrategy
    type: string?
    default: HBAdefault.rfis
    'sbg:x': -1000
    'sbg:y': -1200
  - id: raw_data
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': -1100
  - id: propagatesolutions
    type: boolean?
    default: true
    'sbg:x': -1000
    'sbg:y': -1000
  - id: demix_sources
    type: 'string[]?'
    default:
      - CasA
      - CygA
    'sbg:x': -1000
    'sbg:y': -900
  - id: demix_target
    type: string?
    default: ''
    'sbg:x': -1000
    'sbg:y': -800
  - id: demix_freqstep
    type: int?
    default: 16
    'sbg:x': -1000
    'sbg:y': -700
  - id: demix_timestep
    type: int?
    default: 10
    'sbg:x': -1000
    'sbg:y': -600
  - id: demix
    type: boolean?
    default: false
    'sbg:x': -1000
    'sbg:y': -500
  - id: apply_tec
    type: boolean
    default: false
    'sbg:x': -1000
    'sbg:y': -400
  - id: apply_clock
    type: boolean
    default: true
    'sbg:x': -1000
    'sbg:y': -300
  - id: apply_phase
    type: boolean
    default: false
    'sbg:x': -1000
    'sbg:y': -200
  - id: apply_RM
    type: boolean
    default: true
    'sbg:x': -1000
    'sbg:y': -100
  - id: apply_beam
    type: boolean
    default: true
    'sbg:x': -1000
    'sbg:y': 0
  - id: clipATeam
    type: boolean?
    default: true
    'sbg:x': -1000
    'sbg:y': 100
  - id: updateweights
    type: boolean?
    default: true
    'sbg:x': -1000
    'sbg:y': 200
  - id: max_dppp_threads
    type: int?
    default: 10
    'sbg:x': -1000
    'sbg:y': 300
  - id: memoryperc
    type: int?
    default: 20
    'sbg:x': -1000
    'sbg:y': 400
  - id: min_separation
    type: int?
    default: 30
    'sbg:x': -1000
    'sbg:y': 500
  - id: A-Team_skymodel
    type: File?
    'sbg:x': -1000
    'sbg:y': 600
  - id: avg_timeresolution
    type: float?
    default: 4
    'sbg:x': -1000
    'sbg:y': 700
  - id: avg_freqresolution
    type: string?
    default: 48.82kHz
    'sbg:x': -1000
    'sbg:y': 800
  - id: ionex_server
    type: string?
    default: 'ftp://ftp.aiub.unibe.ch/CODE/'
    'sbg:x': -1000
    'sbg:y': 900
  - id: ionex_prefix
    type: string?
    default: CODG
    'sbg:x': -1000
    'sbg:y': 1000
  - id: proxy_server
    type: string?
    default: null
    'sbg:x': -1000
    'sbg:y': 1100
  - id: proxy_port
    type: int?
    default: null
    'sbg:x': -1000
    'sbg:y': 1200
  - id: proxy_type
    type: string?
    default: null
    'sbg:x': -1000
    'sbg:y': 1300
  - id: proxy_user
    type: string?
    default: null
    'sbg:x': -1000
    'sbg:y': 1400
  - id: proxy_pass
    type: string?
    default: null
    'sbg:x': -1000
    'sbg:y': 1500
  - id: elevation
    type: string
    default: 0deg..15deg
    'sbg:x': -1000
    'sbg:y': 1600
  - id: amplmin
    type: float
    default: 1.e-30
    'sbg:x': -1000
    'sbg:y': 1700
outputs:
  - id: outh5parm
    outputSource:
      - createRMh5parm/h5parmout
    type: File
    'sbg:x': 1000
    'sbg:y': -500
  - id: inspection
    outputSource:
      - check_ateam_separation/output_imag
      - losoto_plot_RM/output_plots
      - plot_Ateamclipper/output_imag
    type: 'File[]?'
    linkMerge: merge_flattened
    'sbg:x': 1000
    'sbg:y': -300
  - id: msout
    outputSource:
      - dppp_prep_target/msout
    type: 'Directory[]'
    'sbg:x': 1000
    'sbg:y': 0
  - id: logfiles
    outputSource:
      - make_sourcedb_ateam/log
      - check_ateam_separation/logfile
      - concat_logfiles_stationlist/output
      - concat_logfiles_RMextract/output
      - concat_logfiles_prep_targ/output
      - concat_logfiles_predict_targ/output
      - concat_logfiles_clipper_targ/output
    type: 'File[]'
    linkMerge: merge_flattened
    'sbg:x': 1000
    'sbg:y': 800
steps:
  - id: check_ateam_separation
    in:
      - id: ms
        source:
          - msin
      - id: min_separation
        source: min_separation
    out:
      - id: output_imag
      - id: logfile
    run: ../../steps/check_ateam_separation.cwl
    label: check_Ateam_separation
    'sbg:x': -500
    'sbg:y': -300
  - id: compare_station_list
    in:
      - id: msin
        source:
          - msin
      - id: h5parmdb
        source: cal_solutions
      - id: solset_name
        default: 'calibrator'
      - id: filter
        source: filter_baselines
    out:
      - id: filter_out
      - id: logfile
    run: ../../steps/compare_station_list.cwl
    label: compare_station_list
    'sbg:x': -500
    'sbg:y': -500
  - id: make_sourcedb_ateam
    in:
      - id: sky_model
        source: A-Team_skymodel
      - id: logname
        default: make_sourcedb_ateam.log
    out:
      - id: sourcedb
      - id: log
    run: ../../lofar-cwl/steps/makesourcedb.cwl
    label: make_sourcedb_ateam
    'sbg:x': -500
    'sbg:y': 300
  - id: createRMh5parm
    in:
      - id: msin
        source:
          - msin
      - id: h5parm
        source: cal_solutions
      - id: ionex_server
        source: ionex_server
      - id: ionex_prefix
        source: ionex_prefix
      - id: solset
        default: target
      - id: proxyserver
        source: proxy_server
      - id: proxyport
        source: proxy_port
      - id: proxytype
        source: proxy_type
      - id: proxyuser
        source: proxy_user
      - id: proxypass
        source: proxy_pass
    out:
      - id: h5parmout
      - id: logfile
    run: ../../steps/createRMh5parm.cwl
    label: createRMh5parm
    'sbg:x': -500
    'sbg:y': 0
  - id: losoto_plot_RM
    in:
      - id: input_h5parm
        source: createRMh5parm/h5parmout
      - id: soltab
        default: target/RMextract
      - id: axesInPlot
        default:
          - time
      - id: axisInTable
        default: ant
      - id: prefix
        default: RMextract
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../lofar-cwl/steps/LoSoTo.Plot.cwl
    label: losoto_plot_RM
    'sbg:x': 0
    'sbg:y': -300
  - id: dppp_prep_target
    in:
      - id: msin
        linkMerge: merge_flattened
        source:
          - msin
      - id: msin_baseline
        default: '*'
        source: compare_station_list/filter_out
      - id: baselines_to_flag
        source: flag_baselines
      - id: elevation_to_flag
        source: elevation
      - id: min_amplitude_to_flag
        source: amplmin
      - id: memoryperc
        default: 20
        source: memoryperc
      - id: raw_data
        default: false
        source: raw_data
      - id: demix
        default: false
        source: demix
      - id: skymodel
        source: make_sourcedb_ateam/sourcedb
      - id: timeresolution
        default: 1
        source: avg_timeresolution
      - id: freqresolution
        default: 12.21kHz
        source: avg_freqresolution
      - id: demix_timestep
        default: 1
        source: demix_timestep
      - id: demix_freqstep
        default: 1
        source: demix_freqstep
      - id: process_baselines_target
        default: '*'
        source: process_baselines_target
      - id: target_source
        source: demix_target
      - id: ntimechunk
        default: 10
        source: max_dppp_threads
      - id: subtract_sources
        source:
          - demix_sources
      - id: parmdb
        source: createRMh5parm/h5parmout
      - id: apply_tec_correction
        source: apply_tec
        default: false
      - id: apply_rm_correction
        source: apply_RM
        default: true
      - id: apply_phase_correction
        source: apply_phase
        default: false
      - id: apply_clock_correction
        source: apply_clock
        default: true
      - id: apply_beam_correction
        source: apply_beam
        default: true
      - id: filter_baselines
        source: compare_station_list/filter_out
      - id: updateweights
        source: updateweights
    out:
      - id: msout
      - id: prep_logfile
      - id: predict_logfile
      - id: clipper_logfile
      - id: clipper_output
      - id: parset
    run: ../../subworkflow/ndppp_prep_targ.cwl
    label: ndppp_prep_target
    scatter:
      - msin
    'sbg:x': 0
    'sbg:y': 0
  - id: plot_Ateamclipper
    in:
      - id: clipper_output
        source: concat_logfiles_clipper_output/output
    out:
      - id: output_imag
    run: ../../steps/plot_Ateamclipper.cwl
    label: concat_logfiles_clipper_output
    'sbg:x': 600
    'sbg:y': 200
  - id: concat_logfiles_clipper_output
    in:
      - id: file_list
        linkMerge: merge_flattened
        source: dppp_prep_target/clipper_output
      - id: file_prefix
        default: Ateamclipper
      - id: file_suffix
        default: txt
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_clipper_output
    'sbg:x': 400
    'sbg:y': 200
  - id: concat_logfiles_prep_targ
    in:
      - id: file_list
        linkMerge: merge_flattened
        source: dppp_prep_target/prep_logfile
      - id: file_prefix
        default: ndppp_prep_targ
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_prep_target
    'sbg:x': 400
    'sbg:y': 300
  - id: concat_logfiles_predict_targ
    in:
      - id: file_list
        linkMerge: merge_flattened
        source: dppp_prep_target/predict_logfile
      - id: file_prefix
        default: predict_targ
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_predict_targ
    'sbg:x': 400
    'sbg:y': 400
  - id: concat_logfiles_clipper_targ
    in:
      - id: file_list
        linkMerge: merge_flattened
        source: dppp_prep_target/clipper_logfile
      - id: file_prefix
        default: Ateamclipper
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_clipper_targ
    'sbg:x': 400
    'sbg:y': 500
  - id: concat_logfiles_RMextract
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - createRMh5parm/logfile
          - losoto_plot_RM/logfile
      - id: file_prefix
        default: RMextract
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_RMextract
    'sbg:x': 400
    'sbg:y': 600
  - id: concat_logfiles_stationlist
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - compare_station_list/logfile
      - id: file_prefix
        default: compareStationList
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_stationlist
    'sbg:x': 400
    'sbg:y': 700
requirements:
  - class: ScatterFeatureRequirement